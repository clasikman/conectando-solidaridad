import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'conectando_solidaridad_app',
  webDir: 'build',
  bundledWebRuntime: false
};

export default config;
