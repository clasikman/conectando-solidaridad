import { IonContent, IonHeader, IonImg, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import React, { useContext, useEffect, useState } from 'react';
import { IonGrid, IonRow, IonCol } from '@ionic/react';
import { IonItem, IonLabel, IonInput, IonButton, IonIcon, IonAlert } from '@ionic/react';
import logo from './logo.png';
import { useHistory } from 'react-router';

type Props = {
  setLog: (val:Boolean) => void;
};

const Login : React.FC<Props> = ({setLog}) => {
  const history=useHistory();
  const [username, set_username] = useState<string>();
  const [password, set_password] = useState<string>();
  const [iserror, setIserror] = useState<boolean>(false);
  const [message, setMessage] = useState<string>("");

  const handleLogin = () => {
    setLog(true);
    //history.push("/tab1");
  };

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle className="ion-text-center">INICIAR SESIÓN</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen className="ion-padding ion-text-center">
        <IonGrid>
        <IonRow>
          <IonCol>
            <IonAlert
                isOpen={iserror}
                onDidDismiss={() => setIserror(false)}
                header={"Error!"}
                message={message}
                buttons={["Aceptar"]}
            />
          </IonCol>
        </IonRow>
        <IonRow>
          <IonCol>
            <IonImg src={logo} style={{height: '40vh'}} className="my-4" alt="The Wisconsin State Capitol building in Madison, WI at night"></IonImg>
          </IonCol>
        </IonRow>
          <IonRow>
            <IonCol>
            <IonItem>
            <IonLabel position="floating">Nombre de Usuario</IonLabel>
            <IonInput
                type="text"
                value={username}
                onIonChange={(e) => set_username(e.detail.value!)}
                >
            </IonInput>
            </IonItem>
            </IonCol>
          </IonRow>
          <IonRow>
            <IonCol>
            <IonItem>
              <IonLabel position="floating">Contraseña</IonLabel>
              <IonInput
                type="password"
                value={password}
                onIonChange={(e) => set_password(e.detail.value!)}
                >
              </IonInput>
            </IonItem>
            </IonCol>
          </IonRow>
          <IonRow>
            <IonCol>
              <IonButton expand="block" onClick={handleLogin}>INGRESAR</IonButton>
            </IonCol>
          </IonRow>
        </IonGrid>
      </IonContent>
    </IonPage>
  );
};

export default Login;