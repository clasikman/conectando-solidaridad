import { IonBackButton, IonButton, IonButtons, IonContent, IonHeader, IonIcon, IonImg, IonInput, IonItem, IonLabel, IonList, IonPage, IonSelect, IonSelectOption, IonTextarea, IonTitle, IonToolbar } from '@ionic/react';
import { camera } from 'ionicons/icons';
import { useState } from 'react';
import { useHistory } from 'react-router';

const RegisterDonacion: React.FC = () => {

    const history=useHistory();
    const [imageSrc,set_imageSrc]=useState();

    const openFileDialog = () => {
        (document as any).getElementById("file-upload").click();
     };

    function setImage(e:any){
        //var file=fileRef.current.files[0];
        var file=e.target.files[0];
        var objectURL:any = URL.createObjectURL(file);
        set_imageSrc(objectURL);
    }

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Registrar Donación</IonTitle>
          <IonButtons slot="start">
            <IonBackButton></IonBackButton>
          </IonButtons>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Registrar Donación</IonTitle>
          </IonToolbar>
        </IonHeader>
        <IonList>
            <IonItem>
                <IonInput label="Fecha" type="date" labelPlacement="stacked" readonly value={new Date().toISOString().substr(0, 10)}></IonInput>
            </IonItem>
            <IonItem>
                <IonSelect aria-label="fruit" placeholder="Seleccione la Entidad" className='ion-width-full'>
                    <IonSelectOption value="Aldeas SOS">Aldeas SOS</IonSelectOption>
                    <IonSelectOption value="Narices Frias">Narices Frias</IonSelectOption>
                    <IonSelectOption value="bananas">Bananas</IonSelectOption>
                </IonSelect>
            </IonItem>
            <IonItem>
                <IonSelect aria-label="fruit" placeholder="Seleccione los Items a Donar" multiple className='ion-width-full'>
                    <IonSelectOption value="Alimentos">Alimentos</IonSelectOption>
                    <IonSelectOption value="Ropa">Ropa</IonSelectOption>
                    <IonSelectOption value="Medicamentos">Medicamentos</IonSelectOption>
                    <IonSelectOption value="Juguetes">Juguetes</IonSelectOption>
                    <IonSelectOption value="Suministros Escolares">Suministros Escolares</IonSelectOption>
                    <IonSelectOption value="Muebles">Muebles</IonSelectOption>
                </IonSelect>
            </IonItem>
            <IonItem>
                <IonTextarea label="Descripción de los Items" labelPlacement="floating" placeholder="*Ropa...." autoGrow={true}></IonTextarea>
            </IonItem>
            <IonItem>
                <IonLabel>Fotografía del Paquete</IonLabel>
                <IonButton onClick={openFileDialog}>
                    <IonIcon slot="icon-only" icon={camera}></IonIcon>
                </IonButton>
                <input
                    type="file"
                    id="file-upload"
                    style={{ display: "none" }}
                    onChange={setImage}
                />
            </IonItem>
            <IonImg src={imageSrc}></IonImg>
            <IonButton expand="block" color="success" onClick={()=>history.push("/tab2")}>Registrar</IonButton>
        </IonList>
      </IonContent>
    </IonPage>
  );
};

export default RegisterDonacion;
