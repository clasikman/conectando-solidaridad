import { IonBackButton, IonButton, IonButtons, IonChip, IonContent, IonHeader, IonIcon, IonImg, IonItem, IonLabel, IonList, IonPage, IonTextarea, IonTitle, IonToolbar } from '@ionic/react';
import { checkmarkDone } from 'ionicons/icons';
import respaldo from './respaldo.png';

const Tab2: React.FC = () => {
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Seguimiento</IonTitle>
          <IonButtons slot="start">
            <IonBackButton></IonBackButton>
          </IonButtons>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Seguimiento</IonTitle>
          </IonToolbar>
        </IonHeader>
        <IonList>
          <IonItem>
            <IonLabel>Claros-3</IonLabel>
            <IonLabel>03/04/2023</IonLabel>
            <IonChip>
              <IonIcon icon={checkmarkDone} color="success"></IonIcon>
              <IonLabel>Utilizado</IonLabel>
            </IonChip>
          </IonItem>
          <IonItem>
            <IonLabel>Narices Frias</IonLabel>
          </IonItem>
          <IonItem>
            <IonLabel>Items: Alimento</IonLabel>
          </IonItem>
          <IonItem>
            <IonTextarea readonly value="*Bolsa de Croquetas de Perro" label="Descripción de los Items" labelPlacement="stacked" placeholder="*Ropa...." autoGrow={true}></IonTextarea>
          </IonItem>
          <IonItem>
            <IonTextarea readonly value="
              ¡Gracias por tu generosidad!!! La bolsa de croquetas que nos has dado es muy apreciada y ayudará a mantener a nuestros peludos amigos alimentados." 
              label="Respuesta ENTIDAD" labelPlacement="stacked" placeholder="*Ropa...." autoGrow={true}></IonTextarea>
          </IonItem>
          <IonItem>
            <IonLabel>Respaldo: </IonLabel>
            <IonImg src={respaldo}></IonImg>
          </IonItem>
        </IonList>
      </IonContent>
    </IonPage>
  );
};

export default Tab2;
