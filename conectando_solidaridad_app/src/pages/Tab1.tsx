import { IonButton, IonContent, IonFab, IonFabButton, IonFabList, IonHeader, IonIcon, IonItem, IonLabel, IonList, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import { add, chevronDownCircle, colorPalette, eye, globe, document } from 'ionicons/icons';
import { useHistory } from 'react-router';
import ExploreContainer from '../components/ExploreContainer';
import './Tab1.css';

const Tab1: React.FC = () => {
  const history=useHistory();
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>DONACIONES</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">DONACIONES</IonTitle>
          </IonToolbar>
        </IonHeader>
        <IonItem>
          Realizadas
        </IonItem>
        <IonList>
          <IonItem>
            <IonLabel>Claros-4</IonLabel>
            <IonLabel>11/04/2023</IonLabel>
            <IonButton slot="end" color="danger" onClick={()=>history.push("/tab3")}>
              <IonIcon slot="icon-only" icon={eye}></IonIcon>
            </IonButton>
          </IonItem>
          <IonItem>
            <IonLabel>Claros-3</IonLabel>
            <IonLabel>05/04/2023</IonLabel>
            <IonButton slot="end" color="danger" onClick={()=>history.push("/tab3")}>
              <IonIcon slot="icon-only" icon={eye}></IonIcon>
            </IonButton>
          </IonItem>
          <IonItem>
            <IonLabel>Claros-2</IonLabel>
            <IonLabel>03/04/2023</IonLabel>
            <IonButton slot="end" color="danger" onClick={()=>history.push("/tab3")}>
              <IonIcon slot="icon-only" icon={eye}></IonIcon>
            </IonButton>
          </IonItem>
          <IonItem>
            <IonLabel>Claros-1</IonLabel>
            <IonLabel>15/03/2023</IonLabel>
            <IonButton slot="end" color="danger" onClick={()=>history.push("/tab3")}>
              <IonIcon slot="icon-only" icon={eye}></IonIcon>
            </IonButton>
          </IonItem>
        </IonList>
        <IonFab slot="fixed" vertical="top" horizontal="end" edge={true}>
          <IonFabButton>
            <IonIcon icon={chevronDownCircle}></IonIcon>
          </IonFabButton>
          <IonFabList side="bottom">
            <IonFabButton onClick={()=>history.push("/registerDonacion")}>
              <IonIcon icon={add}></IonIcon>
            </IonFabButton>
          </IonFabList>
        </IonFab>
      </IonContent>
    </IonPage>
  );
};

export default Tab1;
