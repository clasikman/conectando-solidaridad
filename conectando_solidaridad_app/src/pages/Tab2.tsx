import { IonButton, IonChip, IonContent, IonHeader, IonIcon, IonItem, IonLabel, IonList, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import { car, eye } from 'ionicons/icons';

const Tab2: React.FC = () => {
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>TRANSPORTE</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">TRANSPORTE</IonTitle>
          </IonToolbar>
        </IonHeader>
        <IonList>
          <IonItem>
            <IonLabel>Claros-4</IonLabel>
            <IonLabel>11/04/2023</IonLabel>
            <IonChip>
              <IonIcon icon={car} color="primary"></IonIcon>
              <IonLabel>Solicitado</IonLabel>
            </IonChip>
          </IonItem>
          <IonItem>
            <IonLabel>Claros-3</IonLabel>
            <IonLabel>05/04/2023</IonLabel>
            <IonChip>
              <IonIcon icon={car} color="success"></IonIcon>
              <IonLabel>Finalizado</IonLabel>
            </IonChip>
          </IonItem>
          <IonItem>
            <IonLabel>Claros-2</IonLabel>
            <IonLabel>03/04/2023</IonLabel>
            <IonChip>
              <IonIcon icon={car} color="success"></IonIcon>
              <IonLabel>Finalizado</IonLabel>
            </IonChip>
          </IonItem>
          <IonItem>
            <IonLabel>Claros-1</IonLabel>
            <IonLabel>15/03/2023</IonLabel>
            <IonChip>
              <IonIcon icon={car} color="success"></IonIcon>
              <IonLabel>Finalizado</IonLabel>
            </IonChip>
          </IonItem>
        </IonList>
      </IonContent>
    </IonPage>
  );
};

export default Tab2;
