import { IonChip, IonContent, IonHeader, IonIcon, IonItem, IonLabel, IonList, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import { checkbox, checkmarkDone } from 'ionicons/icons';
import { useHistory } from 'react-router';
import ExploreContainer from '../components/ExploreContainer';
import './Tab3.css';

const Tab3: React.FC = () => {
  const history=useHistory();
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>SEGUIMIENTO</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">SEGUIMIENTO</IonTitle>
          </IonToolbar>
        </IonHeader>
        <IonList>
          <IonItem>
            <IonLabel>Claros-4</IonLabel>
            <IonLabel>Aldeas SOS</IonLabel>
            <IonChip>
              <IonIcon icon={checkbox} color="primary"></IonIcon>
              <IonLabel>Entregado</IonLabel>
            </IonChip>
          </IonItem>
          <IonItem>
            <IonLabel>Claros-3</IonLabel>
            <IonLabel>Narices Frias</IonLabel>
            <IonChip onClick={()=>history.push('/seguimiento')}>
              <IonIcon icon={checkmarkDone} color="success"></IonIcon>
              <IonLabel>Utilizado</IonLabel>
            </IonChip>
          </IonItem>
          <IonItem>
            <IonLabel>Claros-2</IonLabel>
            <IonLabel>Aldeas SOS</IonLabel>
            <IonChip onClick={()=>history.push('/seguimiento')}>
              <IonIcon icon={checkmarkDone} color="success"></IonIcon>
              <IonLabel>Utilizado</IonLabel>
            </IonChip>
          </IonItem>
          <IonItem>
            <IonLabel>Claros-1</IonLabel>
            <IonLabel>Aldeas SOS</IonLabel>
            <IonChip onClick={()=>history.push('/seguimiento')}>
              <IonIcon icon={checkmarkDone} color="success"></IonIcon>
              <IonLabel>Utilizado</IonLabel>
            </IonChip>
          </IonItem>
        </IonList>
      </IonContent>
    </IonPage>
  );
};

export default Tab3;
