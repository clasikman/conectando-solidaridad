import jwt from 'jsonwebtoken';
import { GraphQLError } from 'graphql';

export async function verifyToken(token) {
    try {
        if (!token) return null;
        const userToken = await jwt.verify(token, process.env.JWT_SECRET);
        // Descomentar para mayor seguridad de verificacion
        //import User from '../models/auth/User.js'; //llevar arriba
        //const currentUser = await User.findById(userToken.id);
        //return {currentUser}
        return {currentUser:userToken};
    } catch (error) {
        console.error(error);
        throw new GraphQLError("SESION FINALIZADA", {
            extensions: {
                code: 'UNAUTHENTICATED',
            },
        });
    }
}

export default async function ({req,res}) {
    const token = (req.headers && req.headers.authorization) ? req.headers.authorization.slice(7,-7) : null;
    const user = await verifyToken(token);
    return { ...user };
}