import authentication from "./authentication.js"; 

export default async function ({req, res}) {
    const user=await authentication({req,res});
    return {
        ...user
    }
};