// npm install @apollo/server express graphql cors body-parser dotenv
import { ApolloServer } from '@apollo/server';
import { expressMiddleware } from '@apollo/server/express4';
import { ApolloServerPluginDrainHttpServer } from '@apollo/server/plugin/drainHttpServer';
import express from 'express';
import http from 'http';
import cors from 'cors';
import bodyParser from 'body-parser';
import { typeDefs, resolvers } from './schema/index.js';
import context from './context/index.js';
import { connectDB } from './utils/database.js';
import { updatePermission, createAdminUser } from './utils/update.js';
import { config } from 'dotenv';
config();

const PORT = process.env.PORT || 4000;

const app = express();
await connectDB();
if (process.env.NODE_ENV!="production") {
  //comentar despues de la primera vez
  updatePermission();///// Actualizar Permisos
  createAdminUser();
}

const httpServer = http.createServer(app);

const server = new ApolloServer({
  typeDefs,
  resolvers,
  plugins: [ApolloServerPluginDrainHttpServer({ httpServer })],
});

await server.start();

app.use(
  '/',
  cors(),
  bodyParser.json({ limit: '50mb' }),
  expressMiddleware(server, {
    context
  }),
);

// Modified server startup
await new Promise((resolve) => {
  httpServer.listen({ port: PORT }, resolve)
});

if(process.env.NODE_ENV!='production') {
  console.log(`🚀 Server ready at http://localhost:${process.env.PORT}/`);
}