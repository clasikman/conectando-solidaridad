import { Schema, model } from "mongoose";

const entidadSchema = new Schema({
    categoria: [{
        type: Schema.Types.ObjectId,
        required: true,
        ref: 'CATEGORIAS',
    }],
    nombre: {
        type: Schema.Types.String,
        required: true,
    },
    telefono: {
        type: Schema.Types.Number,
        required: false,
    },
    representante: {
        type: Schema.Types.String,
    },
    direccion: {
        type: Schema.Types.String,
        required: true,
    },
    latitud: {
        type: Schema.Types.Number,
        default: -17.395044,
    },
    longitud: {
        type: Schema.Types.Number,
        default: -66.1452495,
    },
    descripcion: {
        type: Schema.Types.String,
        required: true,
    },
    actividades: {
        type: Schema.Types.String,
        required: true,
    },
    last_user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    }
},{
    timestamps: true, //createdAt updatedAt automatic
    methods: {
        //solo para el documento
    },
    statics: {
        //para todo el modelo
    },
    query: {
        //para odenar o hacer consultas especiales
    }
});
export default model('ENTIDAD',entidadSchema);