import { Schema, model } from "mongoose";

const registro_donaciones_fisicasSchema = new Schema({
    item: {
        type: Schema.Types.Mixed,
        required: true,
    },
    foto: {
        type: Schema.Types.String,
    },
    descripcion: {
        type: Schema.Types.String,
    },
    last_user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    }
},{
    timestamps: true, //createdAt updatedAt automatic
    methods: {
        //solo para el documento
    },
    statics: {
        //para todo el modelo
    },
    query: {
        //para odenar o hacer consultas especiales
    }
});
export default model('REGISTRO_DONACIONES_FISICAS',registro_donaciones_fisicasSchema);