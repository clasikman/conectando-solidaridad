import { Schema, model } from "mongoose";

const categoriasSchema = new Schema({
    nombre: {
        type: Schema.Types.String,
        required: true,
        maxLength: 120,
    },
    descripcion: {
        type: Schema.Types.String,
        required: true,
        maxLength: 100,
    },
    items: [{
        type: Schema.Types.ObjectId,
        required: true,
        ref: 'ITEMS_DONACION',
    }],
    last_user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    }
},{
    timestamps: true, //createdAt updatedAt automatic
    methods: {
        //solo para el documento
    },
    statics: {
        //para todo el modelo
    },
    query: {
        //para odenar o hacer consultas especiales
    }
});
export default model('CATEGORIAS',categoriasSchema);