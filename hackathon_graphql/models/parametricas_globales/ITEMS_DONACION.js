import { Schema, model } from "mongoose";

const items_donacionSchema = new Schema({
    item_donacion: {
        type: Schema.Types.String,
        required: true,
    },
    last_user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    }
},{
    timestamps: true, //createdAt updatedAt automatic
    methods: {
        //solo para el documento
    },
    statics: {
        //para todo el modelo
    },
    query: {
        //para odenar o hacer consultas especiales
    }
});
export default model('ITEMS_DONACION',items_donacionSchema);