import { Schema, model } from "mongoose";

const empresa_transporteSchema = new Schema({
    nombre_transporte: {
        type: Schema.Types.String,
        required: true,
    },
    telefono: {
        type: Schema.Types.Number,
        required: true,
    },
    direccion: {
        type: Schema.Types.String,
        required: true,
    },
    longitude: {
        type: Schema.Types.Number,
        default: -66,
    },
    latitude: {
        type: Schema.Types.Number,
        default: -17.39504476,
    },
    last_user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    }
},{
    timestamps: true, //createdAt updatedAt automatic
    methods: {
        //solo para el documento
    },
    statics: {
        //para todo el modelo
    },
    query: {
        //para odenar o hacer consultas especiales
    }
});
export default model('Empresa_transporte',empresa_transporteSchema);