import { Schema, model } from "mongoose";

const donacionesSchema = new Schema({
    entidad: {
        type: Schema.Types.ObjectId,
        ref: 'ENTIDAD',
    },
    items: {
        type: Schema.Types.Mixed,
        required: true,
    },
    respaldo: {
        type: Schema.Types.String,
    },
    descripcion: {
        type: Schema.Types.String,
        required: true,
    },
    fecha: {
        type: Schema.Types.Date,
        default: 'True',
    },
    codigo: {
        type: Schema.Types.String,
    },
    estado: {
        type: Schema.Types.String,
        required: true,
        default: 'Entregado',
    },
    tarifa: {
        type: Schema.Types.Number,
    },
    usuario: {
        type: Schema.Types.Mixed,
        ref: 'USUARIOS',
    },
    last_user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    }
},{
    timestamps: true, //createdAt updatedAt automatic
    methods: {
        //solo para el documento
    },
    statics: {
        //para todo el modelo
    },
    query: {
        //para odenar o hacer consultas especiales
    }
});
export default model('DONACIONES',donacionesSchema);