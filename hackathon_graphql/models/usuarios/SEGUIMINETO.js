import { Schema, model } from "mongoose";

const seguiminetoSchema = new Schema({
    donacion: {
        type: Schema.Types.ObjectId,
        ref: 'DONACIONES',
    },
    respaldo_entrega_trasnsporte: {
        type: Schema.Types.String,
    },
    respuesta_entidad: {
        type: Schema.Types.String,
        required: true,
    },
    respuesta_entidad_respaldo: {
        type: Schema.Types.String,
    },
    last_user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    }
},{
    timestamps: true, //createdAt updatedAt automatic
    methods: {
        //solo para el documento
    },
    statics: {
        //para todo el modelo
    },
    query: {
        //para odenar o hacer consultas especiales
    }
});
export default model('SEGUIMINETO',seguiminetoSchema);