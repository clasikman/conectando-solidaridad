import { Schema, model } from "mongoose";

const usuariosSchema = new Schema({
    telefono: {
        type: Schema.Types.Number,
        required: true,
        min: 120,
    },
    direccion: {
        type: Schema.Types.String,
        required: true,
    },
    latitude: {
        type: Schema.Types.Number,
        default: -17.394996566,
    },
    longitude: {
        type: Schema.Types.Number,
        default: -66.1453225064,
    },
    nombre: {
        type: Schema.Types.String,
        required: true,
    },
    usuario: {
        type: Schema.Types.String,
        required: true,
    },
    password: {
        type: Schema.Types.String,
        required: true,
    },
    donaciones: [{
        type: Schema.Types.Mixed,
        ref: 'DONACIONES',
    }],
    last_user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    }
},{
    timestamps: true, //createdAt updatedAt automatic
    methods: {
        //solo para el documento
    },
    statics: {
        //para todo el modelo
    },
    query: {
        //para odenar o hacer consultas especiales
    }
});
export default model('USUARIOS',usuariosSchema);