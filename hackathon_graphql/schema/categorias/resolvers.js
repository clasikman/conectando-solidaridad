import { send_error, auth_required, get_all, get_document } from '../../utils/index.js';
import CATEGORIAS from '../../models/parametricas_globales/CATEGORIAS.js';

export default {                                                                                         
    Query: {
        async all_categorias(_,args,{currentUser},info) {
            auth_required(currentUser,['read_categorias']);
            const documents=await get_all(CATEGORIAS,args,info);
            return documents;
        },
        async read_categorias(__,{find},{currentUser},info) {
            auth_required(currentUser,['read_categorias']);
            const document=await get_document(CATEGORIAS,find,info);
            return document;
        },
    },
    Mutation: {
        async create_categorias(__, {categorias},{currentUser}) {
            auth_required(currentUser,['create_categorias']);
            try {
                categorias.last_user=currentUser.id;
                
                var query=await new CATEGORIAS(categorias).save()
                return query;
            } catch(error) {
                console.log(error);
                send_error('DATOS INVÁLIDOS',error.errors.name.name,error.errors.name);
            }
        },
        async update_categorias(__,{id,categorias},{currentUser}) {
            auth_required(currentUser,['update_categorias']);
            try {
                categorias.last_user=currentUser.id;
                const document=await CATEGORIAS.findByIdAndUpdate(id,{
                    $set:categorias
                },{new:true});
                return document;
            } catch (error) {
                console.log(error);
                send_error('DATOS INVÁLIDOS',error.errors.name.name,error.errors.name);
            }
        },
        async delete_categorias(__,{id},{currentUser}) {
            auth_required(currentUser,['delete_categorias']);
            try {
                const categorias=await CATEGORIAS.findByIdAndDelete(id)
                return 'CATEGORIAS delete';
            } catch (error) {
                console.log(error);
                send_error("IDENTIFICADOR INVÁLIDO",error.errors.name.name,error.errors.name);
            }
        },
    }
}