export default `#graphql
    type CATEGORIAS {
        id: ID,
        nombre: String!
        descripcion: String!
        items: [ITEMS_DONACION]!
        last_user: User
        createdAt: Date
        updatedAt: Date
    }
    input CATEGORIASInput {
        nombre: String!
        descripcion: String!
        items: [ID]!
    }
    type All_CATEGORIAS {
        list: [CATEGORIAS]!
        count: Int
    }
    extend type Query {
        all_categorias(find:JSON,page:Int,limit:Int,sort:JSON,count:Boolean): All_CATEGORIAS
        read_categorias(find:JSON): CATEGORIAS!
        count_categorias(find:JSON):Int
    }
    extend type Mutation {
        create_categorias(categorias:CATEGORIASInput): CATEGORIAS
        update_categorias(id:ID!,categorias:CATEGORIASInput): CATEGORIAS
        delete_categorias(id:ID!): String
    }
`;