import { send_error, auth_required, get_all, get_document } from '../../utils/index.js';
import DONACIONES from '../../models/usuarios/DONACIONES.js';

export default {                                                                                         
    Query: {
        async all_donaciones(_,args,{currentUser},info) {
            auth_required(currentUser,['read_donaciones']);
            const documents=await get_all(DONACIONES,args,info);
            return documents;
        },
        async read_donaciones(__,{find},{currentUser},info) {
            auth_required(currentUser,['read_donaciones']);
            const document=await get_document(DONACIONES,find,info);
            return document;
        },
    },
    Mutation: {
        async create_donaciones(__, {donaciones},{currentUser}) {
            auth_required(currentUser,['create_donaciones']);
            try {
                donaciones.last_user=currentUser.id;
                
                var query=await new DONACIONES(donaciones).save()
                return query;
            } catch(error) {
                console.log(error);
                send_error('DATOS INVÁLIDOS',error.errors.name.name,error.errors.name);
            }
        },
        async update_donaciones(__,{id,donaciones},{currentUser}) {
            auth_required(currentUser,['update_donaciones']);
            try {
                donaciones.last_user=currentUser.id;
                const document=await DONACIONES.findByIdAndUpdate(id,{
                    $set:donaciones
                },{new:true});
                return document;
            } catch (error) {
                console.log(error);
                send_error('DATOS INVÁLIDOS',error.errors.name.name,error.errors.name);
            }
        },
        async delete_donaciones(__,{id},{currentUser}) {
            auth_required(currentUser,['delete_donaciones']);
            try {
                const donaciones=await DONACIONES.findByIdAndDelete(id)
                return 'DONACIONES delete';
            } catch (error) {
                console.log(error);
                send_error("IDENTIFICADOR INVÁLIDO",error.errors.name.name,error.errors.name);
            }
        },
    }
}