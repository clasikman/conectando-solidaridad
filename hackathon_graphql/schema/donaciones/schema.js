export default `#graphql
    type DONACIONES {
        id: ID,
        entidad: ENTIDAD
        items: JSON!
        respaldo: String
        descripcion: String!
        fecha: Date
        codigo: String
        estado: String!
        tarifa: Int
        usuario: JSON
        last_user: User
        createdAt: Date
        updatedAt: Date
    }
    input DONACIONESInput {
        entidad: ID
        items: JSON!
        respaldo: String
        descripcion: String!
        fecha: Date
        codigo: String
        estado: String!
        tarifa: Int
        usuario: JSON
    }
    type All_DONACIONES {
        list: [DONACIONES]!
        count: Int
    }
    extend type Query {
        all_donaciones(find:JSON,page:Int,limit:Int,sort:JSON,count:Boolean): All_DONACIONES
        read_donaciones(find:JSON): DONACIONES!
        count_donaciones(find:JSON):Int
    }
    extend type Mutation {
        create_donaciones(donaciones:DONACIONESInput): DONACIONES
        update_donaciones(id:ID!,donaciones:DONACIONESInput): DONACIONES
        delete_donaciones(id:ID!): String
    }
`;