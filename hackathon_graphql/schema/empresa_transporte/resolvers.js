import { send_error, auth_required, get_all, get_document } from '../../utils/index.js';
import Empresa_transporte from '../../models/transporte/Empresa_transporte.js';

export default {                                                                                         
    Query: {
        async all_empresa_transporte(_,args,{currentUser},info) {
            auth_required(currentUser,['read_empresa_transporte']);
            const documents=await get_all(Empresa_transporte,args,info);
            return documents;
        },
        async read_empresa_transporte(__,{find},{currentUser},info) {
            auth_required(currentUser,['read_empresa_transporte']);
            const document=await get_document(Empresa_transporte,find,info);
            return document;
        },
    },
    Mutation: {
        async create_empresa_transporte(__, {empresa_transporte},{currentUser}) {
            auth_required(currentUser,['create_empresa_transporte']);
            try {
                empresa_transporte.last_user=currentUser.id;
                
                var query=await new Empresa_transporte(empresa_transporte).save()
                return query;
            } catch(error) {
                console.log(error);
                send_error('DATOS INVÁLIDOS',error.errors.name.name,error.errors.name);
            }
        },
        async update_empresa_transporte(__,{id,empresa_transporte},{currentUser}) {
            auth_required(currentUser,['update_empresa_transporte']);
            try {
                empresa_transporte.last_user=currentUser.id;
                const document=await Empresa_transporte.findByIdAndUpdate(id,{
                    $set:empresa_transporte
                },{new:true});
                return document;
            } catch (error) {
                console.log(error);
                send_error('DATOS INVÁLIDOS',error.errors.name.name,error.errors.name);
            }
        },
        async delete_empresa_transporte(__,{id},{currentUser}) {
            auth_required(currentUser,['delete_empresa_transporte']);
            try {
                const empresa_transporte=await Empresa_transporte.findByIdAndDelete(id)
                return 'Empresa_transporte delete';
            } catch (error) {
                console.log(error);
                send_error("IDENTIFICADOR INVÁLIDO",error.errors.name.name,error.errors.name);
            }
        },
    }
}