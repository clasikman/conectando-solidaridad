export default `#graphql
    type Empresa_transporte {
        id: ID,
        nombre_transporte: String!
        telefono: Int!
        direccion: String!
        longitude: Float
        latitude: Float
        last_user: User
        createdAt: Date
        updatedAt: Date
    }
    input Empresa_transporteInput {
        nombre_transporte: String!
        telefono: Int!
        direccion: String!
        longitude: Float
        latitude: Float
    }
    type All_Empresa_transporte {
        list: [Empresa_transporte]!
        count: Int
    }
    extend type Query {
        all_empresa_transporte(find:JSON,page:Int,limit:Int,sort:JSON,count:Boolean): All_Empresa_transporte
        read_empresa_transporte(find:JSON): Empresa_transporte!
        count_empresa_transporte(find:JSON):Int
    }
    extend type Mutation {
        create_empresa_transporte(empresa_transporte:Empresa_transporteInput): Empresa_transporte
        update_empresa_transporte(id:ID!,empresa_transporte:Empresa_transporteInput): Empresa_transporte
        delete_empresa_transporte(id:ID!): String
    }
`;