import { send_error, auth_required, get_all, get_document } from '../../utils/index.js';
import ENTIDAD from '../../models/entidades/ENTIDAD.js';

export default {                                                                                         
    Query: {
        async all_entidad(_,args,{currentUser},info) {
            auth_required(currentUser,['read_entidad']);
            const documents=await get_all(ENTIDAD,args,info);
            return documents;
        },
        async read_entidad(__,{find},{currentUser},info) {
            auth_required(currentUser,['read_entidad']);
            const document=await get_document(ENTIDAD,find,info);
            return document;
        },
    },
    Mutation: {
        async create_entidad(__, {entidad},{currentUser}) {
            auth_required(currentUser,['create_entidad']);
            try {
                entidad.last_user=currentUser.id;
                
                var query=await new ENTIDAD(entidad).save()
                return query;
            } catch(error) {
                console.log(error);
                send_error('DATOS INVÁLIDOS',error.errors.name.name,error.errors.name);
            }
        },
        async update_entidad(__,{id,entidad},{currentUser}) {
            auth_required(currentUser,['update_entidad']);
            try {
                entidad.last_user=currentUser.id;
                const document=await ENTIDAD.findByIdAndUpdate(id,{
                    $set:entidad
                },{new:true});
                return document;
            } catch (error) {
                console.log(error);
                send_error('DATOS INVÁLIDOS',error.errors.name.name,error.errors.name);
            }
        },
        async delete_entidad(__,{id},{currentUser}) {
            auth_required(currentUser,['delete_entidad']);
            try {
                const entidad=await ENTIDAD.findByIdAndDelete(id)
                return 'ENTIDAD delete';
            } catch (error) {
                console.log(error);
                send_error("IDENTIFICADOR INVÁLIDO",error.errors.name.name,error.errors.name);
            }
        },
    }
}