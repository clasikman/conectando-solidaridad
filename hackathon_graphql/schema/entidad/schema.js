export default `#graphql
    type ENTIDAD {
        id: ID,
        categoria: [CATEGORIAS]!
        nombre: String!
        telefono: Int
        representante: String
        direccion: String!
        latitud: Float
        longitud: Float
        descripcion: String!
        actividades: String!
        last_user: User
        createdAt: Date
        updatedAt: Date
    }
    input ENTIDADInput {
        categoria: [ID]!
        nombre: String!
        telefono: Int
        representante: String
        direccion: String!
        latitud: Float
        longitud: Float
        descripcion: String!
        actividades: String!
    }
    type All_ENTIDAD {
        list: [ENTIDAD]!
        count: Int
    }
    extend type Query {
        all_entidad(find:JSON,page:Int,limit:Int,sort:JSON,count:Boolean): All_ENTIDAD
        read_entidad(find:JSON): ENTIDAD!
        count_entidad(find:JSON):Int
    }
    extend type Mutation {
        create_entidad(entidad:ENTIDADInput): ENTIDAD
        update_entidad(id:ID!,entidad:ENTIDADInput): ENTIDAD
        delete_entidad(id:ID!): String
    }
`;