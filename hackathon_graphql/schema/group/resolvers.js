import { send_error, auth_required, get_all, get_document } from '../../utils/index.js';
import Group from '../../models/auth/Group.js';

export default {                                                                                         
    Query: {
        async all_group(_,args,{currentUser},info) {
            auth_required(currentUser,['read_group'],true);
            const documents=await get_all(Group,args,info);
            return documents;
        },
        async read_group(__,{find},{currentUser},info) {
            auth_required(currentUser,['read_group'],true);
            const document=await get_document(Group,find,info);
            return document;
        },
        async count_user(_, {find},{currentUser}) {
            auth_required(currentUser,['read_group'],true);
            const count=await Group.countDocuments({...find}).exec();
            return count;
        },
    },
    Mutation: {
        async create_group(__, {group},{currentUser}) {
            auth_required(currentUser,['create_user'],true);
            try {
                group.last_user=currentUser.id;
                var query=await new Group(group).save()
                return query;
            } catch(error) {
                console.log(error);
                send_error('DATOS INVÁLIDOS',error.errors.name.name,error.errors.name);
            }
        },
        async update_group(__,{id,group},{currentUser}) {
            auth_required(currentUser,['update_user'],true);
            try {
                group.last_user=currentUser.id;
                const document=await Group.findByIdAndUpdate(id,{
                    $set:group
                },{new:true});
                return document;
            } catch (error) {
                console.log(error);
                send_error('DATOS INVÁLIDOS',error.errors.name.name,error.errors.name);
            }
        },
        async delete_group(__,{id},{currentUser}) {
            auth_required(currentUser,['delete_group'],true);            
            try {
                const user=await Group.findByIdAndDelete(id)
                return 'Group delete';
            } catch (error) {
                console.log(error);
                send_error("IDENTIFICADOR INVÁLIDO",error.errors.name.name,error.errors.name);
            }
        },
    }
}