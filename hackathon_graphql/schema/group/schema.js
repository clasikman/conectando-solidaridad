export default `#graphql
    scalar Date
    type Group {
        id: ID
        name: String
        permissions: [Permission!]
    }
    input GroupInput {
        name: String
        permissions: [ID]!
    }
    type All_Group {
        list: [Group]!
        count: Int
    }
    extend type Query {
        all_group(find:JSON,page:Int,limit:Int,sort:JSON,count:Boolean): All_Group
        read_group(find:JSON): Group!
        count_group(find:JSON):Int
    }
    extend type Mutation {
        create_group(group:GroupInput): Group
        update_group(id:ID!,group:GroupInput): Group
        delete_group(id:ID!): String
    }
`;