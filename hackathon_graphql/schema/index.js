import { GraphQLScalarType } from 'graphql';
import GraphQLJSON from 'graphql-type-json';

import user from './user/index.js';
import permission from './permission/index.js';
import group from './group/index.js';
import usuarios from './usuarios/index.js';
import donaciones from './donaciones/index.js';
import seguimineto from './seguimineto/index.js';
import entidad from './entidad/index.js';
import registro_donaciones_fisicas from './registro_donaciones_fisicas/index.js';
import empresa_transporte from './empresa_transporte/index.js';
import categorias from './categorias/index.js';
import items_donacion from './items_donacion/index.js';
//imports

const rootType = `#graphql
    scalar JSON
    type Query {
        root: String
    }
    type Mutation {
        root: String
    }
`;

const rootResolvers= {
    Date: new GraphQLScalarType({
        name: 'Date',
        description: 'Date custom scalar type',
        serialize(value) {
            return value.getTime();
        },
        parseValue(value) {
            return new Date(value);
        },
        parseLiteral(ast) {                                                                             
            if (ast.kind === Kind.INT) 
                return new Date(parseInt(ast.value, 10));
            return null;
        },
    }),
    JSON: GraphQLJSON,
}

export const typeDefs=[
    rootType,
    user.schema,
    group.schema,
    permission.schema,
    usuarios.schema,
    donaciones.schema,
    seguimineto.schema,
    entidad.schema,
    registro_donaciones_fisicas.schema,
    empresa_transporte.schema,
    categorias.schema,
    items_donacion.schema,
//schemas
];
export const resolvers=[
    rootResolvers,
    user.resolvers,
    group.resolvers,
    permission.resolvers,
    usuarios.resolvers,
    donaciones.resolvers,
    seguimineto.resolvers,
    entidad.resolvers,
    registro_donaciones_fisicas.resolvers,
    empresa_transporte.resolvers,
    categorias.resolvers,
    items_donacion.resolvers,
//resolvers
];