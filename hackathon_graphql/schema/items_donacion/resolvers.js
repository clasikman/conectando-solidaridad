import { send_error, auth_required, get_all, get_document } from '../../utils/index.js';
import ITEMS_DONACION from '../../models/parametricas_globales/ITEMS_DONACION.js';

export default {                                                                                         
    Query: {
        async all_items_donacion(_,args,{currentUser},info) {
            auth_required(currentUser,['read_items_donacion']);
            const documents=await get_all(ITEMS_DONACION,args,info);
            return documents;
        },
        async read_items_donacion(__,{find},{currentUser},info) {
            auth_required(currentUser,['read_items_donacion']);
            const document=await get_document(ITEMS_DONACION,find,info);
            return document;
        },
    },
    Mutation: {
        async create_items_donacion(__, {items_donacion},{currentUser}) {
            auth_required(currentUser,['create_items_donacion']);
            try {
                items_donacion.last_user=currentUser.id;
                
                var query=await new ITEMS_DONACION(items_donacion).save()
                return query;
            } catch(error) {
                console.log(error);
                send_error('DATOS INVÁLIDOS',error.errors.name.name,error.errors.name);
            }
        },
        async update_items_donacion(__,{id,items_donacion},{currentUser}) {
            auth_required(currentUser,['update_items_donacion']);
            try {
                items_donacion.last_user=currentUser.id;
                const document=await ITEMS_DONACION.findByIdAndUpdate(id,{
                    $set:items_donacion
                },{new:true});
                return document;
            } catch (error) {
                console.log(error);
                send_error('DATOS INVÁLIDOS',error.errors.name.name,error.errors.name);
            }
        },
        async delete_items_donacion(__,{id},{currentUser}) {
            auth_required(currentUser,['delete_items_donacion']);
            try {
                const items_donacion=await ITEMS_DONACION.findByIdAndDelete(id)
                return 'ITEMS_DONACION delete';
            } catch (error) {
                console.log(error);
                send_error("IDENTIFICADOR INVÁLIDO",error.errors.name.name,error.errors.name);
            }
        },
    }
}