export default `#graphql
    type ITEMS_DONACION {
        id: ID,
        item_donacion: String!
        last_user: User
        createdAt: Date
        updatedAt: Date
    }
    input ITEMS_DONACIONInput {
        item_donacion: String!
    }
    type All_ITEMS_DONACION {
        list: [ITEMS_DONACION]!
        count: Int
    }
    extend type Query {
        all_items_donacion(find:JSON,page:Int,limit:Int,sort:JSON,count:Boolean): All_ITEMS_DONACION
        read_items_donacion(find:JSON): ITEMS_DONACION!
        count_items_donacion(find:JSON):Int
    }
    extend type Mutation {
        create_items_donacion(items_donacion:ITEMS_DONACIONInput): ITEMS_DONACION
        update_items_donacion(id:ID!,items_donacion:ITEMS_DONACIONInput): ITEMS_DONACION
        delete_items_donacion(id:ID!): String
    }
`;