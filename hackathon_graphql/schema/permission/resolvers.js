import { send_error, auth_required, get_all, get_document } from '../../utils/index.js';
import Permission from '../../models/auth/Permission.js';

export default {
    Query: {
        async all_permission(_,args,{currentUser},info) {
            auth_required(currentUser,[]);
            const documents=await get_all(Permission,args,info);
            return documents;
        },
        async read_permission(__,{find},{currentUser},info) {
            auth_required(currentUser,[]);
            const document=await get_document(Permission,find,info);
            return document;
        }
    },
}