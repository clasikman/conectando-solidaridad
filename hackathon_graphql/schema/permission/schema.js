export default `#graphql
    type Permission {
        id: ID
        name: String
        codename: String
    }
    type All_Permission {
        list: [Permission]!
        count: Int 
    }
    extend type Query {
        all_permission(find:JSON,page:Int,limit:Int,sort:JSON,count:Boolean): All_Permission
        read_permission(find:JSON): Permission
    }
`;