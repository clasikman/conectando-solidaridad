import { send_error, auth_required, get_all, get_document } from '../../utils/index.js';
import REGISTRO_DONACIONES_FISICAS from '../../models/entidades/REGISTRO_DONACIONES_FISICAS.js';

export default {                                                                                         
    Query: {
        async all_registro_donaciones_fisicas(_,args,{currentUser},info) {
            auth_required(currentUser,['read_registro_donaciones_fisicas']);
            const documents=await get_all(REGISTRO_DONACIONES_FISICAS,args,info);
            return documents;
        },
        async read_registro_donaciones_fisicas(__,{find},{currentUser},info) {
            auth_required(currentUser,['read_registro_donaciones_fisicas']);
            const document=await get_document(REGISTRO_DONACIONES_FISICAS,find,info);
            return document;
        },
    },
    Mutation: {
        async create_registro_donaciones_fisicas(__, {registro_donaciones_fisicas},{currentUser}) {
            auth_required(currentUser,['create_registro_donaciones_fisicas']);
            try {
                registro_donaciones_fisicas.last_user=currentUser.id;
                
                var query=await new REGISTRO_DONACIONES_FISICAS(registro_donaciones_fisicas).save()
                return query;
            } catch(error) {
                console.log(error);
                send_error('DATOS INVÁLIDOS',error.errors.name.name,error.errors.name);
            }
        },
        async update_registro_donaciones_fisicas(__,{id,registro_donaciones_fisicas},{currentUser}) {
            auth_required(currentUser,['update_registro_donaciones_fisicas']);
            try {
                registro_donaciones_fisicas.last_user=currentUser.id;
                const document=await REGISTRO_DONACIONES_FISICAS.findByIdAndUpdate(id,{
                    $set:registro_donaciones_fisicas
                },{new:true});
                return document;
            } catch (error) {
                console.log(error);
                send_error('DATOS INVÁLIDOS',error.errors.name.name,error.errors.name);
            }
        },
        async delete_registro_donaciones_fisicas(__,{id},{currentUser}) {
            auth_required(currentUser,['delete_registro_donaciones_fisicas']);
            try {
                const registro_donaciones_fisicas=await REGISTRO_DONACIONES_FISICAS.findByIdAndDelete(id)
                return 'REGISTRO_DONACIONES_FISICAS delete';
            } catch (error) {
                console.log(error);
                send_error("IDENTIFICADOR INVÁLIDO",error.errors.name.name,error.errors.name);
            }
        },
    }
}