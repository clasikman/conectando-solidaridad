export default `#graphql
    type REGISTRO_DONACIONES_FISICAS {
        id: ID,
        item: JSON!
        foto: String
        descripcion: String
        last_user: User
        createdAt: Date
        updatedAt: Date
    }
    input REGISTRO_DONACIONES_FISICASInput {
        item: JSON!
        foto: String
        descripcion: String
    }
    type All_REGISTRO_DONACIONES_FISICAS {
        list: [REGISTRO_DONACIONES_FISICAS]!
        count: Int
    }
    extend type Query {
        all_registro_donaciones_fisicas(find:JSON,page:Int,limit:Int,sort:JSON,count:Boolean): All_REGISTRO_DONACIONES_FISICAS
        read_registro_donaciones_fisicas(find:JSON): REGISTRO_DONACIONES_FISICAS!
        count_registro_donaciones_fisicas(find:JSON):Int
    }
    extend type Mutation {
        create_registro_donaciones_fisicas(registro_donaciones_fisicas:REGISTRO_DONACIONES_FISICASInput): REGISTRO_DONACIONES_FISICAS
        update_registro_donaciones_fisicas(id:ID!,registro_donaciones_fisicas:REGISTRO_DONACIONES_FISICASInput): REGISTRO_DONACIONES_FISICAS
        delete_registro_donaciones_fisicas(id:ID!): String
    }
`;