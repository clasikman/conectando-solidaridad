import { send_error, auth_required, get_all, get_document } from '../../utils/index.js';
import SEGUIMINETO from '../../models/usuarios/SEGUIMINETO.js';

export default {                                                                                         
    Query: {
        async all_seguimineto(_,args,{currentUser},info) {
            auth_required(currentUser,['read_seguimineto']);
            const documents=await get_all(SEGUIMINETO,args,info);
            return documents;
        },
        async read_seguimineto(__,{find},{currentUser},info) {
            auth_required(currentUser,['read_seguimineto']);
            const document=await get_document(SEGUIMINETO,find,info);
            return document;
        },
    },
    Mutation: {
        async create_seguimineto(__, {seguimineto},{currentUser}) {
            auth_required(currentUser,['create_seguimineto']);
            try {
                seguimineto.last_user=currentUser.id;
                
                var query=await new SEGUIMINETO(seguimineto).save()
                return query;
            } catch(error) {
                console.log(error);
                send_error('DATOS INVÁLIDOS',error.errors.name.name,error.errors.name);
            }
        },
        async update_seguimineto(__,{id,seguimineto},{currentUser}) {
            auth_required(currentUser,['update_seguimineto']);
            try {
                seguimineto.last_user=currentUser.id;
                const document=await SEGUIMINETO.findByIdAndUpdate(id,{
                    $set:seguimineto
                },{new:true});
                return document;
            } catch (error) {
                console.log(error);
                send_error('DATOS INVÁLIDOS',error.errors.name.name,error.errors.name);
            }
        },
        async delete_seguimineto(__,{id},{currentUser}) {
            auth_required(currentUser,['delete_seguimineto']);
            try {
                const seguimineto=await SEGUIMINETO.findByIdAndDelete(id)
                return 'SEGUIMINETO delete';
            } catch (error) {
                console.log(error);
                send_error("IDENTIFICADOR INVÁLIDO",error.errors.name.name,error.errors.name);
            }
        },
    }
}