export default `#graphql
    type SEGUIMINETO {
        id: ID,
        donacion: DONACIONES
        respaldo_entrega_trasnsporte: String
        respuesta_entidad: String!
        respuesta_entidad_respaldo: String
        last_user: User
        createdAt: Date
        updatedAt: Date
    }
    input SEGUIMINETOInput {
        donacion: ID
        respaldo_entrega_trasnsporte: String
        respuesta_entidad: String!
        respuesta_entidad_respaldo: String
    }
    type All_SEGUIMINETO {
        list: [SEGUIMINETO]!
        count: Int
    }
    extend type Query {
        all_seguimineto(find:JSON,page:Int,limit:Int,sort:JSON,count:Boolean): All_SEGUIMINETO
        read_seguimineto(find:JSON): SEGUIMINETO!
        count_seguimineto(find:JSON):Int
    }
    extend type Mutation {
        create_seguimineto(seguimineto:SEGUIMINETOInput): SEGUIMINETO
        update_seguimineto(id:ID!,seguimineto:SEGUIMINETOInput): SEGUIMINETO
        delete_seguimineto(id:ID!): String
    }
`;