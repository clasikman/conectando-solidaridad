import schema from './schema.js';
import resolvers from './resolvers.js';

export default {schema,resolvers};