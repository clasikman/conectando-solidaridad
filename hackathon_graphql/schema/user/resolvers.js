import jwt from 'jsonwebtoken';
import bcrypt from 'bcryptjs';
import { send_error, auth_required, get_all, get_document, makeHeaven } from '../../utils/index.js';
import User from '../../models/auth/User.js';

export default {
    User: {
        async all_permissions(user) {
            const permissions=await user.get_all_permissions(user.id);
            return permissions;
        }
    },
    Query: {
        async me(_,__,{currentUser},info) {
            auth_required(currentUser,[]);
            const user = await get_document(User,currentUser.id,info);
            return user;
        },
        async all_user(_,args,{currentUser},info) {
            auth_required(currentUser,['user_group'],true);
            const query=await get_all(User,args,info);
            return query;
        },
        async read_user(__, {find},{currentUser},info) {
            auth_required(currentUser,['read_user'],true);
            const document=await get_document(User,find,info);
            return document;
        },
        async count_user(_, {find},{currentUser}) {
            auth_required(currentUser,['read_user'],true);
            const count=await User.countDocuments({...find}).exec();
            return count;
        },
    },
    Mutation: {
        async create_user(__, {user},{currentUser}) {
            auth_required(currentUser,['create_user'],true);
            try {
                user.password=await bcrypt.hash(user.password, 10);
                user.last_user=currentUser.id;
                var query=await new User(user).save();
                return query;
            } catch(error) {
                console.log(error);
                send_error('DATOS INVÁLIDOS',error.errors.name.name,error.errors.name);
            }
        },
        async update_user(__,{id,user},{currentUser}) {
            auth_required(currentUser,['update_user'],true);
            try {
                user.last_user=currentUser.id;
                if (user.password) user.password=await bcrypt.hash(user.password, 10);
                const document=await User.findByIdAndUpdate(id,{
                    $set:user
                },{new:true});
                return document;
            } catch (error) {
                console.log(error);
                send_error('DATOS INVÁLIDOS',error.errors.name.name,error.errors.name);
            }
        },
        async delete_user(__,{id},{currentUser}) {
            auth_required(currentUser,['delete_user'],true);
            try {
                const user=await User.findByIdAndDelete(id);
                return 'User delete';
            } catch (error) {
                console.log(error);
                send_error("IDENTIFICADOR INVÁLIDO",error.errors.name.name,error.errors.name);
            }
        },
        async login(__,{username,password}) {
            var message="CREDENCIALES INVALIDAS"
            var code="INVALID_CREDENTIALS2"
            try {
                const user = await User.findOne({ username }).exec();
                if (!user) return send_error('CREDENCIALES INVALIDAS','INVALID_CREDENTIALS1');
                if (user && !user.is_active) {
                    message='USUARIO DESHABILITADO';
                    code='USER_DISABLED';
                    send_error('USUARIO DESHABILITADO','USER_DISABLED');
                }
                if (user && bcrypt.compareSync(password,user.password)) {
                    user.last_login=new Date();
                    user.save();
                    const permissions=await user.get_all_permissions();
                    var token=jwt.sign({
                        id: user.id,
                        permissions,
                        is_admin: user.is_admin
                    },process.env.JWT_SECRET,{
                        expiresIn: process.env.JWT_TIME
                    });
                    token=makeHeaven(7)+token+makeHeaven(7);
                    return { user: user, token};
                } else {
                    send_error('CREDENCIALES INVALIDAS','INVALID_CREDENTIALS2');
                }
            } catch (error) {
                console.log(error);
                send_error(message,code);
            }
        },
        refreshToken(_,__,{currentUser}) {
            auth_required(currentUser,[]);
            var token=jwt.sign({
                id: currentUser.id,
                permissions: currentUser.permissions,
                is_admin: currentUser.is_admin
            },process.env.JWT_SECRET,{
                expiresIn: process.env.JWT_TIME
            });
            token=makeHeaven(7)+token+makeHeaven(7);
            return token;
        },
        async update_profile(__,{user},{currentUser}) {
            auth_required(currentUser,[]);
            try {
                user.last_user=currentUser.id;
                if (user.password) user.password=await bcrypt.hash(this.password, 10);
                const document=await User.findByIdAndUpdate(currentUser.id,{
                    $set:user
                },{new:true});
                return document;
            } catch (error) {
                console.log(error);
                send_error('DATOS INVÁLIDOS',error.errors.name.name,error.errors.name);
            }
        },
    }
}