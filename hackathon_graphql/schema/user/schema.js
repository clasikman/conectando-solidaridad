export default `#graphql
    scalar Date
    type User {
        id: ID
        name: String
        username: String
        email: String
        groups: [Group]!
        permissions: [Permission]!
        all_permissions: [String]!
        is_admin: Boolean
        is_active: Boolean
        photo_url: String
        last_login: Date
    }
    input UserInput {
        name: String
        username: String
        email: String
        password: String
        groups: [ID]
        permissions: [ID]
        is_admin: Boolean
        is_active: Boolean
        photo_url: String
    }
    type All_User {
        list: [User]!
        count: Int
    }
    type LoginResponse {
        user: User
        token: String!
    }
    extend type Query {
        me: User!
        all_user(find:JSON,page:Int,limit:Int,sort:JSON,count:Boolean): All_User
        read_user(find:JSON): User!
        count_user(find:JSON):Int
    }
    extend type Mutation {
        create_user(user:UserInput): User
        update_user(id:ID!,user:UserInput): User
        delete_user(id:ID!): String
        login(username:String!,password:String!): LoginResponse
        refreshToken: String
        update_profile: User
    }
`;