import { send_error, auth_required, get_all, get_document } from '../../utils/index.js';
import USUARIOS from '../../models/usuarios/USUARIOS.js';

export default {                                                                                         
    Query: {
        async all_usuarios(_,args,{currentUser},info) {
            auth_required(currentUser,['read_usuarios']);
            const documents=await get_all(USUARIOS,args,info);
            return documents;
        },
        async read_usuarios(__,{find},{currentUser},info) {
            auth_required(currentUser,['read_usuarios']);
            const document=await get_document(USUARIOS,find,info);
            return document;
        },
    },
    Mutation: {
        async create_usuarios(__, {usuarios},{currentUser}) {
            auth_required(currentUser,['create_usuarios']);
            try {
                usuarios.last_user=currentUser.id;
                
                var query=await new USUARIOS(usuarios).save()
                return query;
            } catch(error) {
                console.log(error);
                send_error('DATOS INVÁLIDOS',error.errors.name.name,error.errors.name);
            }
        },
        async update_usuarios(__,{id,usuarios},{currentUser}) {
            auth_required(currentUser,['update_usuarios']);
            try {
                usuarios.last_user=currentUser.id;
                const document=await USUARIOS.findByIdAndUpdate(id,{
                    $set:usuarios
                },{new:true});
                return document;
            } catch (error) {
                console.log(error);
                send_error('DATOS INVÁLIDOS',error.errors.name.name,error.errors.name);
            }
        },
        async delete_usuarios(__,{id},{currentUser}) {
            auth_required(currentUser,['delete_usuarios']);
            try {
                const usuarios=await USUARIOS.findByIdAndDelete(id)
                return 'USUARIOS delete';
            } catch (error) {
                console.log(error);
                send_error("IDENTIFICADOR INVÁLIDO",error.errors.name.name,error.errors.name);
            }
        },
    }
}