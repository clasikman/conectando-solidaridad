export default `#graphql
    type USUARIOS {
        id: ID,
        telefono: Int!
        direccion: String!
        latitude: Float
        longitude: Float
        nombre: String!
        usuario: String!
        password: String!
        donaciones: [JSON]
        last_user: User
        createdAt: Date
        updatedAt: Date
    }
    input USUARIOSInput {
        telefono: Int!
        direccion: String!
        latitude: Float
        longitude: Float
        nombre: String!
        usuario: String!
        password: String!
        donaciones: [JSON]
    }
    type All_USUARIOS {
        list: [USUARIOS]!
        count: Int
    }
    extend type Query {
        all_usuarios(find:JSON,page:Int,limit:Int,sort:JSON,count:Boolean): All_USUARIOS
        read_usuarios(find:JSON): USUARIOS!
        count_usuarios(find:JSON):Int
    }
    extend type Mutation {
        create_usuarios(usuarios:USUARIOSInput): USUARIOS
        update_usuarios(id:ID!,usuarios:USUARIOSInput): USUARIOS
        delete_usuarios(id:ID!): String
    }
`;