import { connect } from "mongoose";
import { Schema, model } from 'mongoose';

const seqSchema=new Schema({
    name: Schema.Types.String,
    value: {
        type: Schema.Types.Number,
        default: 0
    }
});
const Seq=model('Seq',seqSchema);

export async function connectDB() {
    const mongo_url=process.env.NODE_ENV == "production" || process.env.NODE_ENV == "test" ? process.env.MONGODB_URI_ATLAS : process.env.MONGODB_URI;
    try {
        await connect(mongo_url,{
            dbName: process.env.DATABASE_NAME
        });
        console.log("MongoDB Connected");
    } catch(error) {
        console.log(error);
    }
}

export async function autoIncrement(name) {
    const seq=await Seq.findOneAndUpdate({name},{
        $inc:{value:1}
    },{new:true});
    if (seq) return seq.value;
    const seqNew=new Seq({
        name,
        value:1
    });
    await seqNew.save();
    return seqNew.value;
}