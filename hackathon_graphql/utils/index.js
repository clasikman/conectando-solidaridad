import { GraphQLError } from 'graphql';

export function send_error(message,code,extra={}) {
    throw new GraphQLError(message, {
        extensions: {
            code: code,
            ...extra
        },
    });
}

export function auth_required(user,permissions,is_admin=false) {
    if (!user)
        throw new GraphQLError('Inicio de Sesión Requerido', {
            extensions: {
                code: 'UNAUTHENTICATED',
            },
        });
    if (user.is_admin) return;
    permissions.forEach(permission => {
        if (!user.permissions.includes(permission)) {
            throw new GraphQLError('Usted no tiene permiso para realizar esta acción.', {
                extensions: {
                    code: 'UNAUTHORIZED',
                    permission: permission
                },
            });
        } 
    });
    if (is_admin && !user.is_admin)
        throw new GraphQLError('Usted no tiene permiso para realizar esta acción.', {
            extensions: {
                code: 'UNAUTHORIZED',
                permission: "Solo Superadmin",
            },
        });
}

export function get_fields(info) {
    var fieldsInfo=null;
    if (info.fieldNodes)
        fieldsInfo=info.fieldNodes[0].selectionSet.selections;
    else
        fieldsInfo=info.selectionSet.selections;
    var select=[];
    var populates=[];
    fieldsInfo.forEach(field => {
        if (field.name.value!="list") {
            select.push(field.name.value);
            if (field.selectionSet) {
                var temp=get_fields(field);
                var tempQuery={
                    path:field.name.value,
                    select: temp.select,
                };
                if (temp.populates.length>0) {
                    tempQuery.populate=temp.populates;
                }
                populates.push(tempQuery);
            }
        }
    });
    select=select.join(" ");
    return { select, populates }
}

export async function get_all(model,args,info) {
    try {
        var { select, populates }=get_fields(info.fieldNodes[0].selectionSet.selections[0]);
        const {find,page,limit,sort,count}=args;
        var query=model.find({ ...find }).select(select);
        var countQuery=null;
        if (populates.length>0) query.populate(populates);
        if (page && limit) query.skip((page - 1 ) * limit ).limit( limit )
        if (sort) query.sort(sort)
        if (count) countQuery=await model.countDocuments({...find}).exec();
        query=await query.exec();
        return {list:query,count:countQuery};
    } catch (error) {
        console.log(error);
        send_error('REGISTROS NO ENCONTRADOS',error.errors.name.name,error.errors.name);
    };
}

export async function get_document(model,find,info) {
    var { select, populates }=get_fields(info);
    var query=model.findOne({...find}).select(select);
    if (populates.length>0) query.populate(populates);
    query=await query.exec();
    return query;
}

export function makeHeaven(length) {
    var result='';
    var characters='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz012345678';
    var charactersLength=characters.length;
    for ( var i = 0; i < length; i++ )
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    return result;
}