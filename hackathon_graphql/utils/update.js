import path from 'path';
import fs from 'fs';
import {fileURLToPath} from 'url';
import Permission from '../models/auth/Permission.js';
import User from '../models/auth/User.js';
import Group from '../models/auth/Group.js';
import { autoIncrement } from './database.js';
import bcrypt from 'bcryptjs';

const pathModels = new URL('../models', import.meta.url)
const __dirModels = fileURLToPath(pathModels);

const models=[];
function searchModels(startPath, filter) {
    if (!fs.existsSync(startPath)) {
        console.log("no dir ", startPath);
        return;
    }
    var files = fs.readdirSync(startPath);
    for (var i = 0; i < files.length; i++) {
        var filename = path.join(startPath, files[i]);
        var stat = fs.lstatSync(filename);
        if (stat.isDirectory()) {
            searchModels(filename, filter);
        } else if (filename.endsWith(filter)) {
            var name=files[i];
            name=name.replace(".js","");
            models.push(name.toLowerCase());
        };
    };
};

const traductor={
    group:"Grupo",
    user:"Usuario",

    create:"Crear",
    read:"Ver",
    update:"Editar",
    delete:"Eliminar"
};

async function updatePermissions() {
    //MODELOS PARA EXCLUIR EN LA LISTA DE PERMISOS
    const exclude=['permission'];

    searchModels(__dirModels, '.js');
    const permissions=[];
    models.forEach(element => {
        if (exclude.includes(element)) return;
        permissions.push("create_"+element);
        permissions.push("read_"+element);
        permissions.push("update_"+element);
        permissions.push("delete_"+element);
    });
    console.log(permissions);
    const permissions_db=await Permission.find();
    
    //delete
    permissions_db.forEach(async (per) => {
        if (!permissions.includes(per.codename)) {
            await User.updateMany(
                {}, 
                { $pull: { permissions: per.id } },
            );
            await Group.updateMany(
                {}, 
                { $pull: { permissions: per.id } },
            );
            await Permission.findByIdAndDelete(per.id);
        }
    });

    //create
    permissions.forEach(async(codename) => {
        var find=null;
        permissions_db.forEach(element2 => {
            if (element2.codename==codename) find=element2;
        });
        if(!find) {
            var [action,model]=codename.split('_');
            action=Object.keys(traductor).includes(action)?traductor[action]:action;
            model=Object.keys(traductor).includes(model)?traductor[model]:model;
            model=model.charAt(0).toUpperCase() + model.slice(1);
            const name=action+" "+model;
            const number=await autoIncrement("permission");
            const newPer=new Permission({
                codename,
                name,
                number
            });
            await newPer.save();
        }
    });
}

export async function createAdminUser() {
    const password=await bcrypt.hash("admin1234", 10);
    const user=await User.findOne({username:"admin"}).exec();
    console.log(user);
    if (!user)
        await new User({
            name: "ADMINISTRADOR",
            username: "admin",
            password,
            email: "admin@admin.com",
            is_admin: true,
            is_active: true
        }).save();
}

export async function updatePermission() {
    try{
        await updatePermissions();
    } catch (error) {
        console.error(error);
    }
}