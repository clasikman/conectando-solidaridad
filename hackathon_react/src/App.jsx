import React, { useContext, useState } from 'react'
import './App.css'
import { Outlet } from 'react-router-dom'
import Loading from './components/Loading'
import { StoreContext } from './context/store'

function App() {
  const store = useContext(StoreContext);
  
  return (
    <React.Fragment>
      {store.loading && <Loading />}
      <Outlet/>
    </React.Fragment>
  )
}

export default App;
