import { Fragment, useEffect } from 'react'
import { Menu, Transition } from '@headlessui/react'
import { EllipsisVerticalIcon } from '@heroicons/react/20/solid'
import { useLazyQuery } from "@apollo/client";
import { useRef, useState } from "react";
import { jsPDF } from "jspdf";
import logo from '@/assets/logo.png';
import * as XLSX from 'xlsx/xlsx.mjs';
import Select from "../Select1";
import BadgeBool from '../BadgeBool';

export default function ButtonsReport({title,query,find,sort,fields,all_list,defaultFields=[]}) {
    const htmlReport = useRef(null);
    const optionsRef = useRef(null);
    const [cantidad,setCantidad]=useState("100 Registros");
    const [typeReport, setTypeReport]=useState("");
    const [fieldsReport, setFieldsReport]=useState([]);
    const [lastFind,setLastFind] = useState(-1);
    const [lastSort,setLastSort] = useState(-1);
    const [lastCantidad,setLastCantidad] = useState(-1);
    
    const [get_dates, {called,loading,error,data,refetch}]=useLazyQuery(query);
    function reportPDF() {
        var doc = new jsPDF();
         var divContents = htmlReport.current.innerHTML;
         doc.html(divContents, {
            callback: function(doc) {
                doc.save(`${title}.pdf`);
            },
            x: 15,
            y: 15,
            autoPaging:'text',
            width: 170, //target width in the PDF document
            windowWidth: 650 //window width in CSS pixels
        });
    }
    function reportExcel() {
        var workbook = XLSX.utils.book_new();
        var dates=[];
        var headersRow=['Nro'];
        fields.forEach(element => {
            if (fieldsReport.includes(element.id)) headersRow.push(element.name);
        });
        dates.push(headersRow);
        if (data?.[all_list]?.list) {
            var count=1;
            data[all_list].list.forEach(doc => {
                var row=[count.toString()];
                fieldsReport.forEach(col => {
                    row.push(doc[col]);
                });
                dates.push(row);
                count++;
            });
        }
        var worksheet = XLSX.utils.aoa_to_sheet(dates);
        workbook.SheetNames.push("Hoja1");
        workbook.Sheets["Hoja1"] = worksheet;
        XLSX.writeString(workbook, `${title}.xlsx`);
    }
    function reportPrint() {
        var divContents = htmlReport.current.innerHTML;
        var a = window.open('', '', 'height=600, width=900');
        a.document.write('<html>');
        a.document.write('<body >');
        a.document.write(divContents);
        a.document.write('</body></html>');
        a.document.close();
        a.print();
    }
    function getReport(value) {
        setTypeReport(value);
        if (!loading) {
            if (find!=lastFind || sort!=lastSort || parseInt(cantidad)!=lastCantidad) {
                console.log(find!=lastFind);
                console.log(sort!=lastSort);
                console.log(cantidad!=lastCantidad,cantidad,lastCantidad);
                setLastFind(find);
                setLastSort(sort);
                setLastCantidad(parseInt(cantidad));
                if (!called)
                    get_dates({variables:{
                        find,
                        page:1,
                        limit:parseInt(cantidad),
                        sort:sort,
                        count:false
                    },fetchPolicy: "no-cache"});
                else {
                    refetch({
                        find,
                        page:1,
                        limit:parseInt(cantidad),
                        sort:sort,
                        count:false
                    });
                }
            } else {
                if(value=="PDF") reportPDF();
                else if (value=="EXCEL") reportExcel();
                else if (value=="PRINT") reportPrint();
            }
        }
    }
    function handlerCheck(e) {
        if(!fieldsReport.includes(e.target.value))
            setFieldsReport([...fieldsReport,e.target.value]);
        else {
            var temp=[];
            fieldsReport.forEach(element => {
                if (element!=e.target.value) temp.push(element);
            });
            setFieldsReport(temp);
        }
        //e.preventDefault();
        //e.stopPropagation();
    }
    function getRowField(value) {
        if (typeof value == "boolean")
            return <BadgeBool value={value} />
        else if (typeof value == "object")
            return value[Object.keys(value)[0]]
        else
            return value
    }
    useEffect(()=> {
        if (data?.[all_list] && data[all_list].list) {
            if(typeReport=="PDF") reportPDF();
            else if (typeReport=="EXCEL") reportExcel();
            else if (typeReport=="PRINT") reportPrint();
        }
        if (data?.error) {
            console.log(data.error);
        }
    },[data])
    useEffect(()=> {
        setFieldsReport(defaultFields);
    },[])
    return (
        <>
            <span className="isolate inline-flex rounded-md shadow-sm">
                <button
                    type="button"
                    className="relative inline-flex items-center rounded-l-md border border-gray-300 bg-white px-4 py-2 text-sm font-medium text-gray-800 hover:bg-gray-50 focus:z-10 focus:border-indigo-500 focus:outline-none focus:ring-1 focus:ring-indigo-500"
                    onClick={()=>getReport("PDF")}
                >
                    PDF
                </button>
                <button
                    type="button"
                    className="relative -ml-px inline-flex items-center border border-gray-300 bg-white px-4 py-2 text-sm font-medium text-gray-800 hover:bg-gray-50 focus:z-10 focus:border-indigo-500 focus:outline-none focus:ring-1 focus:ring-indigo-500"
                    onClick={()=>getReport("EXCEL")}
                >
                    Excel
                </button>
                <button
                    type="button"
                    className="relative -ml-px inline-flex items-center rounded-none border border-gray-300 bg-white px-4 py-2 text-sm font-medium text-gray-800 hover:bg-gray-50 focus:z-10 focus:border-indigo-500 focus:outline-none focus:ring-1 focus:ring-indigo-500"
                    onClick={()=>getReport("PRINT")}
                >
                    Imprimir
                </button>
                <Menu as="div" className="relative z-0 -ml-px inline-flex items-center border border-gray-300 bg-white px-1 py-2 text-sm font-medium text-gray-800 hover:bg-gray-50 focus:z-10 focus:border-indigo-500 focus:outline-none focus:ring-1 focus:ring-indigo-500">
                    <div>
                        <Menu.Button ref={optionsRef} className="flex items-center rounded-full text-gray-400 hover:text-gray-600 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 focus:ring-offset-gray-100">
                        <span className="sr-only">Open options</span>
                        <EllipsisVerticalIcon className="h-5 w-5" aria-hidden="true" />
                        </Menu.Button>
                    </div>

                    <Transition
                        as={Fragment}
                        enter="transition ease-out duration-100"
                        enterFrom="transform opacity-0 scale-95"
                        enterTo="transform opacity-100 scale-100"
                        leave="transition ease-in duration-75"
                        leaveFrom="transform opacity-100 scale-100"
                        leaveTo="transform opacity-0 scale-95"
                    >
                        <Menu.Items className="absolute top-10 w-56 h-min rounded-md bg-gray-50 shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none">
                        <div className="py-1">
                            {fields.map((field, fieldIdx) => (
                            <Menu.Item key={fieldIdx}>
                                {() => (
                                    <div className='block px-4 py-2 text-sm flex justify-between text-gray-800'>
                                        <div className="min-w-0 flex-1 text-sm">
                                        <label htmlFor={`field-${field.id}`} className="select-none font-medium text-gray-700">
                                            {field.name}
                                        </label>
                                        </div>
                                        <div className="ml-3 flex h-5 items-center">
                                        <input
                                            id={`field-${field.id}`}
                                            name={`field-${field.id}`}
                                            type="checkbox"
                                            className="h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-500"
                                            value={field.id}
                                            checked={fieldsReport.includes(field.id)}
                                            onChange={handlerCheck}
                                        />
                                        </div>
                                    </div>
                                )}
                            </Menu.Item>
                            ))}
                        </div>
                        </Menu.Items>
                    </Transition>
                </Menu>
                <Select 
                    className="rounded-none rounded-r-md border border-gray-300 -ml-px w-max text-gray-800"
                    options={["Todo","50 Registros","100 Registros","500 Registros","1000 Registros"]} 
                    value={cantidad} setValue={setCantidad} 
                />
            </span>
            <div style={{display:'none'}}>
                <div ref={htmlReport}>
                    <img src={logo} className="h-32" style={{height:"10vh"}} />
                    <h1 className="text-3xl text-center">{title}</h1>
                    <h2>Fecha: {new Date(Date.now()).toLocaleDateString()}</h2>
                    <table className='mt-10 table-auto min-w-full divide-y divide-gray-300'>
                        <thead className="bg-gray-50">
                            <tr>
                                <th scope="col" className="py-3.5 pl-4 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-6">
                                    Nro
                                </th>
                                {fields.map((field,index) => 
                                    fieldsReport.includes(field.id) &&
                                        <th key={index} scope="col" className="px-3 py-3.5 text-left text-sm font-semibold text-gray-900">
                                            {field.name}
                                        </th>
                                )}
                            </tr>
                        </thead>
                        <tbody className="divide-y divide-gray-200 bg-white">
                            {data?.[all_list]?.list && data[all_list].list.map((doc,index)=> 
                                <tr>
                                    <td className="whitespace-nowrap py-4 pl-4 pr-3 text-sm font-medium text-gray-900 sm:pl-6">{index+1}</td>
                                    {fieldsReport.map((col)=>
                                        <td className="whitespace-nowrap px-3 py-4 text-sm text-gray-500">
                                            {getRowField(doc[col])}
                                        </td>
                                    )}
                                </tr>
                            )}
                        </tbody>
                    </table>
                </div>
            </div>
        </>
    )
}
  