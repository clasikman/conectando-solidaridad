import { Fragment, useContext, useEffect, useState } from 'react'
import { Dialog, Menu, Transition, Disclosure } from '@headlessui/react'
import { Link, Outlet } from 'react-router-dom'
import { StoreContext } from '@/context/store'
import Logo from '@/assets/logo.png';
import { 
  XMarkIcon,
  Bars3CenterLeftIcon,
  BellIcon,
  ChevronDownIcon,
} from '@heroicons/react/20/solid';
import Icon from '@/components/Icon';


const navigation = [
  { name: 'Dashboard', icon: "HomeIcon", current: false, href: '/dashboard' },
    {
        name: 'USUARIOS',
        icon: "UserGroupIcon",
        current: false,
        permissions: [],
        children: [
            { name: 'USUARIOS', href: 'usuarios/list', icon: 'UsersIcon', permissions: ['read_usuarios'] },
            { name: 'DONACIONES', href: 'donaciones/list', icon: 'PhotoIcon', permissions: ['read_donaciones'] },
            { name: 'SEGUIMINETO', href: 'seguimineto/list', icon: 'PhotoIcon', permissions: ['read_seguimineto'] },
//children
        ],
    },
    {
        name: 'ENTIDADES',
        icon: "BuildingOffice2Icon",
        current: false,
        permissions: [],
        children: [
            { name: 'ENTIDAD', href: 'entidad/list', icon: 'BuildingOfficeIcon', permissions: ['read_entidad'] },
            { name: 'DONACIONES FISICAS', href: 'registro_donaciones_fisicas/list', icon: 'CubeTransparentIcon', permissions: ['read_registro_donaciones_fisicas'] },
//children
        ],
    },
    {
        name: 'TRANSPORTE',
        icon: "TruckIcon",
        current: false,
        permissions: [],
        children: [
            { name: 'Empresa Transporte', href: 'empresa_transporte/list', icon: 'PhotoIcon', permissions: ['read_empresa_transporte'] },
//children
        ],
    },
    {
        name: 'PARAMETRICAS',
        icon: "PhotoIcon",
        current: false,
        permissions: [],
        children: [
            { name: 'CATEGORIAS', href: 'categorias/list', icon: 'NewspaperIcon', permissions: ['read_categorias'] },
            { name: 'ITEMS DONACION', href: 'items_donacion/list', icon: 'CheckBadgeIcon', permissions: ['read_items_donacion'] },
//children
        ],
    },
//sectionMenu
]
const adminNavigation = [
    {
        name: 'Gestion de Usuarios',
        icon: "UsersIcon",
        current: false,
        permissions: [],
        children: [
            { name: 'Usuarios', href: 'user/list', icon: "UserIcon", permissions: ["read_user"] },
            { name: 'Grupos/Permisos', href: 'group/list',icon: "KeyIcon", permissions: ["read_group"] },
        ],
    },
]
function classNames(...classes) {
  return classes.filter(Boolean).join(' ')
}

export default function Dashboard() {
  const [sidebarOpen, setSidebarOpen] = useState(false)
  const store=useContext(StoreContext);

  useEffect(()=> {
    navigation.forEach(element => {
      if(element.children) {
        var permissions=[];
        element.children.forEach(element2 => {
          permissions.push(element2);
        });
        element.permissions=permissions;
      }
    });
    adminNavigation.forEach(element => {
      if(element.children) {
        var permissions=[];
        element.children.forEach(element2 => {
          permissions.push(element2);
        });
        element.permissions=permissions;
      }
    });
  },[])

  return (
    <>
      {/*
        This example requires updating your template:

        ```
        <html class="h-full bg-gray-100">
        <body class="h-full">
        ```
      */}
      <div className="min-h-full">
        <Transition.Root show={sidebarOpen} as={Fragment}>
          <Dialog as="div" className="relative z-40 lg:hidden" onClose={setSidebarOpen}>
            <Transition.Child
              as={Fragment}
              enter="transition-opacity ease-linear duration-300"
              enterFrom="opacity-0"
              enterTo="opacity-100"
              leave="transition-opacity ease-linear duration-300"
              leaveFrom="opacity-100"
              leaveTo="opacity-0"
            >
              <div className="fixed inset-0 bg-gray-600 bg-opacity-75" />
            </Transition.Child>

            <div className="fixed inset-0 z-40 flex">
              <Transition.Child
                as={Fragment}
                enter="transition ease-in-out duration-300 transform"
                enterFrom="-translate-x-full"
                enterTo="translate-x-0"
                leave="transition ease-in-out duration-300 transform"
                leaveFrom="translate-x-0"
                leaveTo="-translate-x-full"
              >
                <Dialog.Panel className="relative flex w-full max-w-xs flex-1 flex-col bg-orange-700 pt-5 pb-4">
                  <Transition.Child
                    as={Fragment}
                    enter="ease-in-out duration-300"
                    enterFrom="opacity-0"
                    enterTo="opacity-100"
                    leave="ease-in-out duration-300"
                    leaveFrom="opacity-100"
                    leaveTo="opacity-0"
                  >
                    <div className="absolute top-0 right-0 -mr-12 pt-2">
                      <button
                        type="button"
                        className="ml-1 flex h-10 w-10 items-center justify-center rounded-full focus:outline-none focus:ring-2 focus:ring-inset focus:ring-white"
                        onClick={() => setSidebarOpen(false)}
                      >
                        <span className="sr-only">Close sidebar</span>
                        <XMarkIcon className="h-6 w-6 text-white" aria-hidden="true" />
                      </button>
                    </div>
                  </Transition.Child>
                  <div className="flex flex-shrink-0 items-center px-4">
                    <img
                      className="h-32 w-auto border-2"
                      src={Logo}
                      alt="logo"
                    />
                  </div>
                  <nav
                    className="mt-5 h-full flex-shrink-0 divide-y divide-orange-800 overflow-y-auto"
                    aria-label="Sidebar"
                  >
                    <div className="space-y-1 px-2">
                      <nav className="flex-1 space-y-1" aria-label="Sidebar">
                        {navigation.map((item) =>
                          (!item.children && store.checkPermissions(item?.permissions)) ? (
                            <div key={item.name}>
                              <Link
                                to={item.href}
                                className={classNames(
                                  item.current
                                    ? 'bg-orange-800 text-white'
                                    : 'text-orange-100 hover:text-white hover:bg-orange-600',
                                  'group flex items-center px-2 py-2 text-base font-medium rounded-md'
                                )}
                              >
                                <Icon 
                                  name={item.icon}
                                  className="mr-4 h-6 w-6 flex-shrink-0 text-orange-200"
                                  aria-hidden="true"
                                />
                                {item.name}
                              </Link>
                            </div>
                          ) : ( (store.checkPermissionsMenu(item?.permissions)) &&
                            <Disclosure as="div" key={item.name} className="space-y-1">
                              {({ open }) => (
                                <>
                                  <Disclosure.Button
                                    className={classNames(
                                      item.current
                                        ? 'bg-orange-800 text-white'
                                        : 'text-orange-100 hover:text-white hover:bg-orange-600',
                                      'group w-full flex items-center pl-2 pr-1 py-2 text-left font-medium rounded-md focus:outline-none focus:ring-2 focus:ring-white'
                                    )}
                                    
                                  >
                                    <Icon
                                      name={item.icon}
                                      className={classNames(
                                        open 
                                          ? 'text-white' 
                                          : 'text-gray-300',
                                        "mr-4 h-6 w-6 flex-shrink-0 text-orange-200"
                                      )}
                                      aria-hidden="true"
                                    />
                                    <span className="flex-1">{item.name}</span>
                                    <svg
                                      className={classNames(
                                        open ? 'text-gray-400 rotate-90' : 'text-gray-300',
                                        'ml-3 h-5 w-5 flex-shrink-0 transform transition-colors duration-150 ease-in-out group-hover:text-white'
                                      )}
                                      viewBox="0 0 20 20"
                                      aria-hidden="true"
                                    >
                                      <path d="M6 6L14 10L6 14V6Z" fill="currentColor" />
                                    </svg>
                                  </Disclosure.Button>
                                  <Disclosure.Panel className="space-y-1">
                                    {item.children.map((subItem) => (
                                      (store.checkPermissions(subItem?.permissions)) &&
                                      <Link key={subItem.name} to={subItem.href}>
                                        <Disclosure.Button
                                          as="a"
                                          className="group flex w-full items-center rounded-md py-2 pl-8 pr-2 font-medium text-white hover:text-black"
                                        >
                                            <Icon
                                              name={subItem.icon}
                                              className="text-orange-200 group-hover:text-black mr-4 h-6 w-6 flex-shrink-0"
                                              aria-hidden="true"
                                            />
                                            {subItem.name}
                                        </Disclosure.Button>
                                      </Link>
                                    ))}
                                  </Disclosure.Panel>
                                </>
                              )}
                            </Disclosure>
                          )
                        )}
                      </nav>
                    </div>
                    <div className="mt-6 pt-6">
                      <div className="space-y-1 px-2">
                        {store.user.is_admin && 
                          <nav className="flex-1 space-y-1" aria-label="Sidebar">
                            {adminNavigation.map((item) =>
                              (!item.children && store.checkPermissions(item?.permissions)) ? (
                                <div key={item.name}>
                                  <Link
                                    to={item.href}
                                    className={classNames(
                                      item.current
                                        ? 'bg-orange-800 text-white'
                                        : 'text-orange-100 hover:text-white hover:bg-orange-600',
                                      'group flex items-center px-2 py-2 text-base font-medium rounded-md'
                                    )}
                                  >
                                    <Icon
                                      name={item.icon}
                                      className="mr-4 h-6 w-6 flex-shrink-0 text-orange-200"
                                      aria-hidden="true"
                                    />
                                    {item.name}
                                  </Link>
                                </div>
                              ) : ( store.checkPermissionsMenu(item?.permissions) &&
                                <Disclosure as="div" key={item.name} className="space-y-1">
                                  {({ open }) => (
                                    <>
                                      <Disclosure.Button
                                        className={classNames(
                                          item.current
                                            ? 'bg-orange-800 text-white'
                                            : 'text-orange-100 hover:text-white hover:bg-orange-600',
                                          'group w-full flex items-center pl-2 pr-1 py-2 text-left font-medium rounded-md focus:outline-none focus:ring-2 focus:ring-white'
                                        )}
                                        
                                      >
                                        <Icon
                                          name={item.icon}
                                          className={classNames(
                                            open 
                                              ? 'text-white' 
                                              : 'text-gray-300',
                                            "mr-4 h-6 w-6 flex-shrink-0 text-orange-200"
                                          )}
                                          aria-hidden="true"
                                        />
                                        <span className="flex-1">{item.name}</span>
                                        <svg
                                          className={classNames(
                                            open ? 'text-gray-400 rotate-90' : 'text-gray-300',
                                            'ml-3 h-5 w-5 flex-shrink-0 transform transition-colors duration-150 ease-in-out group-hover:text-white'
                                          )}
                                          viewBox="0 0 20 20"
                                          aria-hidden="true"
                                        >
                                          <path d="M6 6L14 10L6 14V6Z" fill="currentColor" />
                                        </svg>
                                      </Disclosure.Button>
                                      <Disclosure.Panel className="space-y-1">
                                        {item.children.map((subItem) => (
                                          (store.checkPermissions(subItem?.permissions)) &&
                                          <Link key={subItem.name} to={subItem.href}>
                                            <Disclosure.Button
                                              as="a"
                                              className="group flex w-full items-center rounded-md py-2 pl-8 pr-2 font-medium text-white hover:text-black"
                                            >
                                                <Icon
                                                  name={subItem.icon}
                                                  className="text-orange-200 group-hover:text-black mr-4 h-6 w-6 flex-shrink-0"
                                                  aria-hidden="true"
                                                />
                                                {subItem.name}
                                            </Disclosure.Button>
                                          </Link>
                                        ))}
                                      </Disclosure.Panel>
                                    </>
                                  )}
                                </Disclosure>
                              )
                            )}
                          </nav>
                        }
                      </div>
                    </div>
                  </nav>
                </Dialog.Panel>
              </Transition.Child>
              <div className="w-14 flex-shrink-0" aria-hidden="true">
                {/* Dummy element to force sidebar to shrink to fit close icon */}
              </div>
            </div>
          </Dialog>
        </Transition.Root>

        {/* Static sidebar for desktop */}
        <div className="hidden lg:fixed lg:inset-y-0 lg:flex lg:w-64 lg:flex-col">
          {/* Sidebar component, swap this element with another sidebar if you like */}
          <div className="flex flex-grow flex-col overflow-y-auto bg-orange-700 pt-5 pb-4">
            <div className="flex flex-shrink-0 items-center px-4 w-full ml-2">
              <img
                className="h-48 w-auto"
                src={Logo}
                alt="logo"
              />
            </div>
            <nav className="mt-5 flex flex-1 flex-col divide-y divide-orange-800 overflow-y-auto" aria-label="Sidebar">
              <div className="space-y-1 px-2">
                <nav className="flex-1 space-y-1" aria-label="Sidebar">
                  {navigation.map((item) =>
                    (!item.children && store.checkPermissions(item?.permissions)) ? (
                      <div key={item.name}>
                        <Link
                          to={item.href}
                          className={classNames(
                            item.current
                              ? 'bg-orange-800 text-white'
                              : 'text-orange-100 hover:text-white hover:bg-orange-600',
                            'group flex items-center px-2 py-2 text-base font-medium rounded-md'
                          )}
                        >
                          <Icon
                            name={item.icon}
                            className="mr-4 h-6 w-6 flex-shrink-0 text-orange-200"
                            aria-hidden="true"
                          />
                          {item.name}
                        </Link>
                      </div>
                    ) : ( store.checkPermissionsMenu(item?.permissions) &&
                      <Disclosure as="div" key={item.name} className="space-y-1">
                        {({ open }) => (
                          <>
                            <Disclosure.Button
                              className={classNames(
                                item.current
                                  ? 'bg-orange-800 text-white'
                                  : 'text-orange-100 hover:text-white hover:bg-orange-600',
                                'group w-full flex items-center pl-2 pr-1 py-2 text-left font-medium rounded-md focus:outline-none focus:ring-2 focus:ring-white'
                              )}
                              
                            >
                              <Icon
                                name={item.icon}
                                className={classNames(
                                  open 
                                    ? 'text-white' 
                                    : 'text-gray-300',
                                  "mr-4 h-6 w-6 flex-shrink-0 text-orange-200"
                                )}
                                aria-hidden="true"
                              />
                              <span className="flex-1">{item.name}</span>
                              <svg
                                className={classNames(
                                  open ? 'text-gray-400 rotate-90' : 'text-gray-300',
                                  'ml-3 h-5 w-5 flex-shrink-0 transform transition-colors duration-150 ease-in-out group-hover:text-white'
                                )}
                                viewBox="0 0 20 20"
                                aria-hidden="true"
                              >
                                <path d="M6 6L14 10L6 14V6Z" fill="currentColor" />
                              </svg>
                            </Disclosure.Button>
                            <Disclosure.Panel className="space-y-1">
                              {item.children.map((subItem) => (
                                (store.checkPermissions(subItem?.permissions)) &&
                                <Link key={subItem.name} to={subItem.href}>
                                  <Disclosure.Button
                                    as="a"
                                    className="group flex w-full items-center rounded-md py-2 pl-8 pr-2 font-medium text-white hover:text-black"
                                  >
                                      <Icon
                                        name={subItem.icon}
                                        className="text-orange-200 group-hover:text-black mr-4 h-6 w-6 flex-shrink-0"
                                        aria-hidden="true"
                                      />
                                      {subItem.name}
                                  </Disclosure.Button>
                                </Link>
                              ))}
                            </Disclosure.Panel>
                          </>
                        )}
                      </Disclosure>
                    )
                  )}
                </nav>
              </div>
              <div className="mt-6 pt-6">
                <div className="space-y-1 px-2">
                  {store.user.is_admin && 
                    <nav className="flex-1 space-y-1" aria-label="Sidebar">
                      {adminNavigation.map((item) =>
                        (!item.children && store.checkPermissions(item?.permissions)) ? (
                          <div key={item.name}>
                            <Link
                              to={item.href}
                              className={classNames(
                                item.current
                                  ? 'bg-orange-800 text-white'
                                  : 'text-orange-100 hover:text-white hover:bg-orange-600',
                                'group flex items-center px-2 py-2 text-base font-medium rounded-md'
                              )}
                            >
                              <Icon
                                name={item.icon}
                                className="mr-4 h-6 w-6 flex-shrink-0 text-orange-200"
                                aria-hidden="true"
                              />
                              {item.name}
                            </Link>
                          </div>
                        ) : ( store.checkPermissionsMenu(item?.permissions) &&
                          <Disclosure as="div" key={item.name} className="space-y-1">
                            {({ open }) => (
                              <>
                                <Disclosure.Button
                                  className={classNames(
                                    item.current
                                      ? 'bg-orange-800 text-white'
                                      : 'text-orange-100 hover:text-white hover:bg-orange-600',
                                    'group w-full flex items-center pl-2 pr-1 py-2 text-left font-medium rounded-md focus:outline-none focus:ring-2 focus:ring-white'
                                  )}
                                  
                                >
                                  <Icon
                                    name={item.icon}
                                    className={classNames(
                                      open 
                                        ? 'text-white' 
                                        : 'text-gray-300',
                                      "mr-4 h-6 w-6 flex-shrink-0 text-orange-200"
                                    )}
                                    aria-hidden="true"
                                  />
                                  <span className="flex-1">{item.name}</span>
                                  <svg
                                    className={classNames(
                                      open ? 'text-gray-400 rotate-90' : 'text-gray-300',
                                      'ml-3 h-5 w-5 flex-shrink-0 transform transition-colors duration-150 ease-in-out group-hover:text-white'
                                    )}
                                    viewBox="0 0 20 20"
                                    aria-hidden="true"
                                  >
                                    <path d="M6 6L14 10L6 14V6Z" fill="currentColor" />
                                  </svg>
                                </Disclosure.Button>
                                <Disclosure.Panel className="space-y-1">
                                  {item.children.map((subItem) => (
                                    (store.checkPermissions(subItem?.permissions)) &&
                                    <Link key={subItem.name} to={subItem.href}>
                                      <Disclosure.Button
                                        as="a"
                                        className="group flex w-full items-center rounded-md py-2 pl-8 pr-2 font-medium text-white hover:text-black"
                                      >
                                          <Icon
                                            name={subItem.icon}
                                            className="text-orange-200 group-hover:text-black mr-4 h-6 w-6 flex-shrink-0"
                                            aria-hidden="true"
                                          />
                                          {subItem.name}
                                      </Disclosure.Button>
                                    </Link>
                                  ))}
                                </Disclosure.Panel>
                              </>
                            )}
                          </Disclosure>
                        )
                      )}
                    </nav>
                  }
                </div>
              </div>
            </nav>
          </div>
        </div>

        <div className="flex flex-1 flex-col lg:pl-64">
          <div className="flex h-16 flex-shrink-0 border-b border-gray-200 bg-gray-100">
            <button
              type="button"
              className="border-r border-gray-200 px-4 text-gray-400 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-orange-500 lg:hidden"
              onClick={() => setSidebarOpen(true)}
            >
              <span className="sr-only">Open sidebar</span>
              <Bars3CenterLeftIcon className="h-6 w-6" aria-hidden="true" />
            </button>
            {/* Search bar */}
            <div className="w-full flex px-4 sm:px-6 lg:px-8">
              <div className="w-full ml-4 flex justify-end items-center md:ml-6">
                <button
                  type="button"
                  className="rounded-full bg-gray-200 hover:bg-gray-300 p-1 text-gray-400 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-orange-500 focus:ring-offset-2"
                >
                  <span className="sr-only">View notifications</span>
                  <BellIcon className="h-6 w-6" aria-hidden="true" />
                </button>
                {/* Profile dropdown */}
                <Menu as="div" className="relative ml-3">
                  <div>
                    <Menu.Button className="bg-gray-200 flex max-w-xs items-center rounded-full text-sm focus:outline-none focus:ring-2 focus:ring-orange-500 focus:ring-offset-2 lg:rounded-md lg:p-2 lg:hover:bg-gray-300">
                      <img
                        className="h-8 w-8 rounded-full"
                        src="https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80"
                        alt=""
                      />
                      <span className="ml-3 hidden text-sm font-medium text-gray-700 lg:block">
                        <span className="sr-only">Open user menu for </span>{store?.user?.name}
                      </span>
                      <ChevronDownIcon
                        className="ml-1 hidden h-5 w-5 flex-shrink-0 text-gray-400 lg:block"
                        aria-hidden="true"
                      />
                    </Menu.Button>
                  </div>
                  <Transition
                    as={Fragment}
                    enter="transition ease-out duration-100"
                    enterFrom="transform opacity-0 scale-95"
                    enterTo="transform opacity-100 scale-100"
                    leave="transition ease-in duration-75"
                    leaveFrom="transform opacity-100 scale-100"
                    leaveTo="transform opacity-0 scale-95"
                  >
                    <Menu.Items className="absolute right-0 z-10 mt-2 w-48 origin-top-right rounded-md bg-white py-1 shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none">
                      <Menu.Item>
                        {({ active }) => (
                          <a
                            href="#"
                            className={classNames(active ? 'bg-gray-100' : '', 'block px-4 py-2 text-sm text-gray-700')}
                          >
                            Perfil
                          </a>
                        )}
                      </Menu.Item>
                      <Menu.Item>
                        {({ active }) => (
                          <a
                            href="#"
                            className={classNames(active ? 'bg-gray-100' : '', 'block px-4 py-2 text-sm text-gray-700')}
                          >
                            Cerrar Sesión
                          </a>
                        )}
                      </Menu.Item>
                    </Menu.Items>
                  </Transition>
                </Menu>
              </div>
            </div>
          </div>
          <main className="flex-1 pb-8">
            <div className="relative z-0 mt-8">
              <Outlet />
            </div>
          </main>
        </div>
      </div>
    </>
  )
}