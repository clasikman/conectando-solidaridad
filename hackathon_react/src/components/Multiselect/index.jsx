
export default function MultiCheckbox({label,options,field="name",values,setValues,className}) {
    function handlerCheck(e) {
        if(!values.includes(e.target.value))
            setValues([...values,e.target.value]);
        else {
            var temp=[];
            values.forEach(element => {
                if (element!=e.target.value) temp.push(element);
            });
            setValues(temp);
        }
    }
    if (!options || options.length==0) return "No se pasaron las opciones de "+label;
    return  (
        <>
            <label className="block text-sm font-medium text-gray-800 mb-2">
                {label}
            </label>
            <div className={"overflow-y-auto max-h-96 "+className}>
                <ul className="pl-2 pb-2 grid grid-cols-4 gap-x-4 gap-y-6">
                    {options.map((option,idx) => 
                        <li key={option.id ? option.id : option} className="col-span-1">
                            <input 
                                type="checkbox" 
                                className="focus:ring-blue-500 h-4 w-4 text-blue-600 border-gray-300 rounded mr-2"
                                value={option.id ? option.id : option}
                                checked={values.includes(option.id ? option.id : option)}
                                onChange={handlerCheck}
                            />
                            {option.id ? option[field] : option}
                        </li>
                    )}
                </ul>        
            </div>
        </>
    )
}