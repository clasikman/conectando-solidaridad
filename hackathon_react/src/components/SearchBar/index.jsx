import { Fragment, useEffect, useState } from 'react'
import { Listbox, Transition } from '@headlessui/react'
import { CheckIcon, ChevronUpDownIcon } from '@heroicons/react/20/solid'
import Icon from '../Icon';

function classNames(...classes) {
    return classes.filter(Boolean).join(' ')
}

export default function SearchBar({fields=[],setFind,submit}) {
    const [value,setValue]=useState('all');
    const [search,setSearch]=useState();
    const options=[
        {id:"all",name:"Todos los Campos"},
        ...fields
    ];
    useEffect(()=>{
        console.log("search",search);
        if(value=="all") {
            if (!search)
                setFind({});
            else {
                var query=[];
                options.forEach(element => {
                    if (element.id!="all") {
                        var field={};
                        field[element.id]={$regex:search}
                        query.push(field);
                    }
                });
                setFind({$or:query});
            }
        } else {
            var query={};
            query[value]={$regex:search};
            setFind(query);
        }
    },[search,value])

    return (
        <Fragment>
            <div className="flex rounded-md shadow-sm">
                <div className='mt-1 mr-2'>Buscar por:</div>
                <div className="mb-1 w-40 sm:text-sm">
                    <Listbox value={value} onChange={setValue}>
                        {({ open }) => (
                            <>
                            <div className="relative">
                                <Listbox.Button className="bg-gray-50 relative w-full cursor-default rounded-none rounded-l-md border border-gray-300 py-2 pl-3 pr-10 text-left shadow-sm focus:border-indigo-500 focus:outline-none focus:ring-1 focus:ring-indigo-500 sm:text-sm">
                                <span className="block truncate">{options.find(x => x.id == value).name}</span>
                                <span className="pointer-events-none absolute inset-y-0 right-0 flex items-center pr-2">
                                    <ChevronUpDownIcon className="h-5 w-5 text-gray-400" aria-hidden="true" />
                                </span>
                                </Listbox.Button>
                                <Transition
                                show={open}
                                as={Fragment}
                                leave="transition ease-in duration-100"
                                leaveFrom="opacity-100"
                                leaveTo="opacity-0"
                                >
                                <Listbox.Options className="absolute z-10 mt-1 max-h-60 w-60 overflow-auto rounded-md bg-white py-1 text-base shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm">
                                    {options.map((option) => (
                                    <Listbox.Option
                                        key={option.id}
                                        className={({ active }) =>
                                            classNames(
                                                active ? 'text-white bg-indigo-600' : 'text-gray-900',
                                                'relative cursor-default select-none py-2 pl-3 pr-9'
                                            )
                                        }
                                        value={option.id}
                                    >
                                        {({ value, active }) => (
                                        <>
                                            <span className={classNames(value ? 'font-semibold' : 'font-normal', 'block truncate')}>
                                                {option.name}
                                            </span>

                                            {value ? (
                                            <span
                                                className={classNames(
                                                active ? 'text-white' : 'text-indigo-600',
                                                'absolute inset-y-0 right-0 flex items-center pr-4'
                                                )}
                                            >
                                                <CheckIcon className="h-5 w-5" aria-hidden="true" />
                                            </span>
                                            ) : null}
                                        </>
                                        )}
                                    </Listbox.Option>
                                    ))}
                                </Listbox.Options>
                                </Transition>
                            </div>
                            </>
                        )}
                    </Listbox>
                </div>
                <div>
                    <input
                        type="text"
                        name="company-website"
                        id="company-website"
                        className="bg-white block w-full min-w-0 flex-1 border-gray-300 px-3 py-2 focus:border-gray-500 focus:ring-gray-500 sm:text-sm"
                        placeholder="Palabra a buscar"
                        value={search}
                        onChange={e => setSearch(e.target.value)}
                    />
                </div>
                <div>
                    <button
                        type="button"
                        className="inline-flex items-center rounded-none rounded-r-md border border-transparent bg-green-600 px-2 py-2 text-sm font-medium text-white shadow-sm hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
                        onClick={submit}
                    >
                        <Icon name="MagnifyingGlassIcon" className="mr-1 h-5 w-5" aria-hidden="true" />
                    </button>
                </div>
            </div>
        </Fragment>
    )
}