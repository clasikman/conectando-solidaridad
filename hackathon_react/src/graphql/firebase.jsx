// Firebase App (the core Firebase SDK) is always required and must be listed first
import * as firebase from "firebase/app";
import { getAuth, signInAnonymously } from "firebase/auth";
import { getStorage, ref, uploadBytes, getDownloadURL  } from "firebase/storage";

const firebaseConfig = {
  apiKey: "AIzaSyCNWGADhRZd_pTjJ3V10kFfsfjKlugPJiU",
  authDomain: "elasadordel-norte.firebaseapp.com",
  projectId: "elasadordel-norte",
  storageBucket: "elasadordel-norte.appspot.com",
  messagingSenderId: "661417810664",
  appId: "1:661417810664:web:f90843676141ce35cdc75d"
};

const firebaseApp=firebase.initializeApp(firebaseConfig);
const storage = getStorage(firebaseApp);
const auth = getAuth();

export async function initStorage() {
  return signInAnonymously(auth)
  .then(() => {
    console.log("Ready Storage!..");
  })
  .catch((error) => {
    const errorCode = error.code;
    const errorMessage = error.message;
    console.log(errorCode,errorMessage);
  });
}

function makeid(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * 
 charactersLength));
   }
   return result;
}

export async function uploadString(file) {
    const storageRef = ref(storage,makeid(15)+".jpg");
    return uploadBytes(storageRef, file).then((snapshot) => {
        return getDownloadURL(storageRef).then((url) => {
            //console.log("***********************");
            //console.log(url);
            return url;
          })
          .catch((error) => {
              return error.message;
            // Handle any errors
          });
    });
}