import { ApolloClient, ApolloLink, HttpLink, InMemoryCache, from  } from '@apollo/client';

const graph_uri=import.meta.env.VITE_NODE_ENV == "production" ? 
    import.meta.env.VITE_GRAPHQL_URI_PRODUCTION : 
    import.meta.env.VITE_GRAPHQL_URI;

const httpLink = new HttpLink({ uri: graph_uri });

const authMiddleware = new ApolloLink((operation, forward) => {
    operation.setContext(({ headers = {} }) => ({
    headers: {
        ...headers,
        authorization: localStorage.getItem('heaven') || null,
    }
    }));

    return forward(operation);
})

const client = new ApolloClient({
    cache: new InMemoryCache(),
    link: from([
        authMiddleware,
        httpLink
    ]),
});

export default client;