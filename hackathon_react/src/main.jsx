import React from 'react';
import ReactDOM from 'react-dom/client';
import { RouterProvider } from "react-router-dom";
import router from './router';
import { ApolloProvider } from '@apollo/client';
import client from './graphql';
import { StoreProvider } from './context/store';
import './index.css';

ReactDOM.createRoot(document.getElementById('root')).render(
  <ApolloProvider client={client}>
    <StoreProvider client={client}>
      <RouterProvider router={router} />    
    </StoreProvider>
  </ApolloProvider>
)