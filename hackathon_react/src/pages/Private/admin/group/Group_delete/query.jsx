import { gql } from "@apollo/client";

export const READ_GROUP=gql`
    query($find:JSON) {
        read_group(find:$find) {
            name
            permissions {
                id
                name
            }
        }
    }
`;

export const DELETE_GROUP=gql`
    mutation($id: ID!) {
        delete_group(id: $id)
    }
`;