import { gql } from "@apollo/client";

export const CREATE_GROUP=gql`
    mutation(
        $name:String,
        $permissions:[ID]!
    ) {
        create_group(
            group:{
                name:$name,
                permissions:$permissions
            }
        ) {
            id
        }
    }   
`;

export const UPDATE_GROUP=gql`
    mutation(
        $id:ID!,
        $name:String,
        $permissions:[ID]!
    ) {
        update_group(
            id:$id
            group:{
                name:$name,
                permissions:$permissions
            }
        ) {
            id
        }
    }
`;

export const READ_GROUP=gql`
    query($find:JSON) {
        read_group(find:$find) {
            name
            permissions {
                id
            }
        }
    }
`;

export const GET_FOREIGN=gql`
    query {
        all_permission(sort:"number") {
            list {
                id
                name
            }
        }
    }
`;