import { gql } from "@apollo/client";

export const ALL_GROUP=gql`
    query ($find:JSON,$page:Int,$limit:Int,$sort:JSON,$count:Boolean) {
        all_group (find:$find,page:$page,limit:$limit,sort:$sort,count:$count){
            list {
                id
                name
            }
            count
        }
    }
`;

export const ALL_GROUP_REPORT=gql`
    query ($find:JSON,$page:Int,$limit:Int,$sort:JSON,$count:Boolean) {
        all_group (find:$find,page:$page,limit:$limit,sort:$sort,count:$count){
            list {
                name
            }
        }
    }
`;