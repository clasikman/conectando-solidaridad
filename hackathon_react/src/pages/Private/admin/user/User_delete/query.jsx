import { gql } from "@apollo/client";

export const READ_USER=gql`
    query($find:JSON) {
        read_user(find:$find) {
            name
            username
            email
            groups {
                id
                name
            }
            permissions {
                id
                name
            }
            is_admin
            is_active
            last_login
        }
    }
`;

export const DELETE_USER=gql`
    mutation($id: ID!) {
        delete_user(id: $id)
    }
`;