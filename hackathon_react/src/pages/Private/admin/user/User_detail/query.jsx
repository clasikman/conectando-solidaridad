import { gql } from "@apollo/client";

export const READ_USER=gql`
    query($find:JSON) {
        read_user(find:$find) {
            name
            username
            email
            groups {
                name
            }
            permissions {
                name
            }
            is_admin
            is_active
            last_login
        }
    }
`;