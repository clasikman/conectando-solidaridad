import { gql } from "@apollo/client";

export const CREATE_USER=gql`
    mutation(
        $name:String,
        $username:String!,
        $email:String,
        $password:String!,
        $groups:[ID],
        $permissions:[ID],
        $is_admin:Boolean,
        $is_active:Boolean,
        $photo_url:String
    ) {
        create_user(
            user:{
                name:$name,
                username:$username,
                email:$email,
                password:$password,
                groups:$groups,
                permissions:$permissions,
                is_admin:$is_admin,
                is_active:$is_active,
                photo_url:$photo_url
            }
        ) {
            id
        }
    }   
`;

export const UPDATE_USER=gql`
    mutation(
        $id:ID!,
        $name:String,
        $username:String!,
        $email:String,
        $password:String,
        $groups:[ID],
        $permissions:[ID],
        $is_admin:Boolean,
        $is_active:Boolean,
        $photo_url:String
    ) {
        update_user(
            id:$id
            user:{
                name:$name,
                username:$username,
                email:$email,
                password:$password,
                groups:$groups,
                permissions:$permissions,
                is_admin:$is_admin,
                is_active:$is_active,
                photo_url:$photo_url
            }
        ) {
            id
        }
    }
`;

export const READ_USER=gql`
    query($find:JSON) {
        read_user(find:$find) {
            name
            username
            email
            groups {
                id
            }
            permissions {
                id
            }
            is_admin
            is_active
            photo_url
        }
    }
`;

export const GET_FOREIGN=gql`
    query {
        all_group {
            list {
                id
                name
            }
        }
        all_permission(sort:"number") {
            list {
                id
                name
            }
        }
    }
`;