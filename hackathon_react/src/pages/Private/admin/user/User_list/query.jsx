import { gql } from "@apollo/client";

export const ALL_USER=gql`
    query ($find:JSON,$page:Int,$limit:Int,$sort:JSON,$count:Boolean) {
        all_user (find:$find,page:$page,limit:$limit,sort:$sort,count:$count){
            list {
                id
                name
                username
                email
                is_admin
                is_active
            }
            count
        }
    }
`;

export const ALL_USER_REPORT=gql`
    query ($find:JSON,$page:Int,$limit:Int,$sort:JSON,$count:Boolean) {
        all_user (find:$find,page:$page,limit:$limit,sort:$sort,count:$count){
            list {
                name
                username
                email
                is_admin
                is_active
                photo_url
                last_login
            }
        }
    }
`;