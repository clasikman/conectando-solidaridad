import Icon from "@/components/Icon";
import { useContext, useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import Error from '@/components/Error';
import Success from '@/components/Success';
import Failure from "@/components/Failure";
import { useMutation, useQuery } from "@apollo/client";
import { UPDATE_USER, READ_USER } from './query';
import { StoreContext } from "@/context/store";
import './User_profile.css';
import { uploadString } from "@/graphql/firebase";

export default function User_profile() {
    //essentials
    const {id} = useParams();
    const store=useContext(StoreContext);
    const [errors, set_errors]=useState([]);
    const [submit,set_submit]=useState(false);
    const [success,set_success]=useState(false);
    const [failure,set_failure]=useState(false);
    const [update, data_update]=useMutation(UPDATE_USER);
    const data_read=useQuery(READ_USER,{fetchPolicy: "no-cache"});
    //foreigns fields
    //
    //fields
    const [name,set_name]=useState();
    const [username,set_username]=useState();
    const [email,set_email]=useState();
    const [password,set_password]=useState();
    const [photo_url,set_photo_url]=useState();

    const [repeat_password,set_repeat_password]=useState();

    const [file,set_file]=useState();
    const [imageSrc,set_imageSrc]=useState();

    function handleString(e){
        //var file=fileRef.current.files[0];
        var file=e.target.files[0];
        set_file(file);
        var objectURL = URL.createObjectURL(file);
        set_imageSrc(objectURL);
    }

    async function handleSubmit(e) {
        e.preventDefault();
        store.setLoading(true);
        set_errors([]);
        var errors=[];
        if (password!=repeat_password) errors.push("Las contraseñas no coinciden");
        set_errors(errors);
        var fileUpload=null;
        if (file!=null) {
            fileUpload=await uploadString(file);
        }
        if (fileUpload) {
            set_photo_url(fileUpload);
        }
        if (errors.length==0) {
            store.setLoading(true);
            update({variables: {
                name,
                email,
                password,
                photo_url:fileUpload,
            }})
        } else window.scrollTo(0, 0);
        set_submit(true);
    }

    useEffect(()=> {
        var data=data_update;
        if(data.called && !data.loading) {
            store.setLoading(false);
            if (data.error) {
                console.log("error",data.error);
                set_errors([data.error.message]);
                set_failure(true);
            }
            if (data.data) {
                console.log(data.data.update_profile);
                console.log("****",{...data.data.update_profile});
                store.updateProfile(data.data.update_profile);
                set_success(true);
            }
        }
    },[data_update.loading])

    useEffect(()=> {
        if (data_read?.data?.me) {
            const user=data_read.data.me;
            set_name(user.name);
            set_username(user.username);
            set_email(user.email);
            set_photo_url(user.photo_url);
            console.log(user);
        }
        if (data_read?.error) console.log(data_read.error);
    },[data_read.data,data_read.error])

    useEffect(()=> {
    },[])

    return (
        <>
            <Success open={success} setOpen={set_success} title="Éxito!" message="Registro actualizado correctamente" />
            <Failure open={failure} setOpen={set_failure} title="Error!" message={errors?.[0]} />
            <div className="px-4 sm:px-6 lg:px-8">
                <div className="bg-gray-50 px-4 py-3 rounded-lg border-gray-200 border sm:flex sm:items-center">
                    <div className="sm:flex-auto">
                        <h1 className="text-xl font-semibold text-gray-900 uppercase">PERFIL DE USUARIO</h1>
                    </div>
                    <div className="mt-4 sm:mt-0 sm:ml-16 sm:flex-none">
                        <Link
                            to="../user/list"
                            type="button"
                            className="inline-flex items-center justify-center rounded-md border border-transparent bg-gray-600 px-4 py-2 text-sm font-medium text-white shadow-sm hover:bg-gray-700 focus:outline-none focus:ring-2 focus:ring-green-500 focus:ring-offset-2 sm:w-auto"
                        >
                            <Icon name="ArrowLeftCircleIcon" className="h-6 w-6" />
                            Atras
                        </Link>
                    </div>
                </div>
                <div className="mt-8 px-4 py-1 bg-gray-50 rounded-lg border border-gray-200">
                    <Error title="Error en el Formulario" errors={errors}  />
                    <form onSubmit={handleSubmit} className="space-y-8 divide-y divide-gray-200">
                        <div className="space-y-6 px-4 py-4">
                            <div className="sm:grid sm:grid-cols-6 sm:gap-x-10 sm:gap-y-6 sm:items-start">
                                <div className="bg-blue-200 rounded-lg border-t border-b sm:col-span-6">
                                    <h1 className="text-md ml-8 leading-6 py-2 font-medium text-gray-900 text-center">Información Personal</h1>
                                </div>
                                <div className="mt-1 sm:mt-0 sm:col-span-3">
                                    <label htmlFor="name" className="block text-sm font-medium text-gray-800">
                                        Nombre
                                    </label>
                                    <input required type="text" maxLength="50" name="name" value={name} onChange={e=>set_name(e.target.value)} className="mt-1 focus:ring-blue-300 focus:border-blue-300 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md uppercase" />
                                </div>
                                <div className="mt-1 sm:mt-0 sm:col-span-3">
                                    <label htmlFor="username" className="block text-sm font-medium text-gray-800">
                                        Nombre de Usuario
                                    </label>
                                    <input readOnly type="text" minLength="4" maxLength="25" name="username" value={username} onChange={e=>set_username(e.target.value)} className="mt-1 focus:ring-blue-300 focus:border-blue-300 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md" />
                                </div>
                                <div className="mt-1 sm:mt-0 sm:col-span-6">
                                    <label htmlFor="email" className="block text-sm font-medium text-gray-700">
                                        Email
                                    </label>
                                    <div className="relative mt-1 rounded-md shadow-sm">
                                        <div className="pointer-events-none absolute inset-y-0 left-0 flex items-center pl-3">
                                            <Icon name="EnvelopeIcon" className="h-5 w-5 text-gray-400" aria-hidden="true" />
                                        </div>
                                        <input type="email" name="field" value={email} onChange={e=>set_email(e.target.value)} className="block w-full rounded-md border-gray-300 pl-10 focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm" />
                                    </div>
                                </div>
                                <div className="mt-1 sm:mt-0 sm:col-span-3">
                                    <label htmlFor="password" className="block text-sm font-medium text-gray-800">
                                        Contraseña
                                    </label>
                                    <input type="password" name="password" value={password} onChange={e=>set_password(e.target.value)} className="mt-1 focus:ring-blue-300 focus:border-blue-300 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md" />
                                </div>
                                <div className="mt-1 sm:mt-0 sm:col-span-3">
                                    <label htmlFor="repeat_password" className="block text-sm font-medium text-gray-800">
                                        Repita la Contraseña
                                    </label>
                                    <input type="password" minLength="4" maxLength="25" name="password" value={repeat_password} onChange={e=>set_repeat_password(e.target.value)} className="mt-1 focus:ring-blue-300 focus:border-blue-300 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md" />
                                </div>
                                <div className="mt-1 sm:mt-0 sm:col-span-6">
                                    <label htmlFor="imagen" className="block text-sm font-medium text-gray-800">
                                        Imagen
                                    </label>
                                    <input type="file" name="imagen" onChange={handleString} accept="image/*" className="mt-1 focus:ring-blue-300 focus:border-blue-300 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md extraclass" />
                                    <img src={imageSrc} className="object-cover shadow-lg rounded-lg h-64" />
                                </div>
                            </div>
                        </div>      
                        <div className="pt-5">
                            <div className="flex justify-end">
                                <Link
                                    to="../user/list"
                                    className="rounded-md border border-gray-300 bg-white py-2 px-4 text-sm font-medium text-gray-700 shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-green-500 focus:ring-offset-2"
                                >
                                    Cancelar
                                </Link>
                                <button
                                    type="submit"
                                    className="ml-3 inline-flex justify-center rounded-md border border-transparent bg-green-600 py-2 px-4 text-sm font-medium text-white shadow-sm hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-green-500 focus:ring-offset-2"
                                >
                                    Aceptar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>            
            </div>
        </>
    )
}