import { gql } from "@apollo/client";

export const UPDATE_USER=gql`
    mutation(
        $name:String,
        $email:String,
        $password:String,
        $photo_url:String
    ) {
        update_profile (
            user:{
                name:$name,
                email:$email,
                password:$password,
                photo_url:$photo_url
            }
        ) {
            name
            email
            photo_url
        }
    }
`;

export const READ_USER=gql`
    query {
        me {
            name
            username
            email
            photo_url
        }
    }
`;