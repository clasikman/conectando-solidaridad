import { gql } from "@apollo/client";

export const READ_ENTIDAD=gql`
    query($find:JSON) {
        read_entidad(find:$find) {
            categoria {
                nombre
            }
            nombre
            telefono
            representante
            direccion
            latitud
            longitud
            descripcion
            actividades
        }
    }
`;