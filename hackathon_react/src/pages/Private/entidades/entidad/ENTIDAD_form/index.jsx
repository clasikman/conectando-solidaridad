import MultiCheckbox from "@/components/Multiselect";
import Icon from "@/components/Icon";
import { useContext, useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import Error from '@/components/Error';
import Success from '@/components/Success';
import Failure from "@/components/Failure";
import { useLazyQuery, useMutation, useQuery } from "@apollo/client";
import { CREATE_ENTIDAD, UPDATE_ENTIDAD, READ_ENTIDAD, GET_FOREIGN } from './query';
import { StoreContext } from "@/context/store";
import './ENTIDAD_form.css';

export default function ENTIDAD_form() {
    //essentials
    const {id} = useParams();
    const store=useContext(StoreContext);
    const [errors, set_errors]=useState([]);
    const [submit,set_submit]=useState(false);
    const [success,set_success]=useState(false);
    const [failure,set_failure]=useState(false);
    const [register, data_register]=useMutation(CREATE_ENTIDAD);
    const [update, data_update]=useMutation(UPDATE_ENTIDAD);
    const [read, data_read]=useLazyQuery(READ_ENTIDAD,{fetchPolicy: "no-cache"});
    //foreigns fields
    const foreigns = useQuery(GET_FOREIGN,{fetchPolicy: "no-cache"});
    //states Default
    const [categoria,set_categoria]=useState([]);
    const [nombre,set_nombre]=useState();
    const [telefono,set_telefono]=useState();
    const [representante,set_representante]=useState();
    const [direccion,set_direccion]=useState();
    const [latitud,set_latitud]=useState(-17.395044);
    const [longitud,set_longitud]=useState(-66.1452495);
    const [descripcion,set_descripcion]=useState();
    const [actividades,set_actividades]=useState();


    function handleSubmit(e) {
        e.preventDefault();
        set_errors([]);
        var errors=[];
        //validacioines if error.push("error en string mensaje");
        if (!categoria.length) errors.push("Seleccione una opción para Categorias")









        set_errors(errors);
        if (errors.length==0) {
            store.setLoading(true);
            if (id) {
                update({variables: {
                    id,
                    categoria,
                    nombre,
                    telefono,
                    representante,
                    direccion,
                    latitud,
                    longitud,
                    descripcion,
                    actividades,

                }})
            } else {
                register({variables: {
                    categoria,
                    nombre,
                    telefono,
                    representante,
                    direccion,
                    latitud,
                    longitud,
                    descripcion,
                    actividades,

                }})
            }
        } else window.scrollTo(0, 0);
        set_submit(true);
    }



    useEffect(()=> {
        var data=id?data_update:data_register;
        if(data.called && !data.loading) {
            store.setLoading(false);
            if (data.error) {
                console.log("error",data.error);
                set_errors([data.error.message]);
                set_failure(true);
            }
            if (data.data) set_success(true);
        }
    },[data_register.loading,data_update.loading])

    useEffect(()=> {
        if (data_read?.data?.read_entidad) {
            const entidad=data_read.data.read_entidad;
            set_categoria(entidad.categoria.map(f=>f.id));
            set_nombre(entidad.nombre);
            set_telefono(entidad.telefono);
            set_representante(entidad.representante);
            set_direccion(entidad.direccion);
            set_latitud(entidad.latitud);
            set_longitud(entidad.longitud);
            set_descripcion(entidad.descripcion);
            set_actividades(entidad.actividades);
            console.log(entidad);
        }
    },[data_read.data])

    useEffect(()=> {
        if (id) read({variables: {find: { _id:id }}});

    },[])

    return (
        <>
            <Success open={success} setOpen={set_success} title="Éxito!" message="Registro actualizado correctamente" redirect="/dashboard/entidad/list" />
            <Failure open={failure} setOpen={set_failure} title="Error!" message={errors?.[0]} />
            <div className="px-4 sm:px-6 lg:px-8">
                <div className="bg-gray-50 px-4 py-3 rounded-lg border-gray-200 border sm:flex sm:items-center">
                    <div className="sm:flex-auto">
                        <h1 className="text-xl font-semibold text-gray-900 uppercase">{id ? "EDITAR ENTIDAD" : "REGISTRAR ENTIDAD"}</h1>
                    </div>
                    <div className="mt-4 sm:mt-0 sm:ml-16 sm:flex-none">
                        <Link
                            to="../entidad/list"
                            type="button"
                            className="inline-flex items-center justify-center rounded-md border border-transparent bg-gray-600 px-4 py-2 text-sm font-medium text-white shadow-sm hover:bg-gray-700 focus:outline-none focus:ring-2 focus:ring-green-500 focus:ring-offset-2 sm:w-auto"
                        >
                            <Icon name="ArrowLeftCircleIcon" className="h-6 w-6" />
                            Atras
                        </Link>
                    </div>
                </div>
                <div className="mt-8 px-4 py-1 bg-gray-50 rounded-lg border border-gray-200">
                    <Error title="Error en el Formulario" errors={errors}  />
                    <form onSubmit={handleSubmit} className="space-y-8 divide-y divide-gray-200">
                        <div className="space-y-6 px-4 py-4">
                            <div className="sm:grid sm:grid-cols-6 sm:gap-x-10 sm:gap-y-6 sm:items-start">
                                <div className="mt-1 sm:mt-0 sm:col-span-6">
                                    <MultiCheckbox label="Categorias" options={foreigns?.data?.all_categorias?.list} field="nombre" values={categoria} setValues={set_categoria} />
                                </div>
                                <div className="mt-1 sm:mt-0 sm:col-span-3">
                                    <label htmlFor="nombre" className="block text-sm font-medium text-gray-800">
                                        Nombre de Entidad
                                    </label>
                                    <input type="text" required  name="nombre" value={nombre} onChange={e=>set_nombre(e.target.value)} className="mt-1 focus:ring-blue-300 focus:border-blue-300 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md extraclass" />
                                </div>
                                <div className="mt-1 sm:mt-0 sm:col-span-3">
                                    <label htmlFor="telefono" className="block text-sm font-medium text-gray-800">
                                        Telefono
                                    </label>
                                    <input type="number" placeholder="1232"  name="telefono" value={telefono} onChange={e=>set_telefono(parseInt(e.target.value))} className="mt-1 focus:ring-blue-300 focus:border-blue-300 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md extraclass" />
                                </div>
                                <div className="mt-1 sm:mt-0 sm:col-span-3">
                                    <label htmlFor="representante" className="block text-sm font-medium text-gray-800">
                                        Representante
                                    </label>
                                    <input type="text" placeholder="1234"  name="representante" value={representante} onChange={e=>set_representante(e.target.value)} className="mt-1 focus:ring-blue-300 focus:border-blue-300 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md extraclass" />
                                </div>
                                <div className="mt-1 sm:mt-0 sm:col-span-3">
                                    <label htmlFor="direccion" className="block text-sm font-medium text-gray-800">
                                        Direccion
                                    </label>
                                    <textarea name="direccion" required placeholder="" value={direccion} onChange={e=>set_direccion(e.target.value)} rows="3" className="mt-1 focus:ring-blue-300 focus:border-blue-300 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"/>
                                </div>
                                <div className="mt-1 sm:mt-0 sm:col-span-3">
                                    <label htmlFor="latitud" className="block text-sm font-medium text-gray-800">
                                        latitud
                                    </label>
                                    <input type="number" step="0.0000001"  name="latitud" value={latitud} onChange={e=>set_latitud(parseFloat(e.target.value))} className="mt-1 focus:ring-blue-300 focus:border-blue-300 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md extraclass" />
                                </div>
                                <div className="mt-1 sm:mt-0 sm:col-span-3">
                                    <label htmlFor="longitud" className="block text-sm font-medium text-gray-800">
                                        longitud
                                    </label>
                                    <input type="number"  name="longitud" value={longitud} onChange={e=>set_longitud(parseInt(e.target.value))} className="mt-1 focus:ring-blue-300 focus:border-blue-300 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md extraclass" />
                                </div>
                                <div className="mt-1 sm:mt-0 sm:col-span-3">
                                    <label htmlFor="descripcion" className="block text-sm font-medium text-gray-800">
                                        Describe a tu organizacion
                                    </label>
                                    <textarea name="descripcion" required  value={descripcion} onChange={e=>set_descripcion(e.target.value)} rows="3" className="mt-1 focus:ring-blue-300 focus:border-blue-300 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"/>
                                </div>
                                <div className="mt-1 sm:mt-0 sm:col-span-3">
                                    <label htmlFor="actividades" className="block text-sm font-medium text-gray-800">
                                        tus activiades mas relevantes
                                    </label>
                                    <textarea name="actividades" required  value={actividades} onChange={e=>set_actividades(e.target.value)} rows="3" className="mt-1 focus:ring-blue-300 focus:border-blue-300 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"/>
                                </div>
                            </div>
                        </div>      
                        <div className="pt-5">
                            <div className="flex justify-end">
                                <Link
                                    to="../entidad/list"
                                    className="rounded-md border border-gray-300 bg-white py-2 px-4 text-sm font-medium text-gray-700 shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-green-500 focus:ring-offset-2"
                                >
                                    Cancelar
                                </Link>
                                <button
                                    type="submit"
                                    className="ml-3 inline-flex justify-center rounded-md border border-transparent bg-green-600 py-2 px-4 text-sm font-medium text-white shadow-sm hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-green-500 focus:ring-offset-2"
                                >
                                    Aceptar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>            
            </div>
        </>
    )
}