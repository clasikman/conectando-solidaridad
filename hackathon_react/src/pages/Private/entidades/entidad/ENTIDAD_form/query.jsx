import { gql } from "@apollo/client";

export const CREATE_ENTIDAD=gql`
    mutation(
        $categoria: [ID]!,
        $nombre: String!,
        $telefono: Int,
        $representante: String,
        $direccion: String!,
        $latitud: Float,
        $longitud: Float,
        $descripcion: String!,
        $actividades: String!
    ) {
        create_entidad(
            entidad:{
                categoria:$categoria,
                nombre:$nombre,
                telefono:$telefono,
                representante:$representante,
                direccion:$direccion,
                latitud:$latitud,
                longitud:$longitud,
                descripcion:$descripcion,
                actividades:$actividades
            }
        ) {
            id
        }
    }   
`;

export const UPDATE_ENTIDAD=gql`
    mutation(
        $id:ID!,
        $categoria: [ID]!,
        $nombre: String!,
        $telefono: Int,
        $representante: String,
        $direccion: String!,
        $latitud: Float,
        $longitud: Float,
        $descripcion: String!,
        $actividades: String!
    ) {
        update_entidad(
            id:$id
            entidad:{
                categoria:$categoria,
                nombre:$nombre,
                telefono:$telefono,
                representante:$representante,
                direccion:$direccion,
                latitud:$latitud,
                longitud:$longitud,
                descripcion:$descripcion,
                actividades:$actividades
            }
        ) {
            id
        }
    }
`;

export const READ_ENTIDAD=gql`
    query($find:JSON) {
        read_entidad(find:$find) {
            categoria {
                id
            }
            nombre
            telefono
            representante
            direccion
            latitud
            longitud
            descripcion
            actividades
        }
    }
`;
export const GET_FOREIGN=gql`
    query {
        all_categorias {
            list {
                id
                nombre
            }
        }
    }
`;