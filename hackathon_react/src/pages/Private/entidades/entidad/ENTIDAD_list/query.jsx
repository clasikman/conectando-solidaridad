import { gql } from "@apollo/client";

export const ALL_ENTIDAD=gql`
    query ($find:JSON,$page:Int,$limit:Int,$sort:JSON,$count:Boolean) {
        all_entidad (find:$find,page:$page,limit:$limit,sort:$sort,count:$count){
            list {
                id
                categoria {
                    nombre
                }
                nombre
            }
            count
        }
    }
`;

export const ALL_ENTIDAD_REPORT=gql`
    query ($find:JSON,$page:Int,$limit:Int,$sort:JSON,$count:Boolean) {
        all_entidad (find:$find,page:$page,limit:$limit,sort:$sort,count:$count){
            list {
                categoria {
                    nombre
                }
                nombre
                telefono
                representante
                direccion
                latitud
                longitud
                descripcion
                actividades
            }
        }
    }
`;