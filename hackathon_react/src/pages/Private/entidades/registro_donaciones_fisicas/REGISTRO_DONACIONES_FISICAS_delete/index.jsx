import Icon from "@/components/Icon";
import { useContext, useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import Failure from "@/components/Failure";
import { useMutation, useQuery } from "@apollo/client";
import { READ_REGISTRO_DONACIONES_FISICAS, DELETE_REGISTRO_DONACIONES_FISICAS } from './query';
import { StoreContext } from "@/context/store";
import './REGISTRO_DONACIONES_FISICAS_delete.css';
import Success from "@/components/Success";

export default function REGISTRO_DONACIONES_FISICAS_delete() {
    //essentials
    const {id} = useParams();
    const store=useContext(StoreContext);
    const [success,set_success]=useState(false);
    const [failure,set_failure]=useState(false);
    const {data,error}=useQuery(READ_REGISTRO_DONACIONES_FISICAS,{fetchPolicy: "no-cache",variables:{find:{_id:id}}});
    const [registro_donaciones_fisicas,set_registro_donaciones_fisicas]=useState({});
    const [del,data_del]=useMutation(DELETE_REGISTRO_DONACIONES_FISICAS);

    function deleteREGISTRO_DONACIONES_FISICAS() {
        del({variables: {
            id:id
        }});
    }

    useEffect(()=> {
        if(data_del.called && !data_del.loading) {
            store.setLoading(false);
            if (data_del.error) {
                console.log("error",data_del.error);
                set_errors([data_del.error.message]);
                set_failure(true);
            }
            if (data_del.data.delete_registro_donaciones_fisicas) set_success(true);
        }
    },[data_del.loading])

    useEffect(()=> {
        if (data?.read_registro_donaciones_fisicas) {
            store.setLoading(false);
            set_registro_donaciones_fisicas(data.read_registro_donaciones_fisicas);
        }
    },[data])
    
    useEffect(()=> {
        store.setLoading(true);
    },[])

    if (error) return <Failure open={failure} setOpen={set_failure} title="Error!" message={error?.message} />

    return (
        <>
            <Success open={success} setOpen={set_success} title="Éxito!" message="Registro eliminado." redirect="/dashboard/registro_donaciones_fisicas/list" />
            <Failure open={failure} setOpen={set_failure} title="Error!" message={data_del?.error?.message} />
            <div className="px-4 sm:px-6 lg:px-8">
                <div className="bg-gray-50 px-4 py-3 rounded-lg border-gray-200 border sm:flex sm:items-center">
                    <div className="sm:flex-auto">
                        <h1 className="text-xl font-semibold text-gray-900 uppercase">ELIMINAR REGISTRO_DONACIONES_FISICAS</h1>
                    </div>
                    <div className="mt-4 sm:mt-0 sm:ml-16 sm:flex-none">
                        <Link
                            to="../registro_donaciones_fisicas/list"
                            type="button"
                            className="inline-flex items-center justify-center rounded-md border border-transparent bg-gray-600 px-4 py-2 text-sm font-medium text-white shadow-sm hover:bg-gray-700 focus:outline-none focus:ring-2 focus:ring-green-500 focus:ring-offset-2 sm:w-auto"
                        >
                            <Icon name="ArrowLeftCircleIcon" className="h-6 w-6" />
                            Atras
                        </Link>
                    </div>
                </div>
                <div className="mt-8 px-4 py-1 bg-gray-50 rounded-lg border border-gray-200">
                    <div className="text-lg mt-2 max-w-xl text-sm text-gray-500">
                        <p>
                            Desea eliminar los datos del REGISTRO_DONACIONES_FISICAS
                        </p>
                    </div>
                    <div className="mt-5">
                        <button type="button" onClick={deleteREGISTRO_DONACIONES_FISICAS} className="text-lg inline-flex items-center justify-center px-4 py-2 border border-transparent font-medium rounded-md text-red-700 bg-red-100 hover:bg-red-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500 sm:text-sm">
                            Eliminar REGISTRO_DONACIONES_FISICAS
                        </button>
                    </div>
                    <hr></hr>
                    <dl className="mt-5 ml-8 pb-8 grid grid-cols-1 gap-x-4 gap-y-8 sm:grid-cols-2">
                        <div className="sm:col-span-1">
                            <dt className="block text-sm font-medium text-gray-700">
                                Items
                            </dt>
                            <dd className="mt-1 block w-full shadow-sm sm:text-sm border-y-400 text-blue-800 rounded-md">
                                {registro_donaciones_fisicas?.item}
                            </dd>
                        </div>
                        <div className="sm:col-span-1">
                            <dt className="block text-sm font-medium text-gray-700">
                                Foto
                            </dt>
                            <dd className="mt-1 block w-full shadow-sm sm:text-sm border-y-400 text-blue-800 rounded-md">
                                {registro_donaciones_fisicas?.foto}
                            </dd>
                        </div>
                        <div className="sm:col-span-1">
                            <dt className="block text-sm font-medium text-gray-700">
                                Descripción
                            </dt>
                            <dd className="mt-1 block w-full shadow-sm sm:text-sm border-y-400 text-blue-800 rounded-md">
                                {registro_donaciones_fisicas?.descripcion}
                            </dd>
                        </div>
                    </dl>
                </div>            
            </div>
        </>
    )
}