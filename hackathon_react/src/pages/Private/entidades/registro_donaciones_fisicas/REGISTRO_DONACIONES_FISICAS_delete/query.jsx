import { gql } from "@apollo/client";

export const READ_REGISTRO_DONACIONES_FISICAS=gql`
    query($find:JSON) {
        read_registro_donaciones_fisicas(find:$find) {
            item
            foto
            descripcion
        }
    }
`;

export const DELETE_REGISTRO_DONACIONES_FISICAS=gql`
    mutation($id: ID!) {
        delete_registro_donaciones_fisicas(id: $id)
    }
`;