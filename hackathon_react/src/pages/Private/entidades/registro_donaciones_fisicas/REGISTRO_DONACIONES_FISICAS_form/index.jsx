import Icon from "@/components/Icon";
import { useContext, useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import Error from '@/components/Error';
import Success from '@/components/Success';
import Failure from "@/components/Failure";
import { useLazyQuery, useMutation } from "@apollo/client";
import { CREATE_REGISTRO_DONACIONES_FISICAS, UPDATE_REGISTRO_DONACIONES_FISICAS, READ_REGISTRO_DONACIONES_FISICAS } from './query';
import { StoreContext } from "@/context/store";
import './REGISTRO_DONACIONES_FISICAS_form.css';

export default function REGISTRO_DONACIONES_FISICAS_form() {
    //essentials
    const {id} = useParams();
    const store=useContext(StoreContext);
    const [errors, set_errors]=useState([]);
    const [submit,set_submit]=useState(false);
    const [success,set_success]=useState(false);
    const [failure,set_failure]=useState(false);
    const [register, data_register]=useMutation(CREATE_REGISTRO_DONACIONES_FISICAS);
    const [update, data_update]=useMutation(UPDATE_REGISTRO_DONACIONES_FISICAS);
    const [read, data_read]=useLazyQuery(READ_REGISTRO_DONACIONES_FISICAS,{fetchPolicy: "no-cache"});
    //foreigns fields
    //Sin Foraneos
    //states Default
    const [item,set_item]=useState();
    const [foto,set_foto]=useState();
    const [descripcion,set_descripcion]=useState();


    function handleSubmit(e) {
        e.preventDefault();
        set_errors([]);
        var errors=[];
        //validacioines if error.push("error en string mensaje");




        set_errors(errors);
        if (errors.length==0) {
            store.setLoading(true);
            if (id) {
                update({variables: {
                    id,
                    item,
                    foto,
                    descripcion,

                }})
            } else {
                register({variables: {
                    item,
                    foto,
                    descripcion,

                }})
            }
        } else window.scrollTo(0, 0);
        set_submit(true);
    }



    useEffect(()=> {
        var data=id?data_update:data_register;
        if(data.called && !data.loading) {
            store.setLoading(false);
            if (data.error) {
                console.log("error",data.error);
                set_errors([data.error.message]);
                set_failure(true);
            }
            if (data.data) set_success(true);
        }
    },[data_register.loading,data_update.loading])

    useEffect(()=> {
        if (data_read?.data?.read_registro_donaciones_fisicas) {
            const registro_donaciones_fisicas=data_read.data.read_registro_donaciones_fisicas;
            set_item(registro_donaciones_fisicas.item);
            set_foto(registro_donaciones_fisicas.foto);
            set_descripcion(registro_donaciones_fisicas.descripcion);
            console.log(registro_donaciones_fisicas);
        }
    },[data_read.data])

    useEffect(()=> {
        if (id) read({variables: {find: { _id:id }}});

    },[])

    return (
        <>
            <Success open={success} setOpen={set_success} title="Éxito!" message="Registro actualizado correctamente" redirect="/dashboard/registro_donaciones_fisicas/list" />
            <Failure open={failure} setOpen={set_failure} title="Error!" message={errors?.[0]} />
            <div className="px-4 sm:px-6 lg:px-8">
                <div className="bg-gray-50 px-4 py-3 rounded-lg border-gray-200 border sm:flex sm:items-center">
                    <div className="sm:flex-auto">
                        <h1 className="text-xl font-semibold text-gray-900 uppercase">{id ? "EDITAR REGISTRO_DONACIONES_FISICAS" : "REGISTRAR REGISTRO_DONACIONES_FISICAS"}</h1>
                    </div>
                    <div className="mt-4 sm:mt-0 sm:ml-16 sm:flex-none">
                        <Link
                            to="../registro_donaciones_fisicas/list"
                            type="button"
                            className="inline-flex items-center justify-center rounded-md border border-transparent bg-gray-600 px-4 py-2 text-sm font-medium text-white shadow-sm hover:bg-gray-700 focus:outline-none focus:ring-2 focus:ring-green-500 focus:ring-offset-2 sm:w-auto"
                        >
                            <Icon name="ArrowLeftCircleIcon" className="h-6 w-6" />
                            Atras
                        </Link>
                    </div>
                </div>
                <div className="mt-8 px-4 py-1 bg-gray-50 rounded-lg border border-gray-200">
                    <Error title="Error en el Formulario" errors={errors}  />
                    <form onSubmit={handleSubmit} className="space-y-8 divide-y divide-gray-200">
                        <div className="space-y-6 px-4 py-4">
                            <div className="sm:grid sm:grid-cols-6 sm:gap-x-10 sm:gap-y-6 sm:items-start">
                                <div className="mt-1 sm:mt-0 sm:col-span-3">
                                    <label htmlFor="item" className="block text-sm font-medium text-gray-800">
                                        Items
                                    </label>
                                    <input type="file" required  name="item" value={item} onChange={e=>set_item(e.target.value)} className="mt-1 focus:ring-blue-300 focus:border-blue-300 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md extraclass" />
                                </div>
                                <div className="mt-1 sm:mt-0 sm:col-span-3">
                                    <label htmlFor="foto" className="block text-sm font-medium text-gray-800">
                                        Foto
                                    </label>
                                    <input type="text"  name="foto" value={foto} onChange={e=>set_foto(e.target.value)} className="mt-1 focus:ring-blue-300 focus:border-blue-300 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md extraclass" />
                                </div>
                                <div className="mt-1 sm:mt-0 sm:col-span-3">
                                    <label htmlFor="descripcion" className="block text-sm font-medium text-gray-800">
                                        Descripción
                                    </label>
                                    <textarea name="descripcion"  value={descripcion} onChange={e=>set_descripcion(e.target.value)} rows="3" className="mt-1 focus:ring-blue-300 focus:border-blue-300 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"/>
                                </div>
                            </div>
                        </div>      
                        <div className="pt-5">
                            <div className="flex justify-end">
                                <Link
                                    to="../registro_donaciones_fisicas/list"
                                    className="rounded-md border border-gray-300 bg-white py-2 px-4 text-sm font-medium text-gray-700 shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-green-500 focus:ring-offset-2"
                                >
                                    Cancelar
                                </Link>
                                <button
                                    type="submit"
                                    className="ml-3 inline-flex justify-center rounded-md border border-transparent bg-green-600 py-2 px-4 text-sm font-medium text-white shadow-sm hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-green-500 focus:ring-offset-2"
                                >
                                    Aceptar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>            
            </div>
        </>
    )
}