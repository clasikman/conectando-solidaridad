import { gql } from "@apollo/client";

export const CREATE_REGISTRO_DONACIONES_FISICAS=gql`
    mutation(
        $item: JSON!,
        $foto: String,
        $descripcion: String
    ) {
        create_registro_donaciones_fisicas(
            registro_donaciones_fisicas:{
                item:$item,
                foto:$foto,
                descripcion:$descripcion
            }
        ) {
            id
        }
    }   
`;

export const UPDATE_REGISTRO_DONACIONES_FISICAS=gql`
    mutation(
        $id:ID!,
        $item: JSON!,
        $foto: String,
        $descripcion: String
    ) {
        update_registro_donaciones_fisicas(
            id:$id
            registro_donaciones_fisicas:{
                item:$item,
                foto:$foto,
                descripcion:$descripcion
            }
        ) {
            id
        }
    }
`;

export const READ_REGISTRO_DONACIONES_FISICAS=gql`
    query($find:JSON) {
        read_registro_donaciones_fisicas(find:$find) {
            item
            foto
            descripcion
        }
    }
`;
