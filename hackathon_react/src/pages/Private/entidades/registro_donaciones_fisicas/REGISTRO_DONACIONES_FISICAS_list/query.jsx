import { gql } from "@apollo/client";

export const ALL_REGISTRO_DONACIONES_FISICAS=gql`
    query ($find:JSON,$page:Int,$limit:Int,$sort:JSON,$count:Boolean) {
        all_registro_donaciones_fisicas (find:$find,page:$page,limit:$limit,sort:$sort,count:$count){
            list {
                id

            }
            count
        }
    }
`;

export const ALL_REGISTRO_DONACIONES_FISICAS_REPORT=gql`
    query ($find:JSON,$page:Int,$limit:Int,$sort:JSON,$count:Boolean) {
        all_registro_donaciones_fisicas (find:$find,page:$page,limit:$limit,sort:$sort,count:$count){
            list {
                item
                foto
                descripcion
            }
        }
    }
`;