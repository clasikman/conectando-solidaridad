import { StoreContext } from "@/context/store";
import React, { useContext, useEffect, useState } from "react";
import { Navigate, Outlet } from "react-router-dom";
import { gql, useMutation } from '@apollo/client';

const REFRESH_TOKEN = gql`
    mutation {
        refreshToken
    }
`;

function Private() {
    const store=useContext(StoreContext);
    const [refreshToken, {data, loading, error}] = useMutation(REFRESH_TOKEN);
    const [sidebarOpen, setSidebarOpen] = useState(false)
    useEffect(()=>{
        refreshToken();
        const interval = setInterval(() => {
            refreshToken();
        }, parseInt(import.meta.env.VITE_JWT_TIME)*60*1000);
        return () => clearInterval(interval);
    },[])
    useEffect(()=>{
        if (data && data.refreshToken) 
            store.refreshToken(data);
        if (error)
            store.logout();
    },[data,error]);

    //if (loading) return null;
    if (!loading && !store.user) return <Navigate to={'/login'} />

    return (
        <React.Fragment>
            <Outlet/>
        </React.Fragment>
    )
}

export default Private