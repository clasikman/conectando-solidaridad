import { gql } from "@apollo/client";

export const READ_CATEGORIAS=gql`
    query($find:JSON) {
        read_categorias(find:$find) {
            nombre
            descripcion
            items {
                item_donacion
            }
        }
    }
`;

export const DELETE_CATEGORIAS=gql`
    mutation($id: ID!) {
        delete_categorias(id: $id)
    }
`;