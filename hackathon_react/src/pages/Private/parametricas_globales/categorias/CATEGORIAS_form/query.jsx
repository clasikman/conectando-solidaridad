import { gql } from "@apollo/client";

export const CREATE_CATEGORIAS=gql`
    mutation(
        $nombre: String!,
        $descripcion: String!,
        $items: [ID]!
    ) {
        create_categorias(
            categorias:{
                nombre:$nombre,
                descripcion:$descripcion,
                items:$items
            }
        ) {
            id
        }
    }   
`;

export const UPDATE_CATEGORIAS=gql`
    mutation(
        $id:ID!,
        $nombre: String!,
        $descripcion: String!,
        $items: [ID]!
    ) {
        update_categorias(
            id:$id
            categorias:{
                nombre:$nombre,
                descripcion:$descripcion,
                items:$items
            }
        ) {
            id
        }
    }
`;

export const READ_CATEGORIAS=gql`
    query($find:JSON) {
        read_categorias(find:$find) {
            nombre
            descripcion
            items {
                id
            }
        }
    }
`;
export const GET_FOREIGN=gql`
    query {
        all_items_donacion {
            list {
                id
                item_donacion
            }
        }
    }
`;