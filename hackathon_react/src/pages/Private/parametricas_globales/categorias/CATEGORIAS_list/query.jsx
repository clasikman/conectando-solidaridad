import { gql } from "@apollo/client";

export const ALL_CATEGORIAS=gql`
    query ($find:JSON,$page:Int,$limit:Int,$sort:JSON,$count:Boolean) {
        all_categorias (find:$find,page:$page,limit:$limit,sort:$sort,count:$count){
            list {
                id
                nombre
                items {
                    item_donacion
                }
            }
            count
        }
    }
`;

export const ALL_CATEGORIAS_REPORT=gql`
    query ($find:JSON,$page:Int,$limit:Int,$sort:JSON,$count:Boolean) {
        all_categorias (find:$find,page:$page,limit:$limit,sort:$sort,count:$count){
            list {
                nombre
                descripcion
                items {
                    item_donacion
                }
            }
        }
    }
`;