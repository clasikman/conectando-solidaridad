import { gql } from "@apollo/client";

export const READ_ITEMS_DONACION=gql`
    query($find:JSON) {
        read_items_donacion(find:$find) {
            item_donacion
        }
    }
`;

export const DELETE_ITEMS_DONACION=gql`
    mutation($id: ID!) {
        delete_items_donacion(id: $id)
    }
`;