import { gql } from "@apollo/client";

export const READ_ITEMS_DONACION=gql`
    query($find:JSON) {
        read_items_donacion(find:$find) {
            item_donacion
        }
    }
`;