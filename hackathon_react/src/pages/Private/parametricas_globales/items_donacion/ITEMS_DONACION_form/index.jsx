import Icon from "@/components/Icon";
import { useContext, useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import Error from '@/components/Error';
import Success from '@/components/Success';
import Failure from "@/components/Failure";
import { useLazyQuery, useMutation } from "@apollo/client";
import { CREATE_ITEMS_DONACION, UPDATE_ITEMS_DONACION, READ_ITEMS_DONACION } from './query';
import { StoreContext } from "@/context/store";
import './ITEMS_DONACION_form.css';

export default function ITEMS_DONACION_form() {
    //essentials
    const {id} = useParams();
    const store=useContext(StoreContext);
    const [errors, set_errors]=useState([]);
    const [submit,set_submit]=useState(false);
    const [success,set_success]=useState(false);
    const [failure,set_failure]=useState(false);
    const [register, data_register]=useMutation(CREATE_ITEMS_DONACION);
    const [update, data_update]=useMutation(UPDATE_ITEMS_DONACION);
    const [read, data_read]=useLazyQuery(READ_ITEMS_DONACION,{fetchPolicy: "no-cache"});
    //foreigns fields
    //Sin Foraneos
    //states Default
    const [item_donacion,set_item_donacion]=useState();


    function handleSubmit(e) {
        e.preventDefault();
        set_errors([]);
        var errors=[];
        //validacioines if error.push("error en string mensaje");


        set_errors(errors);
        if (errors.length==0) {
            store.setLoading(true);
            if (id) {
                update({variables: {
                    id,
                    item_donacion,

                }})
            } else {
                register({variables: {
                    item_donacion,

                }})
            }
        } else window.scrollTo(0, 0);
        set_submit(true);
    }



    useEffect(()=> {
        var data=id?data_update:data_register;
        if(data.called && !data.loading) {
            store.setLoading(false);
            if (data.error) {
                console.log("error",data.error);
                set_errors([data.error.message]);
                set_failure(true);
            }
            if (data.data) set_success(true);
        }
    },[data_register.loading,data_update.loading])

    useEffect(()=> {
        if (data_read?.data?.read_items_donacion) {
            const items_donacion=data_read.data.read_items_donacion;
            set_item_donacion(items_donacion.item_donacion);
            console.log(items_donacion);
        }
    },[data_read.data])

    useEffect(()=> {
        if (id) read({variables: {find: { _id:id }}});

    },[])

    return (
        <>
            <Success open={success} setOpen={set_success} title="Éxito!" message="Registro actualizado correctamente" redirect="/dashboard/items_donacion/list" />
            <Failure open={failure} setOpen={set_failure} title="Error!" message={errors?.[0]} />
            <div className="px-4 sm:px-6 lg:px-8">
                <div className="bg-gray-50 px-4 py-3 rounded-lg border-gray-200 border sm:flex sm:items-center">
                    <div className="sm:flex-auto">
                        <h1 className="text-xl font-semibold text-gray-900 uppercase">{id ? "EDITAR ITEMS_DONACION" : "REGISTRAR ITEMS_DONACION"}</h1>
                    </div>
                    <div className="mt-4 sm:mt-0 sm:ml-16 sm:flex-none">
                        <Link
                            to="../items_donacion/list"
                            type="button"
                            className="inline-flex items-center justify-center rounded-md border border-transparent bg-gray-600 px-4 py-2 text-sm font-medium text-white shadow-sm hover:bg-gray-700 focus:outline-none focus:ring-2 focus:ring-green-500 focus:ring-offset-2 sm:w-auto"
                        >
                            <Icon name="ArrowLeftCircleIcon" className="h-6 w-6" />
                            Atras
                        </Link>
                    </div>
                </div>
                <div className="mt-8 px-4 py-1 bg-gray-50 rounded-lg border border-gray-200">
                    <Error title="Error en el Formulario" errors={errors}  />
                    <form onSubmit={handleSubmit} className="space-y-8 divide-y divide-gray-200">
                        <div className="space-y-6 px-4 py-4">
                            <div className="sm:grid sm:grid-cols-6 sm:gap-x-10 sm:gap-y-6 sm:items-start">
                                <div className="mt-1 sm:mt-0 sm:col-span-3">
                                    <label htmlFor="item_donacion" className="block text-sm font-medium text-gray-700">
                                        item donacion
                                    </label>
                                    <div className="relative mt-1 rounded-md shadow-sm">
                                        <div className="pointer-events-none absolute inset-y-0 left-0 flex items-center pl-3">
                                            <Icon name="CheckIcon" className="h-5 w-5 text-gray-400" aria-hidden="true" />
                                        </div>
                                        <input type="text" required placeholder="Ropa y Calzados"  name="item_donacion" value={item_donacion} onChange={e=>set_item_donacion(e.target.value)} className="block w-full rounded-md border-gray-300 pl-10 focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm" />
                                    </div>
                                </div>
                            </div>
                        </div>      
                        <div className="pt-5">
                            <div className="flex justify-end">
                                <Link
                                    to="../items_donacion/list"
                                    className="rounded-md border border-gray-300 bg-white py-2 px-4 text-sm font-medium text-gray-700 shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-green-500 focus:ring-offset-2"
                                >
                                    Cancelar
                                </Link>
                                <button
                                    type="submit"
                                    className="ml-3 inline-flex justify-center rounded-md border border-transparent bg-green-600 py-2 px-4 text-sm font-medium text-white shadow-sm hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-green-500 focus:ring-offset-2"
                                >
                                    Aceptar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>            
            </div>
        </>
    )
}