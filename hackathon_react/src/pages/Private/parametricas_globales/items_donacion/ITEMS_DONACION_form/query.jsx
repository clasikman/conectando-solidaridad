import { gql } from "@apollo/client";

export const CREATE_ITEMS_DONACION=gql`
    mutation(
        $item_donacion: String!
    ) {
        create_items_donacion(
            items_donacion:{
                item_donacion:$item_donacion
            }
        ) {
            id
        }
    }   
`;

export const UPDATE_ITEMS_DONACION=gql`
    mutation(
        $id:ID!,
        $item_donacion: String!
    ) {
        update_items_donacion(
            id:$id
            items_donacion:{
                item_donacion:$item_donacion
            }
        ) {
            id
        }
    }
`;

export const READ_ITEMS_DONACION=gql`
    query($find:JSON) {
        read_items_donacion(find:$find) {
            item_donacion
        }
    }
`;
