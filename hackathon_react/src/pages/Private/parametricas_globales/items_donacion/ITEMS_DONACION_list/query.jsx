import { gql } from "@apollo/client";

export const ALL_ITEMS_DONACION=gql`
    query ($find:JSON,$page:Int,$limit:Int,$sort:JSON,$count:Boolean) {
        all_items_donacion (find:$find,page:$page,limit:$limit,sort:$sort,count:$count){
            list {
                id
                item_donacion
            }
            count
        }
    }
`;

export const ALL_ITEMS_DONACION_REPORT=gql`
    query ($find:JSON,$page:Int,$limit:Int,$sort:JSON,$count:Boolean) {
        all_items_donacion (find:$find,page:$page,limit:$limit,sort:$sort,count:$count){
            list {
                item_donacion
            }
        }
    }
`;