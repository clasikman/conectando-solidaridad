import Icon from "@/components/Icon";
import { useContext, useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import Failure from "@/components/Failure";
import { useMutation, useQuery } from "@apollo/client";
import { READ_EMPRESA_TRANSPORTE, DELETE_EMPRESA_TRANSPORTE } from './query';
import { StoreContext } from "@/context/store";
import './Empresa_transporte_delete.css';
import Success from "@/components/Success";

export default function Empresa_transporte_delete() {
    //essentials
    const {id} = useParams();
    const store=useContext(StoreContext);
    const [success,set_success]=useState(false);
    const [failure,set_failure]=useState(false);
    const {data,error}=useQuery(READ_EMPRESA_TRANSPORTE,{fetchPolicy: "no-cache",variables:{find:{_id:id}}});
    const [empresa_transporte,set_empresa_transporte]=useState({});
    const [del,data_del]=useMutation(DELETE_EMPRESA_TRANSPORTE);

    function deleteEmpresa_transporte() {
        del({variables: {
            id:id
        }});
    }

    useEffect(()=> {
        if(data_del.called && !data_del.loading) {
            store.setLoading(false);
            if (data_del.error) {
                console.log("error",data_del.error);
                set_errors([data_del.error.message]);
                set_failure(true);
            }
            if (data_del.data.delete_empresa_transporte) set_success(true);
        }
    },[data_del.loading])

    useEffect(()=> {
        if (data?.read_empresa_transporte) {
            store.setLoading(false);
            set_empresa_transporte(data.read_empresa_transporte);
        }
    },[data])
    
    useEffect(()=> {
        store.setLoading(true);
    },[])

    if (error) return <Failure open={failure} setOpen={set_failure} title="Error!" message={error?.message} />

    return (
        <>
            <Success open={success} setOpen={set_success} title="Éxito!" message="Registro eliminado." redirect="/dashboard/empresa_transporte/list" />
            <Failure open={failure} setOpen={set_failure} title="Error!" message={data_del?.error?.message} />
            <div className="px-4 sm:px-6 lg:px-8">
                <div className="bg-gray-50 px-4 py-3 rounded-lg border-gray-200 border sm:flex sm:items-center">
                    <div className="sm:flex-auto">
                        <h1 className="text-xl font-semibold text-gray-900 uppercase">ELIMINAR EMPRESA_TRANSPORTE</h1>
                    </div>
                    <div className="mt-4 sm:mt-0 sm:ml-16 sm:flex-none">
                        <Link
                            to="../empresa_transporte/list"
                            type="button"
                            className="inline-flex items-center justify-center rounded-md border border-transparent bg-gray-600 px-4 py-2 text-sm font-medium text-white shadow-sm hover:bg-gray-700 focus:outline-none focus:ring-2 focus:ring-green-500 focus:ring-offset-2 sm:w-auto"
                        >
                            <Icon name="ArrowLeftCircleIcon" className="h-6 w-6" />
                            Atras
                        </Link>
                    </div>
                </div>
                <div className="mt-8 px-4 py-1 bg-gray-50 rounded-lg border border-gray-200">
                    <div className="text-lg mt-2 max-w-xl text-sm text-gray-500">
                        <p>
                            Desea eliminar los datos del Empresa_transporte
                        </p>
                    </div>
                    <div className="mt-5">
                        <button type="button" onClick={deleteEmpresa_transporte} className="text-lg inline-flex items-center justify-center px-4 py-2 border border-transparent font-medium rounded-md text-red-700 bg-red-100 hover:bg-red-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500 sm:text-sm">
                            Eliminar Empresa_transporte
                        </button>
                    </div>
                    <hr></hr>
                    <dl className="mt-5 ml-8 pb-8 grid grid-cols-1 gap-x-4 gap-y-8 sm:grid-cols-2">
                        <div className="sm:col-span-1">
                            <dt className="block text-sm font-medium text-gray-700">
                                Nombre de Empresa de Transporte
                            </dt>
                            <dd className="mt-1 block w-full shadow-sm sm:text-sm border-y-400 text-blue-800 rounded-md">
                                {empresa_transporte?.nombre_transporte}
                            </dd>
                        </div>
                        <div className="sm:col-span-1">
                            <dt className="block text-sm font-medium text-gray-700">
                                Telefono
                            </dt>
                            <dd className="mt-1 block w-full shadow-sm sm:text-sm border-y-400 text-blue-800 rounded-md">
                                {empresa_transporte?.telefono}
                            </dd>
                        </div>
                        <div className="sm:col-span-1">
                            <dt className="block text-sm font-medium text-gray-700">
                                Dirección
                            </dt>
                            <dd className="mt-1 block w-full shadow-sm sm:text-sm border-y-400 text-blue-800 rounded-md">
                                {empresa_transporte?.direccion}
                            </dd>
                        </div>
                        <div className="sm:col-span-1">
                            <dt className="block text-sm font-medium text-gray-700">
                                Longitu
                            </dt>
                            <dd className="mt-1 block w-full shadow-sm sm:text-sm border-y-400 text-blue-800 rounded-md">
                                {empresa_transporte?.longitude}
                            </dd>
                        </div>
                        <div className="sm:col-span-1">
                            <dt className="block text-sm font-medium text-gray-700">
                                latitude
                            </dt>
                            <dd className="mt-1 block w-full shadow-sm sm:text-sm border-y-400 text-blue-800 rounded-md">
                                {empresa_transporte?.latitude}
                            </dd>
                        </div>
                    </dl>
                </div>            
            </div>
        </>
    )
}