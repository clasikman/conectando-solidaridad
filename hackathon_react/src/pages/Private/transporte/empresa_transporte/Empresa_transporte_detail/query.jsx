import { gql } from "@apollo/client";

export const READ_EMPRESA_TRANSPORTE=gql`
    query($find:JSON) {
        read_empresa_transporte(find:$find) {
            nombre_transporte
            telefono
            direccion
            longitude
            latitude
        }
    }
`;