import Icon from "@/components/Icon";
import { useContext, useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import Error from '@/components/Error';
import Success from '@/components/Success';
import Failure from "@/components/Failure";
import { useLazyQuery, useMutation } from "@apollo/client";
import { CREATE_EMPRESA_TRANSPORTE, UPDATE_EMPRESA_TRANSPORTE, READ_EMPRESA_TRANSPORTE } from './query';
import { StoreContext } from "@/context/store";
import './Empresa_transporte_form.css';

export default function Empresa_transporte_form() {
    //essentials
    const {id} = useParams();
    const store=useContext(StoreContext);
    const [errors, set_errors]=useState([]);
    const [submit,set_submit]=useState(false);
    const [success,set_success]=useState(false);
    const [failure,set_failure]=useState(false);
    const [register, data_register]=useMutation(CREATE_EMPRESA_TRANSPORTE);
    const [update, data_update]=useMutation(UPDATE_EMPRESA_TRANSPORTE);
    const [read, data_read]=useLazyQuery(READ_EMPRESA_TRANSPORTE,{fetchPolicy: "no-cache"});
    //foreigns fields
    //Sin Foraneos
    //states Default
    const [nombre_transporte,set_nombre_transporte]=useState();
    const [telefono,set_telefono]=useState();
    const [direccion,set_direccion]=useState();
    const [longitude,set_longitude]=useState(-66);
    const [latitude,set_latitude]=useState(-17.39504476);


    function handleSubmit(e) {
        e.preventDefault();
        set_errors([]);
        var errors=[];
        //validacioines if error.push("error en string mensaje");






        set_errors(errors);
        if (errors.length==0) {
            store.setLoading(true);
            if (id) {
                update({variables: {
                    id,
                    nombre_transporte,
                    telefono,
                    direccion,
                    longitude,
                    latitude,

                }})
            } else {
                register({variables: {
                    nombre_transporte,
                    telefono,
                    direccion,
                    longitude,
                    latitude,

                }})
            }
        } else window.scrollTo(0, 0);
        set_submit(true);
    }



    useEffect(()=> {
        var data=id?data_update:data_register;
        if(data.called && !data.loading) {
            store.setLoading(false);
            if (data.error) {
                console.log("error",data.error);
                set_errors([data.error.message]);
                set_failure(true);
            }
            if (data.data) set_success(true);
        }
    },[data_register.loading,data_update.loading])

    useEffect(()=> {
        if (data_read?.data?.read_empresa_transporte) {
            const empresa_transporte=data_read.data.read_empresa_transporte;
            set_nombre_transporte(empresa_transporte.nombre_transporte);
            set_telefono(empresa_transporte.telefono);
            set_direccion(empresa_transporte.direccion);
            set_longitude(empresa_transporte.longitude);
            set_latitude(empresa_transporte.latitude);
            console.log(empresa_transporte);
        }
    },[data_read.data])

    useEffect(()=> {
        if (id) read({variables: {find: { _id:id }}});

    },[])

    return (
        <>
            <Success open={success} setOpen={set_success} title="Éxito!" message="Registro actualizado correctamente" redirect="/dashboard/empresa_transporte/list" />
            <Failure open={failure} setOpen={set_failure} title="Error!" message={errors?.[0]} />
            <div className="px-4 sm:px-6 lg:px-8">
                <div className="bg-gray-50 px-4 py-3 rounded-lg border-gray-200 border sm:flex sm:items-center">
                    <div className="sm:flex-auto">
                        <h1 className="text-xl font-semibold text-gray-900 uppercase">{id ? "EDITAR EMPRESA_TRANSPORTE" : "REGISTRAR EMPRESA_TRANSPORTE"}</h1>
                    </div>
                    <div className="mt-4 sm:mt-0 sm:ml-16 sm:flex-none">
                        <Link
                            to="../empresa_transporte/list"
                            type="button"
                            className="inline-flex items-center justify-center rounded-md border border-transparent bg-gray-600 px-4 py-2 text-sm font-medium text-white shadow-sm hover:bg-gray-700 focus:outline-none focus:ring-2 focus:ring-green-500 focus:ring-offset-2 sm:w-auto"
                        >
                            <Icon name="ArrowLeftCircleIcon" className="h-6 w-6" />
                            Atras
                        </Link>
                    </div>
                </div>
                <div className="mt-8 px-4 py-1 bg-gray-50 rounded-lg border border-gray-200">
                    <Error title="Error en el Formulario" errors={errors}  />
                    <form onSubmit={handleSubmit} className="space-y-8 divide-y divide-gray-200">
                        <div className="space-y-6 px-4 py-4">
                            <div className="sm:grid sm:grid-cols-6 sm:gap-x-10 sm:gap-y-6 sm:items-start">
                                <div className="mt-1 sm:mt-0 sm:col-span-3">
                                    <label htmlFor="nombre_transporte" className="block text-sm font-medium text-gray-800">
                                        Nombre de Empresa de Transporte
                                    </label>
                                    <input type="text" required  name="nombre_transporte" value={nombre_transporte} onChange={e=>set_nombre_transporte(e.target.value)} className="mt-1 focus:ring-blue-300 focus:border-blue-300 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md extraclass" />
                                </div>
                                <div className="mt-1 sm:mt-0 sm:col-span-3">
                                    <label htmlFor="telefono" className="block text-sm font-medium text-gray-800">
                                        Telefono
                                    </label>
                                    <input type="number" required placeholder="12456"  name="telefono" value={telefono} onChange={e=>set_telefono(parseInt(e.target.value))} className="mt-1 focus:ring-blue-300 focus:border-blue-300 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md extraclass" />
                                </div>
                                <div className="mt-1 sm:mt-0 sm:col-span-3">
                                    <label htmlFor="direccion" className="block text-sm font-medium text-gray-800">
                                        Dirección
                                    </label>
                                    <textarea name="direccion" required  value={direccion} onChange={e=>set_direccion(e.target.value)} rows="3" className="mt-1 focus:ring-blue-300 focus:border-blue-300 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"/>
                                </div>
                                <div className="mt-1 sm:mt-0 sm:col-span-3">
                                    <label htmlFor="longitude" className="block text-sm font-medium text-gray-800">
                                        Longitu
                                    </label>
                                    <input type="number" step="0.0000001"  name="longitude" value={longitude} onChange={e=>set_longitude(parseFloat(e.target.value))} className="mt-1 focus:ring-blue-300 focus:border-blue-300 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md extraclass" />
                                </div>
                                <div className="mt-1 sm:mt-0 sm:col-span-3">
                                    <label htmlFor="latitude" className="block text-sm font-medium text-gray-800">
                                        latitude
                                    </label>
                                    <input type="number" step="0.0000000001"  name="latitude" value={latitude} onChange={e=>set_latitude(parseFloat(e.target.value))} className="mt-1 focus:ring-blue-300 focus:border-blue-300 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md extraclass" />
                                </div>
                            </div>
                        </div>      
                        <div className="pt-5">
                            <div className="flex justify-end">
                                <Link
                                    to="../empresa_transporte/list"
                                    className="rounded-md border border-gray-300 bg-white py-2 px-4 text-sm font-medium text-gray-700 shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-green-500 focus:ring-offset-2"
                                >
                                    Cancelar
                                </Link>
                                <button
                                    type="submit"
                                    className="ml-3 inline-flex justify-center rounded-md border border-transparent bg-green-600 py-2 px-4 text-sm font-medium text-white shadow-sm hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-green-500 focus:ring-offset-2"
                                >
                                    Aceptar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>            
            </div>
        </>
    )
}