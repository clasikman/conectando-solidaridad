import { gql } from "@apollo/client";

export const CREATE_EMPRESA_TRANSPORTE=gql`
    mutation(
        $nombre_transporte: String!,
        $telefono: Int!,
        $direccion: String!,
        $longitude: Float,
        $latitude: Float
    ) {
        create_empresa_transporte(
            empresa_transporte:{
                nombre_transporte:$nombre_transporte,
                telefono:$telefono,
                direccion:$direccion,
                longitude:$longitude,
                latitude:$latitude
            }
        ) {
            id
        }
    }   
`;

export const UPDATE_EMPRESA_TRANSPORTE=gql`
    mutation(
        $id:ID!,
        $nombre_transporte: String!,
        $telefono: Int!,
        $direccion: String!,
        $longitude: Float,
        $latitude: Float
    ) {
        update_empresa_transporte(
            id:$id
            empresa_transporte:{
                nombre_transporte:$nombre_transporte,
                telefono:$telefono,
                direccion:$direccion,
                longitude:$longitude,
                latitude:$latitude
            }
        ) {
            id
        }
    }
`;

export const READ_EMPRESA_TRANSPORTE=gql`
    query($find:JSON) {
        read_empresa_transporte(find:$find) {
            nombre_transporte
            telefono
            direccion
            longitude
            latitude
        }
    }
`;
