import { gql } from "@apollo/client";

export const ALL_EMPRESA_TRANSPORTE=gql`
    query ($find:JSON,$page:Int,$limit:Int,$sort:JSON,$count:Boolean) {
        all_empresa_transporte (find:$find,page:$page,limit:$limit,sort:$sort,count:$count){
            list {
                id
                nombre_transporte
            }
            count
        }
    }
`;

export const ALL_EMPRESA_TRANSPORTE_REPORT=gql`
    query ($find:JSON,$page:Int,$limit:Int,$sort:JSON,$count:Boolean) {
        all_empresa_transporte (find:$find,page:$page,limit:$limit,sort:$sort,count:$count){
            list {
                nombre_transporte
                telefono
                direccion
                longitude
                latitude
            }
        }
    }
`;