import { gql } from "@apollo/client";

export const READ_DONACIONES=gql`
    query($find:JSON) {
        read_donaciones(find:$find) {
            entidad {
                nombre
            }
            items
            respaldo
            descripcion
            fecha
            codigo
            estado
            tarifa
            usuario
        }
    }
`;

export const DELETE_DONACIONES=gql`
    mutation($id: ID!) {
        delete_donaciones(id: $id)
    }
`;