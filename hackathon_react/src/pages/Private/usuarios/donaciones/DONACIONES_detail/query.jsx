import { gql } from "@apollo/client";

export const READ_DONACIONES=gql`
    query($find:JSON) {
        read_donaciones(find:$find) {
            entidad {
                nombre
            }
            items
            respaldo
            descripcion
            fecha
            codigo
            estado
            tarifa
            usuario
        }
    }
`;