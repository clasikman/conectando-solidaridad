import Select from "@/components/Select1";
import Icon from "@/components/Icon";
import { useContext, useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import Error from '@/components/Error';
import Success from '@/components/Success';
import Failure from "@/components/Failure";
import { useLazyQuery, useMutation, useQuery } from "@apollo/client";
import { CREATE_DONACIONES, UPDATE_DONACIONES, READ_DONACIONES, GET_FOREIGN } from './query';
import { StoreContext } from "@/context/store";
import './DONACIONES_form.css';

export default function DONACIONES_form() {
    //essentials
    const {id} = useParams();
    const store=useContext(StoreContext);
    const [errors, set_errors]=useState([]);
    const [submit,set_submit]=useState(false);
    const [success,set_success]=useState(false);
    const [failure,set_failure]=useState(false);
    const [register, data_register]=useMutation(CREATE_DONACIONES);
    const [update, data_update]=useMutation(UPDATE_DONACIONES);
    const [read, data_read]=useLazyQuery(READ_DONACIONES,{fetchPolicy: "no-cache"});
    //foreigns fields
    const foreigns = useQuery(GET_FOREIGN,{fetchPolicy: "no-cache"});
    //states Default
    const [entidad,set_entidad]=useState();
    const [items,set_items]=useState();
    const [respaldo,set_respaldo]=useState();
    const [descripcion,set_descripcion]=useState();
    const [fecha,set_fecha]=useState("True");
    const [codigo,set_codigo]=useState('dayan-01');
    const [estado,set_estado]=useState("Solicitud de Transporte");
    const [tarifa,set_tarifa]=useState();
    const [usuario,set_usuario]=useState([]);
    const [usuario_s,set_usuario_s]=useState();


    function handleSubmit(e) {
        e.preventDefault();
        set_errors([]);
        var errors=[];
        //validacioines if error.push("error en string mensaje");






if (!estado) errors.push("Seleccione una opción para Estado")



        set_errors(errors);
        if (errors.length==0) {
            store.setLoading(true);
            if (id) {
                update({variables: {
                    id,
                    entidad,
                    items,
                    respaldo,
                    descripcion,
                    fecha,
                    codigo,
                    estado,
                    tarifa,
                    usuario,

                }})
            } else {
                register({variables: {
                    entidad,
                    items,
                    respaldo,
                    descripcion,
                    fecha,
                    codigo,
                    estado,
                    tarifa,
                    usuario,

                }})
            }
        } else window.scrollTo(0, 0);
        set_submit(true);
    }

    useEffect(()=> {
        if (usuario_s && !usuario.some(x=>x.id==usuario_s)) {
            var temp={...foreigns?.data?.all_usuarios?.list.find(x=>x.id==usuario_s)};
            set_usuario([...usuario,temp]);
            set_usuario_s("");
        }
    },[usuario_s])

    useEffect(()=> {
        var data=id?data_update:data_register;
        if(data.called && !data.loading) {
            store.setLoading(false);
            if (data.error) {
                console.log("error",data.error);
                set_errors([data.error.message]);
                set_failure(true);
            }
            if (data.data) set_success(true);
        }
    },[data_register.loading,data_update.loading])

    useEffect(()=> {
        if (data_read?.data?.read_donaciones) {
            const donaciones=data_read.data.read_donaciones;
            set_entidad(donaciones.entidad.id);
            set_items(donaciones.items);
            set_respaldo(donaciones.respaldo);
            set_descripcion(donaciones.descripcion);
            set_fecha(donaciones.fecha);
            set_codigo(donaciones.codigo);
            set_estado(donaciones.estado);
            set_tarifa(donaciones.tarifa);
            set_usuario(donaciones.usuario);
            console.log(donaciones);
        }
    },[data_read.data])

    useEffect(()=> {
        if (id) read({variables: {find: { _id:id }}});

    },[])

    return (
        <>
            <Success open={success} setOpen={set_success} title="Éxito!" message="Registro actualizado correctamente" redirect="/dashboard/donaciones/list" />
            <Failure open={failure} setOpen={set_failure} title="Error!" message={errors?.[0]} />
            <div className="px-4 sm:px-6 lg:px-8">
                <div className="bg-gray-50 px-4 py-3 rounded-lg border-gray-200 border sm:flex sm:items-center">
                    <div className="sm:flex-auto">
                        <h1 className="text-xl font-semibold text-gray-900 uppercase">{id ? "EDITAR DONACIONES" : "REGISTRAR DONACIONES"}</h1>
                    </div>
                    <div className="mt-4 sm:mt-0 sm:ml-16 sm:flex-none">
                        <Link
                            to="../donaciones/list"
                            type="button"
                            className="inline-flex items-center justify-center rounded-md border border-transparent bg-gray-600 px-4 py-2 text-sm font-medium text-white shadow-sm hover:bg-gray-700 focus:outline-none focus:ring-2 focus:ring-green-500 focus:ring-offset-2 sm:w-auto"
                        >
                            <Icon name="ArrowLeftCircleIcon" className="h-6 w-6" />
                            Atras
                        </Link>
                    </div>
                </div>
                <div className="mt-8 px-4 py-1 bg-gray-50 rounded-lg border border-gray-200">
                    <Error title="Error en el Formulario" errors={errors}  />
                    <form onSubmit={handleSubmit} className="space-y-8 divide-y divide-gray-200">
                        <div className="space-y-6 px-4 py-4">
                            <div className="sm:grid sm:grid-cols-6 sm:gap-x-10 sm:gap-y-6 sm:items-start">
                                <div className="mt-1 sm:mt-0 sm:col-span-3">
                                    <Select label="Entidad" options={foreigns?.data?.all_entidad?.list} field="nombre" value={entidad} setValue={set_entidad}  />
                                </div>
                                <div className="mt-1 sm:mt-0 sm:col-span-3">
                                    <label htmlFor="items" className="block text-sm font-medium text-gray-800">
                                        donacion
                                    </label>
                                    <input type="file" required  name="items" value={items} onChange={e=>set_items(e.target.value)} className="mt-1 focus:ring-blue-300 focus:border-blue-300 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md extraclass" />
                                </div>
                                <div className="mt-1 sm:mt-0 sm:col-span-3">
                                    <label htmlFor="respaldo" className="block text-sm font-medium text-gray-700">
                                        Respaldo
                                    </label>
                                    <div className="relative mt-1 rounded-md shadow-sm">
                                        <div className="pointer-events-none absolute inset-y-0 left-0 flex items-center pl-3">
                                            <Icon name="DocumentIcon" className="h-5 w-5 text-gray-400" aria-hidden="true" />
                                        </div>
                                        <input type="text"  name="respaldo" value={respaldo} onChange={e=>set_respaldo(e.target.value)} className="block w-full rounded-md border-gray-300 pl-10 focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm" />
                                    </div>
                                </div>
                                <div className="mt-1 sm:mt-0 sm:col-span-3">
                                    <label htmlFor="descripcion" className="block text-sm font-medium text-gray-800">
                                        Descripcion de donacion
                                    </label>
                                    <input type="text" required  name="descripcion" value={descripcion} onChange={e=>set_descripcion(e.target.value)} className="mt-1 focus:ring-blue-300 focus:border-blue-300 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md extraclass" />
                                </div>
                                <div className="mt-1 sm:mt-0 sm:col-span-3">
                                    <label htmlFor="fecha" className="block text-sm font-medium text-gray-800">
                                        FECHA
                                    </label>
                                    <input type="datetime-local" placeholder=""  name="fecha" value={fecha} onChange={e=>set_fecha(e.target.value)} className="mt-1 focus:ring-blue-300 focus:border-blue-300 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md extraclass" />
                                </div>
                                <div className="mt-1 sm:mt-0 sm:col-span-3">
                                    <label htmlFor="codigo" className="block text-sm font-medium text-gray-700">
                                        ID DE DONACION
                                    </label>
                                    <div className="relative mt-1 rounded-md shadow-sm">
                                        <div className="pointer-events-none absolute inset-y-0 left-0 flex items-center pl-3">
                                            <Icon name="HashtagIcon" className="h-5 w-5 text-gray-400" aria-hidden="true" />
                                        </div>
                                        <input type="text" placeholder="USUARIO-00001" readOnly  name="codigo" value={codigo} onChange={e=>set_codigo(e.target.value)} className="block w-full rounded-md border-gray-300 pl-10 focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm" />
                                    </div>
                                </div>
                                <div className="mt-1 sm:mt-0 sm:col-span-6">
                                    <Select label="Estado" options={['Solicitud de Transporte','Entregado', 'Recepcioando']} value={estado} setValue={set_estado} className={submit && !estado && 'border-2 border-red-400'} />
                                </div>
                                <div className="mt-1 sm:mt-0 sm:col-span-3">
                                    <label htmlFor="tarifa" className="block text-sm font-medium text-gray-800">
                                        Tarifa
                                    </label>
                                    <input type="number" placeholder=""  name="tarifa" value={tarifa} onChange={e=>set_tarifa(parseInt(e.target.value))} className="mt-1 focus:ring-blue-300 focus:border-blue-300 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md extraclass" />
                                </div>
                                <div className="mt-1 sm:mt-0 sm:col-span-3">
                                    <Select label="USUARIO" options={foreigns?.data?.all_usuarios?.list} field="usuario" value={usuario} setValue={set_usuario}  />
                                    {/* automatico del usuario logueado */}
                                </div>
                            </div>
                        </div>      
                        <div className="pt-5">
                            <div className="flex justify-end">
                                <Link
                                    to="../donaciones/list"
                                    className="rounded-md border border-gray-300 bg-white py-2 px-4 text-sm font-medium text-gray-700 shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-green-500 focus:ring-offset-2"
                                >
                                    Cancelar
                                </Link>
                                <button
                                    type="submit"
                                    className="ml-3 inline-flex justify-center rounded-md border border-transparent bg-green-600 py-2 px-4 text-sm font-medium text-white shadow-sm hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-green-500 focus:ring-offset-2"
                                >
                                    Aceptar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>            
            </div>
        </>
    )
}