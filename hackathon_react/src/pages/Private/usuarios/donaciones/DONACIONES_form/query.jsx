import { gql } from "@apollo/client";

export const CREATE_DONACIONES=gql`
    mutation(
        $entidad: ID,
        $items: JSON!,
        $respaldo: String,
        $descripcion: String!,
        $fecha: Date,
        $codigo: String,
        $estado: String!,
        $tarifa: Int,
        $usuario: JSON
    ) {
        create_donaciones(
            donaciones:{
                entidad:$entidad,
                items:$items,
                respaldo:$respaldo,
                descripcion:$descripcion,
                fecha:$fecha,
                codigo:$codigo,
                estado:$estado,
                tarifa:$tarifa,
                usuario:$usuario
            }
        ) {
            id
        }
    }   
`;

export const UPDATE_DONACIONES=gql`
    mutation(
        $id:ID!,
        $entidad: ID,
        $items: JSON!,
        $respaldo: String,
        $descripcion: String!,
        $fecha: Date,
        $codigo: String,
        $estado: String!,
        $tarifa: Int,
        $usuario: JSON
    ) {
        update_donaciones(
            id:$id
            donaciones:{
                entidad:$entidad,
                items:$items,
                respaldo:$respaldo,
                descripcion:$descripcion,
                fecha:$fecha,
                codigo:$codigo,
                estado:$estado,
                tarifa:$tarifa,
                usuario:$usuario
            }
        ) {
            id
        }
    }
`;

export const READ_DONACIONES=gql`
    query($find:JSON) {
        read_donaciones(find:$find) {
            entidad {
                id
            }
            items
            respaldo
            descripcion
            fecha
            codigo
            estado
            tarifa
            usuario
        }
    }
`;
export const GET_FOREIGN=gql`
    query {
        all_entidad {
            list {
                id
                nombre
            }
        }
        all_usuarios {
            list {
                id
                usuario
            }
        }
    }
`;