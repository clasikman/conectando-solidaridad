import { useContext, useEffect, useState } from "react";
import Icon from '@/components/Icon'
import { Link } from "react-router-dom";
import { ALL_DONACIONES, ALL_DONACIONES_REPORT } from "./query";
import { useQuery } from "@apollo/client";
import Failure from "@/components/Failure";
import OptionsRUD from "@/components/OptionsRUD";
import SearchBar from "@/components/SearchBar";
import ButtonsReport from "@/components/ButtonsReport";
import { StoreContext } from "@/context/store";
import './DONACIONES_list.css';

const headers=[
    'Estado',
];

const docForPage=10;

function DONACIONES_list() {
    const store=useContext(StoreContext);
    const [page, setPage] = useState(1);
    const [maxPage, setMaxPage] = useState(1);
    const [sort, setSort] = useState(null);
    const [total, setTotal] = useState(0);
    const [list, setList] = useState([]);
    const [find, setFind] = useState({});
    const [permissions,set_permissions]=useState({});
    const [failure,set_failure]=useState(true);
    const { error, data, refetch } = useQuery(ALL_DONACIONES, {
        fetchPolicy: "no-cache",
        variables: {
            page:page,
            limit:docForPage,
            count:true
        }
    });
    function navPage(next) {
        if (next) {
            if (page<maxPage) {
                refetch({
                    find:find,
                    page:page+1,
                    limit:docForPage,
                    sort:sort,
                    count:false
                });
                setPage(page+1);
            }
        } else {
            if (page>1) {
                refetch({
                    find:find,
                    page:page-1,
                    limit:docForPage,
                    sort:sort,
                    count:false
                });
                setPage(page-1);
            }
        }
    }
    function search() {
        setPage(1);
        refetch({
            page:1,
            limit:docForPage,
            find:find,
            count:true
        });
    }

    useEffect(()=>{
        if (data?.all_donaciones) {
            if(data.all_donaciones.list)
                setList(data.all_donaciones.list);
            if(data.all_donaciones.count){
                setTotal(data.all_donaciones.count);
                setMaxPage(Math.ceil(data.all_donaciones.count/docForPage));
            }
        }
    },[data]);

    useEffect(()=> {
        var temp={};
        temp.read=store.checkPermissions(["read_donaciones"]);
        temp.update=store.checkPermissions(["update_donaciones"]);
        temp.delete=store.checkPermissions(["delete_donaciones"]);
        set_permissions(temp);
    },[])

    if (error) return <Failure open={failure} setOpen={set_failure} title="Error!" message={error?.message} />

    return (
        <>
            <div className="px-4 sm:px-6 lg:px-8">
                <div className="bg-gray-50 px-4 py-3 rounded-lg border-gray-200 border sm:flex sm:items-center">
                    <div className="sm:flex-auto">
                        <h1 className="text-xl font-semibold text-gray-900 uppercase">Lista de DONACIONES</h1>
                    </div>
                    <div className="mt-4 sm:mt-0 sm:ml-16 sm:flex-none">
                        {store.checkPermissions(['create_donaciones']) &&
                            <Link
                                to="../donaciones/create"
                                type="button"
                                className="inline-flex items-center justify-center rounded-md border border-transparent bg-green-600 px-4 py-2 text-sm font-medium text-white shadow-sm hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-green-500 focus:ring-offset-2 sm:w-auto"
                            >
                                <Icon name="PlusCircleIcon" className="h-6 w-6" />
                                Añadir DONACIONES
                            </Link>
                        }
                    </div>
                </div>
                <div className="z-0 mt-8 flex flex-col">
                    <div className="-my-2 -mx-4 overflow-x-auto sm:-mx-6 lg:-mx-8">
                        <div className="inline-block min-w-full py-2 align-middle md:px-6 lg:px-8">
                            <div className="flex justify-between">
                                <div>
                                    <ButtonsReport title={"Lista de DONACIONES"} query={ALL_DONACIONES_REPORT} find={find} sort={sort} 
                                        fields={[
                                            {id:"entidad",name:"Entidad"},
                                            {id:"items",name:"donacion"},
                                            {id:"respaldo",name:"Respaldo"},
                                            {id:"descripcion",name:"Descripcion de donacion"},
                                            {id:"fecha",name:"FECHA"},
                                            {id:"codigo",name:"ID DE DONACION"},
                                            {id:"estado",name:"Estado"},
                                            {id:"tarifa",name:"Tarifa"},
                                            {id:"usuario",name:"USUARIO"},

                                        ]}
                                        defaultFields={["estado"]}
                                    />
                                </div>
                                <div>
                                    <SearchBar 
                                        fields={[
                                            {id:"entidad",name:"Entidad"},
                                            {id:"items",name:"donacion"},
                                            {id:"respaldo",name:"Respaldo"},
                                            {id:"descripcion",name:"Descripcion de donacion"},
                                            {id:"fecha",name:"FECHA"},
                                            {id:"codigo",name:"ID DE DONACION"},
                                            {id:"estado",name:"Estado"},
                                            {id:"tarifa",name:"Tarifa"},
                                            {id:"usuario",name:"USUARIO"},

                                        ]}
                                        setFind={setFind}
                                        submit={search}
                                    />
                                </div>
                            </div>
                            <div className="overflow-hidden shadow ring-1 ring-black ring-opacity-5 md:rounded-lg">
                                <table className="min-w-full divide-y divide-gray-300">
                                    <thead className="bg-gray-50">
                                    <tr>
                                        <th scope="col" className="py-3.5 pl-4 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-6">
                                            Nro
                                        </th>
                                        {headers.map((h,index) => 
                                            <th key={index} scope="col" className="px-3 py-3.5 text-left text-sm font-semibold text-gray-900">
                                                {h}
                                            </th>
                                        )}
                                        <th scope="col" className="relative py-3.5 pl-3 pr-4 sm:pr-6">
                                            <span className="sr-only">Opciones</span>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody className="divide-y divide-gray-200 bg-white">
                                        {(list && list.length) ?
                                            list.map((donaciones,index)=>(
                                                <tr key={donaciones.id}>
                                                    <td className="whitespace-nowrap py-4 pl-4 pr-3 text-sm font-medium text-gray-900 sm:pl-6">
                                                        {(index+1)+((page-1)*docForPage)}
                                                    </td>
                                                    <td className="whitespace-nowrap px-3 py-4 text-sm text-gray-500">{donaciones.estado}</td>

                                                    <td className="relative whitespace-nowrap py-4 pl-3 pr-4 text-right text-sm font-medium sm:pr-6">
                                                        <OptionsRUD id={donaciones.id} model="donaciones" permissions={permissions} />
                                                    </td>
                                                </tr>
                                            )) : 
                                            <tr>
                                                <td className="py-4 text-center" colSpan={headers.length+2}><h1>No existen Registros</h1></td>
                                            </tr>
                                        }
                                    </tbody>
                                </table>
                                <nav
                                className="flex items-center justify-between border-t border-gray-200 bg-white px-4 py-3 sm:px-6"
                                aria-label="Pagination"
                                >
                                    <div className="hidden sm:block">
                                        <p className="text-sm text-gray-700">
                                        Mostrando <span className="font-medium">{(page-1)*docForPage+1}</span> a <span className="font-medium">{page==maxPage ? total : page*docForPage}</span> de{' '}
                                        <span className="font-medium">{total}</span> resultados
                                        </p>
                                    </div>
                                    <div className="flex flex-1 justify-between sm:justify-end">
                                        <button
                                            onClick={()=> navPage(false)}
                                            className="relative inline-flex items-center rounded-md border border-gray-300 bg-white px-4 py-2 text-sm font-medium text-gray-700 hover:bg-gray-50"
                                        >
                                            Anterior
                                        </button>
                                        <button
                                            onClick={()=> navPage(true)}
                                            className="relative ml-3 inline-flex items-center rounded-md border border-gray-300 bg-white px-4 py-2 text-sm font-medium text-gray-700 hover:bg-gray-50"
                                        >
                                            Siguiente
                                        </button>
                                    </div>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default DONACIONES_list;