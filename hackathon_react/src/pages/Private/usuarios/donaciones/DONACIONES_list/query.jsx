import { gql } from "@apollo/client";

export const ALL_DONACIONES=gql`
    query ($find:JSON,$page:Int,$limit:Int,$sort:JSON,$count:Boolean) {
        all_donaciones (find:$find,page:$page,limit:$limit,sort:$sort,count:$count){
            list {
                id
                estado
            }
            count
        }
    }
`;

export const ALL_DONACIONES_REPORT=gql`
    query ($find:JSON,$page:Int,$limit:Int,$sort:JSON,$count:Boolean) {
        all_donaciones (find:$find,page:$page,limit:$limit,sort:$sort,count:$count){
            list {
                entidad {
                    nombre
                }
                items
                respaldo
                descripcion
                fecha
                codigo
                estado
                tarifa
                usuario
            }
        }
    }
`;