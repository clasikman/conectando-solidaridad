import { gql } from "@apollo/client";

export const READ_SEGUIMINETO=gql`
    query($find:JSON) {
        read_seguimineto(find:$find) {
            donacion {
                codigo
            }
            respaldo_entrega_trasnsporte
            respuesta_entidad
            respuesta_entidad_respaldo
        }
    }
`;

export const DELETE_SEGUIMINETO=gql`
    mutation($id: ID!) {
        delete_seguimineto(id: $id)
    }
`;