import Select from "@/components/Select1";
import Icon from "@/components/Icon";
import { useContext, useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import Error from '@/components/Error';
import Success from '@/components/Success';
import Failure from "@/components/Failure";
import { useLazyQuery, useMutation, useQuery } from "@apollo/client";
import { CREATE_SEGUIMINETO, UPDATE_SEGUIMINETO, READ_SEGUIMINETO, GET_FOREIGN } from './query';
import { StoreContext } from "@/context/store";
import './SEGUIMINETO_form.css';

export default function SEGUIMINETO_form() {
    //essentials
    const {id} = useParams();
    const store=useContext(StoreContext);
    const [errors, set_errors]=useState([]);
    const [submit,set_submit]=useState(false);
    const [success,set_success]=useState(false);
    const [failure,set_failure]=useState(false);
    const [register, data_register]=useMutation(CREATE_SEGUIMINETO);
    const [update, data_update]=useMutation(UPDATE_SEGUIMINETO);
    const [read, data_read]=useLazyQuery(READ_SEGUIMINETO,{fetchPolicy: "no-cache"});
    //foreigns fields
    const foreigns = useQuery(GET_FOREIGN,{fetchPolicy: "no-cache"});
    //states Default
    const [donacion,set_donacion]=useState();
    const [respaldo_entrega_trasnsporte,set_respaldo_entrega_trasnsporte]=useState();
    const [respuesta_entidad,set_respuesta_entidad]=useState();
    const [respuesta_entidad_respaldo,set_respuesta_entidad_respaldo]=useState();


    function handleSubmit(e) {
        e.preventDefault();
        set_errors([]);
        var errors=[];
        //validacioines if error.push("error en string mensaje");





        set_errors(errors);
        if (errors.length==0) {
            store.setLoading(true);
            if (id) {
                update({variables: {
                    id,
                    donacion,
                    respaldo_entrega_trasnsporte,
                    respuesta_entidad,
                    respuesta_entidad_respaldo,

                }})
            } else {
                register({variables: {
                    donacion,
                    respaldo_entrega_trasnsporte,
                    respuesta_entidad,
                    respuesta_entidad_respaldo,

                }})
            }
        } else window.scrollTo(0, 0);
        set_submit(true);
    }



    useEffect(()=> {
        var data=id?data_update:data_register;
        if(data.called && !data.loading) {
            store.setLoading(false);
            if (data.error) {
                console.log("error",data.error);
                set_errors([data.error.message]);
                set_failure(true);
            }
            if (data.data) set_success(true);
        }
    },[data_register.loading,data_update.loading])

    useEffect(()=> {
        if (data_read?.data?.read_seguimineto) {
            const seguimineto=data_read.data.read_seguimineto;
            set_donacion(seguimineto.donacion.id);
            set_respaldo_entrega_trasnsporte(seguimineto.respaldo_entrega_trasnsporte);
            set_respuesta_entidad(seguimineto.respuesta_entidad);
            set_respuesta_entidad_respaldo(seguimineto.respuesta_entidad_respaldo);
            console.log(seguimineto);
        }
    },[data_read.data])

    useEffect(()=> {
        if (id) read({variables: {find: { _id:id }}});

    },[])

    return (
        <>
            <Success open={success} setOpen={set_success} title="Éxito!" message="Registro actualizado correctamente" redirect="/dashboard/seguimineto/list" />
            <Failure open={failure} setOpen={set_failure} title="Error!" message={errors?.[0]} />
            <div className="px-4 sm:px-6 lg:px-8">
                <div className="bg-gray-50 px-4 py-3 rounded-lg border-gray-200 border sm:flex sm:items-center">
                    <div className="sm:flex-auto">
                        <h1 className="text-xl font-semibold text-gray-900 uppercase">{id ? "EDITAR SEGUIMINETO" : "REGISTRAR SEGUIMINETO"}</h1>
                    </div>
                    <div className="mt-4 sm:mt-0 sm:ml-16 sm:flex-none">
                        <Link
                            to="../seguimineto/list"
                            type="button"
                            className="inline-flex items-center justify-center rounded-md border border-transparent bg-gray-600 px-4 py-2 text-sm font-medium text-white shadow-sm hover:bg-gray-700 focus:outline-none focus:ring-2 focus:ring-green-500 focus:ring-offset-2 sm:w-auto"
                        >
                            <Icon name="ArrowLeftCircleIcon" className="h-6 w-6" />
                            Atras
                        </Link>
                    </div>
                </div>
                <div className="mt-8 px-4 py-1 bg-gray-50 rounded-lg border border-gray-200">
                    <Error title="Error en el Formulario" errors={errors}  />
                    <form onSubmit={handleSubmit} className="space-y-8 divide-y divide-gray-200">
                        <div className="space-y-6 px-4 py-4">
                            <div className="sm:grid sm:grid-cols-6 sm:gap-x-10 sm:gap-y-6 sm:items-start">
                                <div className="mt-1 sm:mt-0 sm:col-span-3">
                                    <Select label="Donacion" options={foreigns?.data?.all_donaciones?.list} field="codigo" value={donacion} setValue={set_donacion}  />
                                </div>
                                <div className="mt-1 sm:mt-0 sm:col-span-3">
                                    <label htmlFor="respaldo_entrega_trasnsporte" className="block text-sm font-medium text-gray-800">
                                        Respaldo Entrega Transporte
                                    </label>
                                    <input type="text"  name="respaldo_entrega_trasnsporte" value={respaldo_entrega_trasnsporte} onChange={e=>set_respaldo_entrega_trasnsporte(e.target.value)} className="mt-1 focus:ring-blue-300 focus:border-blue-300 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md extraclass" />
                                </div>
                                <div className="mt-1 sm:mt-0 sm:col-span-3">
                                    <label htmlFor="respuesta_entidad" className="block text-sm font-medium text-gray-800">
                                        Respuesta Entidad
                                    </label>
                                    <textarea name="respuesta_entidad" required  value={respuesta_entidad} onChange={e=>set_respuesta_entidad(e.target.value)} rows="3" className="mt-1 focus:ring-blue-300 focus:border-blue-300 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"/>
                                </div>
                                <div className="mt-1 sm:mt-0 sm:col-span-3">
                                    <label htmlFor="respuesta_entidad_respaldo" className="block text-sm font-medium text-gray-800">
                                        Respuesta Entidad Respaldo
                                    </label>
                                    <input type="text"  name="respuesta_entidad_respaldo" value={respuesta_entidad_respaldo} onChange={e=>set_respuesta_entidad_respaldo(e.target.value)} className="mt-1 focus:ring-blue-300 focus:border-blue-300 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md extraclass" />
                                </div>
                            </div>
                        </div>      
                        <div className="pt-5">
                            <div className="flex justify-end">
                                <Link
                                    to="../seguimineto/list"
                                    className="rounded-md border border-gray-300 bg-white py-2 px-4 text-sm font-medium text-gray-700 shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-green-500 focus:ring-offset-2"
                                >
                                    Cancelar
                                </Link>
                                <button
                                    type="submit"
                                    className="ml-3 inline-flex justify-center rounded-md border border-transparent bg-green-600 py-2 px-4 text-sm font-medium text-white shadow-sm hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-green-500 focus:ring-offset-2"
                                >
                                    Aceptar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>            
            </div>
        </>
    )
}