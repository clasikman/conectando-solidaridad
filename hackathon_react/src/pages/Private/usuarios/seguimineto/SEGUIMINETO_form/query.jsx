import { gql } from "@apollo/client";

export const CREATE_SEGUIMINETO=gql`
    mutation(
        $donacion: ID,
        $respaldo_entrega_trasnsporte: String,
        $respuesta_entidad: String!,
        $respuesta_entidad_respaldo: String
    ) {
        create_seguimineto(
            seguimineto:{
                donacion:$donacion,
                respaldo_entrega_trasnsporte:$respaldo_entrega_trasnsporte,
                respuesta_entidad:$respuesta_entidad,
                respuesta_entidad_respaldo:$respuesta_entidad_respaldo
            }
        ) {
            id
        }
    }   
`;

export const UPDATE_SEGUIMINETO=gql`
    mutation(
        $id:ID!,
        $donacion: ID,
        $respaldo_entrega_trasnsporte: String,
        $respuesta_entidad: String!,
        $respuesta_entidad_respaldo: String
    ) {
        update_seguimineto(
            id:$id
            seguimineto:{
                donacion:$donacion,
                respaldo_entrega_trasnsporte:$respaldo_entrega_trasnsporte,
                respuesta_entidad:$respuesta_entidad,
                respuesta_entidad_respaldo:$respuesta_entidad_respaldo
            }
        ) {
            id
        }
    }
`;

export const READ_SEGUIMINETO=gql`
    query($find:JSON) {
        read_seguimineto(find:$find) {
            donacion {
                id
            }
            respaldo_entrega_trasnsporte
            respuesta_entidad
            respuesta_entidad_respaldo
        }
    }
`;
export const GET_FOREIGN=gql`
    query {
        all_donaciones {
            list {
                id
                codigo
            }
        }
    }
`;