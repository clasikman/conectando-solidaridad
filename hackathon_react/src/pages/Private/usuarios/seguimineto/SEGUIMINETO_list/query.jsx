import { gql } from "@apollo/client";

export const ALL_SEGUIMINETO=gql`
    query ($find:JSON,$page:Int,$limit:Int,$sort:JSON,$count:Boolean) {
        all_seguimineto (find:$find,page:$page,limit:$limit,sort:$sort,count:$count){
            list {
                id

            }
            count
        }
    }
`;

export const ALL_SEGUIMINETO_REPORT=gql`
    query ($find:JSON,$page:Int,$limit:Int,$sort:JSON,$count:Boolean) {
        all_seguimineto (find:$find,page:$page,limit:$limit,sort:$sort,count:$count){
            list {
                donacion {
                    codigo
                }
                respaldo_entrega_trasnsporte
                respuesta_entidad
                respuesta_entidad_respaldo
            }
        }
    }
`;