import Icon from "@/components/Icon";
import { useContext, useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import Failure from "@/components/Failure";
import { useQuery } from "@apollo/client";
import { READ_USUARIOS } from './query';
import { StoreContext } from "@/context/store";
import './USUARIOS_detail.css';

export default function USUARIOS_detail() {
    //essentials
    const {id} = useParams();
    const store=useContext(StoreContext);
    const [failure,set_failure]=useState(true);
    const {data,error}=useQuery(READ_USUARIOS,{fetchPolicy: "no-cache",variables:{find:{_id:id}}});
    const [usuarios,set_usuarios]=useState({});

    useEffect(()=> {
        if (data?.read_usuarios) {
            store.setLoading(false);
            set_usuarios(data.read_usuarios);
        }
    },[data])
    
    useEffect(()=> {
        store.setLoading(true);
    },[])

    if (error) return <Failure open={failure} setOpen={set_failure} title="Error!" message={error?.message} />

    return (
        <>
            <div className="px-4 sm:px-6 lg:px-8">
                <div className="bg-gray-50 px-4 py-3 rounded-lg border-gray-200 border sm:flex sm:items-center">
                    <div className="sm:flex-auto">
                        <h1 className="text-xl font-semibold text-gray-900 uppercase">USUARIOS DETALLE</h1>
                    </div>
                    <div className="mt-4 sm:mt-0 sm:ml-16 sm:flex-none">
                        <Link
                            to="../usuarios/list"
                            type="button"
                            className="inline-flex items-center justify-center rounded-md border border-transparent bg-gray-600 px-4 py-2 text-sm font-medium text-white shadow-sm hover:bg-gray-700 focus:outline-none focus:ring-2 focus:ring-green-500 focus:ring-offset-2 sm:w-auto"
                        >
                            <Icon name="ArrowLeftCircleIcon" className="h-6 w-6" />
                            Atras
                        </Link>
                    </div>
                </div>
                <div className="mt-8 px-4 py-1 bg-gray-50 rounded-lg border border-gray-200">
                    <dl className="mt-5 ml-8 pb-8 grid grid-cols-1 gap-x-4 gap-y-8 sm:grid-cols-2">
                        <div className="sm:col-span-1">
                            <dt className="block text-sm font-medium text-gray-700">
                                Telefono
                            </dt>
                            <dd className="mt-1 block w-full shadow-sm sm:text-sm border-y-400 text-blue-800 rounded-md">
                                {usuarios?.telefono}
                            </dd>
                        </div>
                        <div className="sm:col-span-1">
                            <dt className="block text-sm font-medium text-gray-700">
                                Dirección
                            </dt>
                            <dd className="mt-1 block w-full shadow-sm sm:text-sm border-y-400 text-blue-800 rounded-md">
                                {usuarios?.direccion}
                            </dd>
                        </div>
                        <div className="sm:col-span-1">
                            <dt className="block text-sm font-medium text-gray-700">
                                latitud
                            </dt>
                            <dd className="mt-1 block w-full shadow-sm sm:text-sm border-y-400 text-blue-800 rounded-md">
                                {usuarios?.latitude}
                            </dd>
                        </div>
                        <div className="sm:col-span-1">
                            <dt className="block text-sm font-medium text-gray-700">
                                longitud
                            </dt>
                            <dd className="mt-1 block w-full shadow-sm sm:text-sm border-y-400 text-blue-800 rounded-md">
                                {usuarios?.longitude}
                            </dd>
                        </div>
                        <div className="sm:col-span-1">
                            <dt className="block text-sm font-medium text-gray-700">
                                Nombre
                            </dt>
                            <dd className="mt-1 block w-full shadow-sm sm:text-sm border-y-400 text-blue-800 rounded-md">
                                {usuarios?.nombre}
                            </dd>
                        </div>
                        <div className="sm:col-span-1">
                            <dt className="block text-sm font-medium text-gray-700">
                                usuario
                            </dt>
                            <dd className="mt-1 block w-full shadow-sm sm:text-sm border-y-400 text-blue-800 rounded-md">
                                {usuarios?.usuario}
                            </dd>
                        </div>
                        <div className="sm:col-span-1">
                            <dt className="block text-sm font-medium text-gray-700">
                                contraseña
                            </dt>
                            <dd className="mt-1 block w-full shadow-sm sm:text-sm border-y-400 text-blue-800 rounded-md">
                                {usuarios?.password}
                            </dd>
                        </div>
                        <div className="sm:col-span-1">
                            <dt className="block text-sm font-medium text-gray-700">
                                Donaciones
                            </dt>
                            <dd className="mt-1 block w-full shadow-sm sm:text-sm border-y-400 text-blue-800 rounded-md">
                                {usuarios?.donaciones?.map(f => f.codigo).join(", ")}
                            </dd>
                        </div>
                    </dl>
                </div>            
            </div>
        </>
    )
}