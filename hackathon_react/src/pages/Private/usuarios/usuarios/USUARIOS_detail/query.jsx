import { gql } from "@apollo/client";

export const READ_USUARIOS=gql`
    query($find:JSON) {
        read_usuarios(find:$find) {
            telefono
            direccion
            latitude
            longitude
            nombre
            usuario
            password
            donaciones
        }
    }
`;