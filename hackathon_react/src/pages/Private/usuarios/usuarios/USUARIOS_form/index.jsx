import Autocomplete from "@/components/Autocomplete";
import Icon from "@/components/Icon";
import { useContext, useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import Error from '@/components/Error';
import Success from '@/components/Success';
import Failure from "@/components/Failure";
import { useLazyQuery, useMutation, useQuery } from "@apollo/client";
import { CREATE_USUARIOS, UPDATE_USUARIOS, READ_USUARIOS, GET_FOREIGN } from './query';
import { StoreContext } from "@/context/store";
import './USUARIOS_form.css';


export default function USUARIOS_form() {
    //essentials
    const {id} = useParams();
    const store=useContext(StoreContext);
    const [errors, set_errors]=useState([]);
    const [submit,set_submit]=useState(false);
    const [success,set_success]=useState(false);
    const [failure,set_failure]=useState(false);
    const [register, data_register]=useMutation(CREATE_USUARIOS);
    const [update, data_update]=useMutation(UPDATE_USUARIOS);
    const [read, data_read]=useLazyQuery(READ_USUARIOS,{fetchPolicy: "no-cache"});
    //foreigns fields
    const foreigns = useQuery(GET_FOREIGN,{fetchPolicy: "no-cache"});
    //states Default
    const [telefono,set_telefono]=useState();
    const [direccion,set_direccion]=useState();
    const [latitude,set_latitude]=useState(-17.394996566);
    const [longitude,set_longitude]=useState(-66.1453225064);
    const [nombre,set_nombre]=useState();
    const [usuario,set_usuario]=useState();
    const [password,set_password]=useState();
    const [donaciones,set_donaciones]=useState([]);
    const [donaciones_s,set_donaciones_s]=useState();


    function handleSubmit(e) {
        e.preventDefault();
        set_errors([]);
        var errors=[];
        //validacioines if error.push("error en string mensaje");

        set_errors(errors);
        if (errors.length==0) {
            store.setLoading(true);
            if (id) {
                update({variables: {
                    id,
                    telefono,
                    direccion,
                    latitude,
                    longitude,
                    nombre,
                    usuario,
                    password,
                    donaciones,

                }})
            } else {
                register({variables: {
                    telefono,
                    direccion,
                    latitude,
                    longitude,
                    nombre,
                    usuario,
                    password,
                    donaciones,

                }})
            }
        } else window.scrollTo(0, 0);
        set_submit(true);
    }

    useEffect(()=> {
        if (donaciones_s && !donaciones.some(x=>x.id==donaciones_s)) {
            var temp={...foreigns?.data?.all_donaciones?.list.find(x=>x.id==donaciones_s)};
            set_donaciones([...donaciones,temp]);
            set_donaciones_s("");
        }
    },[donaciones_s])

    useEffect(()=> {
        var data=id?data_update:data_register;
        if(data.called && !data.loading) {
            store.setLoading(false);
            if (data.error) {
                console.log("error",data.error);
                set_errors([data.error.message]);
                set_failure(true);
            }
            if (data.data) set_success(true);
        }
    },[data_register.loading,data_update.loading])

    useEffect(()=> {
        if (data_read?.data?.read_usuarios) {
            const usuarios=data_read.data.read_usuarios;
            set_telefono(usuarios.telefono);
            set_direccion(usuarios.direccion);
            set_latitude(usuarios.latitude);
            set_longitude(usuarios.longitude);
            set_nombre(usuarios.nombre);
            set_usuario(usuarios.usuario);
            set_password(usuarios.password);
            set_donaciones(usuarios.donaciones);
            console.log(usuarios);
        }
    },[data_read.data])

    useEffect(()=> {
        if (id) read({variables: {find: { _id:id }}});

    },[])

    return (
        <>
            <Success open={success} setOpen={set_success} title="Éxito!" message="Registro actualizado correctamente" redirect="/dashboard/usuarios/list" />
            <Failure open={failure} setOpen={set_failure} title="Error!" message={errors?.[0]} />
            <div className="px-4 sm:px-6 lg:px-8">
                <div className="bg-gray-50 px-4 py-3 rounded-lg border-gray-200 border sm:flex sm:items-center">
                    <div className="sm:flex-auto">
                        <h1 className="text-xl font-semibold text-gray-900 uppercase">{id ? "EDITAR USUARIOS" : "REGISTRAR USUARIOS"}</h1>
                    </div>
                    <div className="mt-4 sm:mt-0 sm:ml-16 sm:flex-none">
                        <Link
                            to="../usuarios/list"
                            type="button"
                            className="inline-flex items-center justify-center rounded-md border border-transparent bg-gray-600 px-4 py-2 text-sm font-medium text-white shadow-sm hover:bg-gray-700 focus:outline-none focus:ring-2 focus:ring-green-500 focus:ring-offset-2 sm:w-auto"
                        >
                            <Icon name="ArrowLeftCircleIcon" className="h-6 w-6" />
                            Atras
                        </Link>
                    </div>
                </div>
                <div className="mt-8 px-4 py-1 bg-gray-50 rounded-lg border border-gray-200">
                    <Error title="Error en el Formulario" errors={errors}  />
                    <form onSubmit={handleSubmit} className="space-y-8 divide-y divide-gray-200">
                        <div className="space-y-6 px-4 py-4">
                            <div className="sm:grid sm:grid-cols-6 sm:gap-x-10 sm:gap-y-6 sm:items-start">
                                <div className="mt-1 sm:mt-0 sm:col-span-3">
                                    <label htmlFor="telefono" className="block text-sm font-medium text-gray-700">
                                        Telefono
                                    </label>
                                    <div className="relative mt-1 rounded-md shadow-sm">
                                        <div className="pointer-events-none absolute inset-y-0 left-0 flex items-center pl-3">
                                            <Icon name="PhoneIcon" className="h-5 w-5 text-gray-400" aria-hidden="true" />
                                        </div>
                                        <input type="number" required min="120" placeholder="12345"  name="telefono" value={telefono} onChange={e=>set_telefono(parseInt(e.target.value))} className="block w-full rounded-md border-gray-300 pl-10 focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm" />
                                    </div>
                                </div>
                                <div className="mt-1 sm:mt-0 sm:col-span-3">
                                    <label htmlFor="direccion" className="block text-sm font-medium text-gray-700">
                                        Dirección
                                    </label>
                                    <div className="relative mt-1 rounded-md shadow-sm">
                                        <div className="pointer-events-none absolute inset-y-0 left-0 flex items-center pl-3">
                                            <Icon name="MapIcon" className="h-5 w-5 text-gray-400" aria-hidden="true" />
                                        </div>
                                        <input type="text" required placeholder="Calle Lanza entre La Paz y Oruro"  name="direccion" value={direccion} onChange={e=>set_direccion(e.target.value)} className="block w-full rounded-md border-gray-300 pl-10 focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm" />
                                    </div>
                                </div>
                                <div className="mt-1 sm:mt-0 sm:col-span-3">
                                    <label htmlFor="latitude" className="block text-sm font-medium text-gray-800">
                                        latitud
                                    </label>
                                    <input type="number" step="0.00000000001" placeholder="" readOnly  name="latitude" value={latitude} onChange={e=>set_latitude(parseFloat(e.target.value))} className="mt-1 focus:ring-blue-300 focus:border-blue-300 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md extraclass" />
                                </div>
                                <div className="mt-1 sm:mt-0 sm:col-span-3">
                                    <label htmlFor="longitude" className="block text-sm font-medium text-gray-800">
                                        longitud
                                    </label>
                                    <input type="number" step="0.00000000000001"  name="longitude" value={longitude} onChange={e=>set_longitude(parseFloat(e.target.value))} className="mt-1 focus:ring-blue-300 focus:border-blue-300 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md extraclass" />
                                </div>
                                <div className="mt-1 sm:mt-0 sm:col-span-3">
                                    <label htmlFor="nombre" className="block text-sm font-medium text-gray-800">
                                        Nombre
                                    </label>
                                    <input type="text" required placeholder="Juan Valey"  name="nombre" value={nombre} onChange={e=>set_nombre(e.target.value)} className="mt-1 focus:ring-blue-300 focus:border-blue-300 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md extraclass" />
                                </div>
                                <div className="mt-1 sm:mt-0 sm:col-span-3">
                                    <label htmlFor="usuario" className="block text-sm font-medium text-gray-800">
                                        usuario
                                    </label>
                                    <input type="text" required placeholder="valey78"  name="usuario" value={usuario} onChange={e=>set_usuario(e.target.value)} className="mt-1 focus:ring-blue-300 focus:border-blue-300 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md extraclass" />
                                </div>
                                <div className="mt-1 sm:mt-0 sm:col-span-3">
                                    <label htmlFor="password" className="block text-sm font-medium text-gray-800">
                                        contraseña
                                    </label>
                                    <input type="text" required placeholder=""  name="password" value={password} onChange={e=>set_password(e.target.value)} className="mt-1 focus:ring-blue-300 focus:border-blue-300 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md extraclass" />
                                </div>
                            </div>
                        </div>      
                        <div className="pt-5">
                            <div className="flex justify-end">
                                <Link
                                    to="../usuarios/list"
                                    className="rounded-md border border-gray-300 bg-white py-2 px-4 text-sm font-medium text-gray-700 shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-green-500 focus:ring-offset-2"
                                >
                                    Cancelar
                                </Link>
                                <button
                                    type="submit"
                                    className="ml-3 inline-flex justify-center rounded-md border border-transparent bg-green-600 py-2 px-4 text-sm font-medium text-white shadow-sm hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-green-500 focus:ring-offset-2"
                                >
                                    Aceptar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>            
            </div>
        </>
    )
}