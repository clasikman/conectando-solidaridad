import { gql } from "@apollo/client";

export const CREATE_USUARIOS=gql`
    mutation(
        $telefono: Int!,
        $direccion: String!,
        $latitude: Float,
        $longitude: Float,
        $nombre: String!,
        $usuario: String!,
        $password: String!,
        $donaciones: [JSON]
    ) {
        create_usuarios(
            usuarios:{
                telefono:$telefono,
                direccion:$direccion,
                latitude:$latitude,
                longitude:$longitude,
                nombre:$nombre,
                usuario:$usuario,
                password:$password,
                donaciones:$donaciones
            }
        ) {
            id
        }
    }   
`;

export const UPDATE_USUARIOS=gql`
    mutation(
        $id:ID!,
        $telefono: Int!,
        $direccion: String!,
        $latitude: Float,
        $longitude: Float,
        $nombre: String!,
        $usuario: String!,
        $password: String!,
        $donaciones: [JSON]
    ) {
        update_usuarios(
            id:$id
            usuarios:{
                telefono:$telefono,
                direccion:$direccion,
                latitude:$latitude,
                longitude:$longitude,
                nombre:$nombre,
                usuario:$usuario,
                password:$password,
                donaciones:$donaciones
            }
        ) {
            id
        }
    }
`;

export const READ_USUARIOS=gql`
    query($find:JSON) {
        read_usuarios(find:$find) {
            telefono
            direccion
            latitude
            longitude
            nombre
            usuario
            password
            donaciones
        }
    }
`;
export const GET_FOREIGN=gql`
    query {
        all_donaciones {
            list {
                id
                codigo
            }
        }
    }
`;