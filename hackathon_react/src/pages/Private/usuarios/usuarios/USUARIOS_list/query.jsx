import { gql } from "@apollo/client";

export const ALL_USUARIOS=gql`
    query ($find:JSON,$page:Int,$limit:Int,$sort:JSON,$count:Boolean) {
        all_usuarios (find:$find,page:$page,limit:$limit,sort:$sort,count:$count){
            list {
                id
                telefono
                direccion
                nombre
            }
            count
        }
    }
`;

export const ALL_USUARIOS_REPORT=gql`
    query ($find:JSON,$page:Int,$limit:Int,$sort:JSON,$count:Boolean) {
        all_usuarios (find:$find,page:$page,limit:$limit,sort:$sort,count:$count){
            list {
                telefono
                direccion
                latitude
                longitude
                nombre
                usuario
                password
                donaciones
            }
        }
    }
`;