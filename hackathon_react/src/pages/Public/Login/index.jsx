import { useContext, useEffect, useState } from 'react';
import Logo from '@/assets/logo.png';
import './Login.css';
import { LOGIN } from './query';
import Error from '@/components/Error';
import Loading from '@/components/Loading';
import { useMutation } from '@apollo/client';
import { StoreContext } from '@/context/store';
import { useNavigate } from 'react-router-dom';
import Failure from '@/components/Failure';

function Login() {
    const store=useContext(StoreContext);
    const navigate=useNavigate();
    const [username,set_username]=useState('');
    const [password,set_password]=useState('');
    const [errors,setErrors]=useState([]);
    const [failure,set_failure]=useState(false);
    const [login, {data,loading,error}] = useMutation(LOGIN);

    function handleSubmit(e) {
        e.preventDefault();
        login({ variables: { username, password } });
        set_username('');
        set_password('');
        setErrors([]);
    }

    useEffect(()=>{
        if(data && data.login)
            store.login(data.login);
        if(store.user) {
            navigate("/dashboard");
        }
    },[data,store.user]);

    useEffect(()=>{
        if (error && error.message) {
            if (error.graphQLErrors[0].extensions.code=="USER_DISABLED") {
                set_failure(true);
            } else {
                setErrors(["Verifique los datos"]);
                //setErrors([error?.message]);   
            }
        }
    },[error])

    return (
      <>
        {loading && <Loading />}
        <Failure open={failure} setOpen={set_failure} title="Error!" message={error?.message} />
        <div className="min-h-screen bg-gray-100  flex flex-col justify-center">
            <div className="relative py-3 sm:max-w-xl sm:mx-auto">
                <div className="absolute inset-0 bg-gradient-to-r from-orange-800 to-orange-500 shadow-lg transform -skew-y-6 sm:skew-y-0 sm:-rotate-6 sm:rounded-3xl"></div>
                <div className="relative px-4 py-10 bg-white shadow-lg sm:rounded-3xl sm:p-20">
                    <div className="max-w-md mx-auto">
                        <div>
                            <img className="mx-auto h-48 w-auto pb-4" src={Logo} alt="Workflow" />
                            <h2 className="max-w-sm pb-2 mx-auto text-3xl font-extrabold text-orange-900 text-center">
                                INICIAR SESIÓN
                            </h2>
                        </div>
                        <div className="divide-y divide-gray-200">
                            <Error title="CREDENCIALES INVÁLIDAS" errors={errors} />
                            <form className="space-y-6" onSubmit={handleSubmit} >
                                <div>
                                    <label htmlFor="username" className="block text-sm font-medium text-gray-700">
                                        Nombre de Usuario
                                    </label>
                                    <div className="mt-1">
                                        <input 
                                            id="username"
                                            name="username"
                                            type="text"
                                            autoComplete="username"
                                            required
                                            value={username}
                                            onChange={e => set_username(e.target.value)}    
                                            className="appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm" 
                                        />
                                    </div>
                                </div>
                                <div>
                                    <label htmlFor="password" className="block text-sm font-medium text-gray-700">
                                        Contraseña
                                    </label>
                                    <div className="mt-1">
                                        <input
                                            id="password"
                                            name="password"
                                            type="password"
                                            autoComplete="current-password"
                                            required
                                            value={password}
                                            className="appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                                            onChange={e => set_password(e.target.value)}
                                        />
                                    </div>
                                </div>
                                <div>
                                    <button type="submit" className="w-full flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-orange-600 hover:bg-orange-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                                        Ingresar
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

      </>
    )
  };

  export default Login;