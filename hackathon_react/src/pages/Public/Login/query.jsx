import { gql } from "@apollo/client";

export const LOGIN=gql`
    mutation($username: String!,$password:String!) {
        login(username:$username, password:$password) {
            token
            user {
                id
                name
                username
                email
                is_admin
                is_active
                photo_url
                all_permissions
            }
        }
    }
`;