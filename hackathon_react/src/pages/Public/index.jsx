import React from "react";
import { Outlet } from "react-router-dom";

function Public() {
    return (
        <React.Fragment>
            <h1>Home</h1>
            <Outlet/>
        </React.Fragment>
    );
}

export default Public;