import { createBrowserRouter } from "react-router-dom";
import { lazy, Suspense } from "react";
import App from "../App";
import Public from '../pages/Public';
import Private from '../pages/Private';
import Login from '../pages/Public/Login'
import Loading from '../components/Loading';
import PermissionCheck from '../components/PermissionCheck';
const Dashboard=lazy(()=>import('@/components/Dashboard1'));

//Renderizacion ligera de paginas
//Crear otra funcion para publico si utilizara
function Page({component,permissions}) {
    const path=`../pages/Private/${component}`;
    const Component=lazy(() => import(/* @vite-ignore */ path));
    if (permissions && permissions.length)
        return (
            <PermissionCheck permissions={permissions}>
                <Suspense fallback={<Loading/>}>
                    <Component />
                </Suspense>
            </PermissionCheck>
        );
    else
        return (
            <Suspense fallback={<Loading/>}>
                <Component />
            </Suspense>
        );
}

const publicRoutes = [
    {
        path: "/",
        element: <Public />,
    },
    {
        path: "/login",
        element: <Login />,
    }
];

//required Authentication
const privateRoutes = [
    {
        element: <Private />,
        children: [
            {
                path:'/dashboard',
                element: <Suspense fallback={<Loading/>}><Dashboard /></Suspense>,
                children: [
                    {path:'user/list',element: <Page component="admin/user/User_list" permissions={['read_user']} />},
                    {path:'user/create',element: <Page component="admin/user/User_form" permissions={['create_user']} />},
                    {path:'user/detail/:id',element: <Page component="admin/user/User_detail" permissions={['read_user']} />},
                    {path:'user/update/:id',element: <Page component="admin/user/User_form" permissions={['update_user']} />},
                    {path:'user/delete/:id',element: <Page component="admin/user/User_delete" permissions={['delete_user']} />},

                    {path:'group/list',element: <Page component="admin/group/Group_list" permissions={['read_group']} />},
                    {path:'group/create',element: <Page component="admin/group/Group_form" permissions={['create_group']} />},
                    {path:'group/detail/:id',element: <Page component="admin/group/Group_detail" permissions={['read_group']} />},
                    {path:'group/update/:id',element: <Page component="admin/group/Group_form" permissions={['update_group']} />},
                    {path:'group/delete/:id',element: <Page component="admin/group/Group_delete" permissions={['delete_group']} />},

                    {path:'usuarios/list',element: <Page component="usuarios/usuarios/USUARIOS_list" permissions={['read_usuarios']} />},
                    {path:'usuarios/create',element: <Page component="usuarios/usuarios/USUARIOS_form" permissions={['create_usuarios']} />},
                    {path:'usuarios/detail/:id',element: <Page component="usuarios/usuarios/USUARIOS_detail" permissions={['read_usuarios']} />},
                    {path:'usuarios/update/:id',element: <Page component="usuarios/usuarios/USUARIOS_form" permissions={['update_usuarios']} />},
                    {path:'usuarios/delete/:id',element: <Page component="usuarios/usuarios/USUARIOS_delete" permissions={['delete_usuarios']} />},

                    {path:'donaciones/list',element: <Page component="usuarios/donaciones/DONACIONES_list" permissions={['read_donaciones']} />},
                    {path:'donaciones/create',element: <Page component="usuarios/donaciones/DONACIONES_form" permissions={['create_donaciones']} />},
                    {path:'donaciones/detail/:id',element: <Page component="usuarios/donaciones/DONACIONES_detail" permissions={['read_donaciones']} />},
                    {path:'donaciones/update/:id',element: <Page component="usuarios/donaciones/DONACIONES_form" permissions={['update_donaciones']} />},
                    {path:'donaciones/delete/:id',element: <Page component="usuarios/donaciones/DONACIONES_delete" permissions={['delete_donaciones']} />},

                    {path:'seguimineto/list',element: <Page component="usuarios/seguimineto/SEGUIMINETO_list" permissions={['read_seguimineto']} />},
                    {path:'seguimineto/create',element: <Page component="usuarios/seguimineto/SEGUIMINETO_form" permissions={['create_seguimineto']} />},
                    {path:'seguimineto/detail/:id',element: <Page component="usuarios/seguimineto/SEGUIMINETO_detail" permissions={['read_seguimineto']} />},
                    {path:'seguimineto/update/:id',element: <Page component="usuarios/seguimineto/SEGUIMINETO_form" permissions={['update_seguimineto']} />},
                    {path:'seguimineto/delete/:id',element: <Page component="usuarios/seguimineto/SEGUIMINETO_delete" permissions={['delete_seguimineto']} />},

                    {path:'entidad/list',element: <Page component="entidades/entidad/ENTIDAD_list" permissions={['read_entidad']} />},
                    {path:'entidad/create',element: <Page component="entidades/entidad/ENTIDAD_form" permissions={['create_entidad']} />},
                    {path:'entidad/detail/:id',element: <Page component="entidades/entidad/ENTIDAD_detail" permissions={['read_entidad']} />},
                    {path:'entidad/update/:id',element: <Page component="entidades/entidad/ENTIDAD_form" permissions={['update_entidad']} />},
                    {path:'entidad/delete/:id',element: <Page component="entidades/entidad/ENTIDAD_delete" permissions={['delete_entidad']} />},

                    {path:'registro_donaciones_fisicas/list',element: <Page component="entidades/registro_donaciones_fisicas/REGISTRO_DONACIONES_FISICAS_list" permissions={['read_registro_donaciones_fisicas']} />},
                    {path:'registro_donaciones_fisicas/create',element: <Page component="entidades/registro_donaciones_fisicas/REGISTRO_DONACIONES_FISICAS_form" permissions={['create_registro_donaciones_fisicas']} />},
                    {path:'registro_donaciones_fisicas/detail/:id',element: <Page component="entidades/registro_donaciones_fisicas/REGISTRO_DONACIONES_FISICAS_detail" permissions={['read_registro_donaciones_fisicas']} />},
                    {path:'registro_donaciones_fisicas/update/:id',element: <Page component="entidades/registro_donaciones_fisicas/REGISTRO_DONACIONES_FISICAS_form" permissions={['update_registro_donaciones_fisicas']} />},
                    {path:'registro_donaciones_fisicas/delete/:id',element: <Page component="entidades/registro_donaciones_fisicas/REGISTRO_DONACIONES_FISICAS_delete" permissions={['delete_registro_donaciones_fisicas']} />},

                    {path:'empresa_transporte/list',element: <Page component="transporte/empresa_transporte/Empresa_transporte_list" permissions={['read_empresa_transporte']} />},
                    {path:'empresa_transporte/create',element: <Page component="transporte/empresa_transporte/Empresa_transporte_form" permissions={['create_empresa_transporte']} />},
                    {path:'empresa_transporte/detail/:id',element: <Page component="transporte/empresa_transporte/Empresa_transporte_detail" permissions={['read_empresa_transporte']} />},
                    {path:'empresa_transporte/update/:id',element: <Page component="transporte/empresa_transporte/Empresa_transporte_form" permissions={['update_empresa_transporte']} />},
                    {path:'empresa_transporte/delete/:id',element: <Page component="transporte/empresa_transporte/Empresa_transporte_delete" permissions={['delete_empresa_transporte']} />},

                    {path:'categorias/list',element: <Page component="parametricas_globales/categorias/CATEGORIAS_list" permissions={['read_categorias']} />},
                    {path:'categorias/create',element: <Page component="parametricas_globales/categorias/CATEGORIAS_form" permissions={['create_categorias']} />},
                    {path:'categorias/detail/:id',element: <Page component="parametricas_globales/categorias/CATEGORIAS_detail" permissions={['read_categorias']} />},
                    {path:'categorias/update/:id',element: <Page component="parametricas_globales/categorias/CATEGORIAS_form" permissions={['update_categorias']} />},
                    {path:'categorias/delete/:id',element: <Page component="parametricas_globales/categorias/CATEGORIAS_delete" permissions={['delete_categorias']} />},

                    {path:'items_donacion/list',element: <Page component="parametricas_globales/items_donacion/ITEMS_DONACION_list" permissions={['read_items_donacion']} />},
                    {path:'items_donacion/create',element: <Page component="parametricas_globales/items_donacion/ITEMS_DONACION_form" permissions={['create_items_donacion']} />},
                    {path:'items_donacion/detail/:id',element: <Page component="parametricas_globales/items_donacion/ITEMS_DONACION_detail" permissions={['read_items_donacion']} />},
                    {path:'items_donacion/update/:id',element: <Page component="parametricas_globales/items_donacion/ITEMS_DONACION_form" permissions={['update_items_donacion']} />},
                    {path:'items_donacion/delete/:id',element: <Page component="parametricas_globales/items_donacion/ITEMS_DONACION_delete" permissions={['delete_items_donacion']} />},

//router
                ]
            },
        ]
    }
]

const router = createBrowserRouter([{
    element: <App />,
    children: [
        ...publicRoutes,
        ...privateRoutes
    ]
}]);

export default router;